package com.sharesansar.trading.controller.institution;

import java.util.Collection;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.constant.UrlConstant;
import com.sharesansar.trading.service.RoleService;

@Controller
@RequestMapping({UrlConstant.URL_LOGIN, UrlConstant.URL_ROOT})
@Secured(RoleConstant.IS_AUTHENTICATED_ANONYMOUSLY)
public class UserLoginController {

	private static final Logger LOG = LogManager.getLogger(UserLoginController.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	RoleService userRoleServiceImpl;

	@RequestMapping(method = RequestMethod.GET)
	public String getLoginPage(Model model) {
		LOG.info("Inside UserLoginController#getLoginPage method");
		@SuppressWarnings("unchecked")
		Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder
				.getContext().getAuthentication().getAuthorities();
		if (userRoleServiceImpl.isValidAuthorities(authorities))
			for (GrantedAuthority authority : authorities) {
				if (authority.getAuthority().equals(RoleConstant.INSTITUTION_ADMIN))
					return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_DASHBOARD + "/";
				else if (authority.getAuthority().equals(RoleConstant.NORMAL_USER))
					return "redirect:" + UrlConstant.URL_INSTITUTION_USER_DASHBOARD;
				else
					return "redirect:" + UrlConstant.URL_LOGIN;
			}
		return PageConstant.PAGE_USER_LOGIN;
	}

	@RequestMapping(value = UrlConstant.URL_USER_LOGIN_FAIL, method = RequestMethod.GET)
	public String getLoginFailPage(HttpServletRequest request, Model model) {
		LOG.info("Inside UserLoginController#getLoginFailPage method");
		AuthenticationException exception = (AuthenticationException) request.getSession()
				.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
		if (exception instanceof BadCredentialsException)
			model.addAttribute("authFailed",
					messageSource.getMessage("login.invalid.credentials", null, Locale.getDefault()));
		if (exception instanceof DisabledException)
			model.addAttribute("authFaild",
					messageSource.getMessage("login.account.dissable", null, Locale.getDefault()));
		else
			model.addAttribute("authFaild", messageSource.getMessage("login.invalid", null, Locale.getDefault()));

		return PageConstant.PAGE_USER_LOGIN;
	}
}
