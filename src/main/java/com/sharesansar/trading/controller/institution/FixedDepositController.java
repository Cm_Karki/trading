package com.sharesansar.trading.controller.institution;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sharesansar.trading.constant.AppConstant;
import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.constant.UrlConstant;
import com.sharesansar.trading.entity.AuditUser;
import com.sharesansar.trading.entity.FixedDeposit;
import com.sharesansar.trading.object.FixedDepositJsonResponse;
import com.sharesansar.trading.service.FixedDepositService;
import com.sharesansar.trading.util.JtableJSONResponse;
import com.sharesansar.trading.util.JtableRequestData;
import com.sharesansar.trading.util.ValidatorUtil;

/**
 * Fixed Deposit Controller
 * 
 * @author aleeza
 *
 */
@Controller
@Secured(RoleConstant.NORMAL_USER)
@RequestMapping(value = UrlConstant.URL_FIXED_DEPOSIT)
public class FixedDepositController {

	private static final Logger LOG = LogManager.getLogger(FixedDepositController.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private ValidatorUtil validatorUtil;

	@Autowired
	private FixedDepositService fixedDepositServiceImpl;

	@RequestMapping(value = UrlConstant.URL_LIST, method = RequestMethod.GET)
	public String getFixedDepositList(Model model) {
		System.out.println("--------------");
		LOG.info("Inside FixedDepositController#getFixedDepositList method");
		return PageConstant.PAGE_FIXED_DEPOSIT_LIST;
	}

	/**
	 * Method for getting a list of fixed deposit
	 * 
	 * @param jtableRequestData
	 * @return {@link JtableJSONResponse} of {@link FixedDepositJsonResponse}
	 */
	@ResponseBody
	@RequestMapping(value = UrlConstant.URL_LIST_AJAX, method = RequestMethod.GET)
	public JtableJSONResponse<FixedDepositJsonResponse> users(@ModelAttribute JtableRequestData jtableRequestData,
			@RequestParam(name = "fields[]", required = false) String[] fields,
			@RequestParam(name = "queries[]", required = false) String[] queries) {
		LOG.info("Inside FixedDepositController#fixedDeposits method : Search queries {} and fields {}", queries,
				fields);
		JtableJSONResponse<FixedDepositJsonResponse> jtableJSONResponse = new JtableJSONResponse<FixedDepositJsonResponse>();
		try {
			List<FixedDeposit> fixedDeposits = fixedDepositServiceImpl.getAllFixedDeposit(fields, queries,
					jtableRequestData);
			jtableJSONResponse.setTotalRecordCount(fixedDepositServiceImpl.countAllUser(fields, queries));
			jtableJSONResponse.setResult(AppConstant.ALL_CAPS_OK);
			jtableJSONResponse.setRecords(getFixedDepositJsonResponses(fixedDeposits));
		} catch (Exception e) {
			LOG.error("Error while getting fixeddeposit list {}", e);
			jtableJSONResponse.setResult(AppConstant.ALL_CAPS_ERROR);
			jtableJSONResponse.setMessage(messageSource.getMessage("exception.user.fetch", null, Locale.getDefault()));
		}
		return jtableJSONResponse;
	}

	/**
	 * <p>
	 * Method for getting {@link List} of {@link FixedDepositJsonResponse}. The list
	 * is used as response data for user jtable.
	 * </p>
	 * 
	 * @param users {@link List} of {@link FixedDeposit} The objects containing data
	 *              required to build {@link FixedDepositJsonResponse}.
	 * @return {@link List} of {@link FixedDepositJsonResponse} contains response
	 *         data for jtable
	 */

	private List<FixedDepositJsonResponse> getFixedDepositJsonResponses(List<FixedDeposit> fixedDeposits) {
		LOG.info("Inside FixedDepositController#getBranchJsonResponses method. fixedDeposits size {}",
				fixedDeposits.size());
		List<FixedDepositJsonResponse> fixedDepositJsonResponses = new ArrayList<FixedDepositJsonResponse>();
		for (FixedDeposit fixedDeposit : fixedDeposits) {
			FixedDepositJsonResponse fixedDepositJsonResponse = new FixedDepositJsonResponse();
			fixedDepositJsonResponse.setId(fixedDeposit.getId());
			fixedDepositJsonResponse.setAmount((fixedDeposit.getAmount()));
			fixedDepositJsonResponse.setMonth((fixedDeposit.getMonth()));
			fixedDepositJsonResponses.add(fixedDepositJsonResponse);

		}
		return fixedDepositJsonResponses;
	}

	@RequestMapping(value = UrlConstant.URL_CREATE, method = RequestMethod.GET)
	public String create(Model model) {
		LOG.info("Inside FixedDepositController#create method");
		if (!model.containsAttribute("fixedDeposit"))
			model.addAttribute("fixedDeposit", new FixedDeposit());
		return PageConstant.PAGE_FIXED_DEPOSIT_CREATE;
	}

	@RequestMapping(value = UrlConstant.URL_CREATE, method = RequestMethod.POST)
	public String save(@ModelAttribute("fixedDeposit") FixedDeposit fixedDeposit, BindingResult result,
			RedirectAttributes redirectAttributes, HttpServletRequest request, Model model) {
		LOG.info("Inside FixedDepositController#save method. Fixed Deposit {}", fixedDeposit);
		AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		fixedDeposit.setUser(auth.getUser());
		validatorUtil.validate(fixedDeposit, result);
		if (result.hasErrors()) {
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.fixedDeposit", result);
			redirectAttributes.addFlashAttribute("fixedDeposit", fixedDeposit);
			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("error.fixedDeposit.create", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_FIXED_DEPOSIT + UrlConstant.URL_CREATE;
		}
		try {
			fixedDepositServiceImpl.save(fixedDeposit);
			LOG.info("Fixed deposit created successfully.");
			redirectAttributes.addFlashAttribute("success",
					messageSource.getMessage("success.fixedDeposit.create", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_FIXED_DEPOSIT + UrlConstant.URL_LIST;
		} catch (Exception e) {
			LOG.error("Error while saving fixed deposit {}", e);
			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("exception.fixedDeposit.create", null, Locale.getDefault()));
			redirectAttributes.addFlashAttribute("fixedDeposit", fixedDeposit);
			return "redirect:" + UrlConstant.URL_FIXED_DEPOSIT + UrlConstant.URL_CREATE;
		}
	}

	/**
	 * Method for getting a user object with specified id for view
	 * 
	 * @param id
	 * @param redirectAttributes
	 * @param model
	 * @return {@link String} user view page
	 */
	@RequestMapping(value = UrlConstant.URL_VIEW_ID, method = RequestMethod.GET)
	public String view(@PathVariable("id") Long id, RedirectAttributes redirectAttributes, Model model) {
		LOG.info("Inside FixedDepositController#view method: {}", id);
		FixedDeposit fixedDeposit = fixedDepositServiceImpl.findById(id);
		System.out.println(fixedDeposit);
		if (fixedDeposit == null) {
			redirectAttributes.addFlashAttribute("info",
					messageSource.getMessage("exist.not.fixedDeposit", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_FIXED_DEPOSIT + UrlConstant.URL_LIST;
		}
		model.addAttribute("fixedDeposit", fixedDeposit);
		return PageConstant.PAGE_FIXED_DEPOSIT_VIEW;
	}

	/**
	 * Method for getting user to edit
	 * 
	 * @param id
	 * @param redirectAttributes
	 * @param model
	 * @return {@link String} user edit page
	 */
	@RequestMapping(value = UrlConstant.URL_EDIT_ID, method = RequestMethod.GET)
	public String edit(@PathVariable("id") long id, RedirectAttributes redirectAttributes, Model model) {
		LOG.info("Inside FixedDepositController#edit method");
		FixedDeposit fixedDeposit = fixedDepositServiceImpl.findById(id);
		if (fixedDeposit == null) {
			redirectAttributes.addFlashAttribute("info",
					messageSource.getMessage("exist.not.fixedDeposit", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_FIXED_DEPOSIT + UrlConstant.URL_LIST;
		}
		if (!model.containsAttribute("fixedDeposit")) {
			model.addAttribute("fixedDeposit", fixedDeposit);
		}
		return PageConstant.PAGE_FIXED_DEPOSIT_UPDATE;
	}

	/**
	 * The method use to update the fixedDeposit
	 *
	 * @param fixeddeposit
	 * @param result
	 * @param redirectAttributes
	 * @return {@link String} Redirect to respective Get request
	 */
	@RequestMapping(value = UrlConstant.URL_UPDATE, method = RequestMethod.POST)
	public String update(@ModelAttribute("fixedDeposit") FixedDeposit fixedDeposit, BindingResult result,
			RedirectAttributes redirectAttributes) {
		LOG.info("Inside FixedDepositController#update method");
		FixedDeposit fixedDeposit1 = fixedDepositServiceImpl.findById(fixedDeposit.getId());
		if (fixedDeposit1 == null) {
			redirectAttributes.addFlashAttribute("info",
					messageSource.getMessage("exit.not.fixedDeposit", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_FIXED_DEPOSIT + UrlConstant.URL_LIST;
		}

		fixedDeposit1.setAmount(fixedDeposit.getAmount());
		fixedDeposit1.setMonth(fixedDeposit.getMonth());
		validatorUtil.validate(fixedDeposit1, result);
		if (result.hasErrors()) {
			LOG.debug("Fixeddeposit validation errors : {}", result);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.user", result);
			redirectAttributes.addFlashAttribute("fixedDeposit", fixedDeposit1);
			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("error.user.update", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_FIXED_DEPOSIT + UrlConstant.URL_EDIT + "/" + fixedDeposit1.getId();
		}
		try {
			fixedDepositServiceImpl.update(fixedDeposit1);
			redirectAttributes.addFlashAttribute("success", messageSource.getMessage("success.fixedDeposit.update",
					null, Locale.getDefault()));
			LOG.info("User updated successfully {}", fixedDeposit1);
			return "redirect:" + UrlConstant.URL_FIXED_DEPOSIT + UrlConstant.URL_LIST;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("fixedDeposit", fixedDeposit1);
			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("exception.fixedDeposit.update", null, Locale.getDefault()));
			LOG.error("Cannot update user due to following error : {} ", e);
		}
		return "redirect:" + UrlConstant.URL_FIXED_DEPOSIT + UrlConstant.URL_EDIT + "/" + fixedDeposit1.getId();
	}
	
	/**
	 * Delete {@link FixedDeposit}
	 * 
	 * @param id
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = UrlConstant.URL_DELETE, method = RequestMethod.POST)
	public String delete(@RequestParam("id") long id, RedirectAttributes redirectAttributes) {
		LOG.info("Inside FixedDepositController#delete method . Deleting id {}", id);
		FixedDeposit fixedDeposit = fixedDepositServiceImpl.findById(id);
		if (fixedDeposit == null) {
			redirectAttributes.addFlashAttribute("info",
					messageSource.getMessage("exist.not.fixedDeposit", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_FIXED_DEPOSIT + UrlConstant.URL_LIST;
		}
		try {
			fixedDepositServiceImpl.delete(fixedDeposit);
			redirectAttributes.addFlashAttribute("success", messageSource.getMessage("success.fixedDeposit.delete",
					null, Locale.getDefault()));
			LOG.info("fixedDeposit deleted successfully. Id {}", id);
			return "redirect:" + UrlConstant.URL_FIXED_DEPOSIT + UrlConstant.URL_LIST;
		} catch (Exception e) {
			LOG.info("Cannot delete institution due to {}", e);
			if (e instanceof DataIntegrityViolationException)
				redirectAttributes.addFlashAttribute("error",
						messageSource.getMessage("foreignKeyException.fixedDeposit.delete",
								new Object[] { fixedDeposit.getId() }, Locale.getDefault()));
			else
				redirectAttributes.addFlashAttribute("error", messageSource.getMessage("exception.fixedDeposit.delete",
						new Object[] { fixedDeposit.getId() }, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_FIXED_DEPOSIT + UrlConstant.URL_VIEW + "/"
					+  fixedDeposit.getId();
		}
	}
		

}
