
package com.sharesansar.trading.controller.institution;

import java.util.Collection;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.UrlConstant;
import com.sharesansar.trading.entity.Password;
import com.sharesansar.trading.entity.auth.User;
import com.sharesansar.trading.service.RoleService;
import com.sharesansar.trading.service.UserService;
import com.sharesansar.trading.util.UserUtil;
import com.sharesansar.trading.util.ValidatorUtil;

/**
 * Password Reset Controller
 * 
 * @author cm
 *
 */
@Controller
@RequestMapping(value = UrlConstant.URL_PASSWORD)
@SuppressWarnings("unchecked")
public class PasswordResetController {

	private static final Logger LOG = LogManager.getLogger(PasswordResetController.class);

	@Autowired
	private ValidatorUtil validatorUtil;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private UserService userServiceImpl;

	@Autowired
	private RoleService roleServiceImpl;

	/**
	 * Get Password Reset Form
	 * 
	 * @param id
	 * @param token
	 * @param model
	 * @param redirectAttributes
	 * @return String
	 */
	@RequestMapping(value = UrlConstant.URL_RESET, method = RequestMethod.GET)
	public String form(@RequestParam("id") Long id, @RequestParam("token") String token, Model model,
			RedirectAttributes redirectAttributes) {
		LOG.info("Inside PasswordResetController#form method.");
		Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder
				.getContext().getAuthentication().getAuthorities();
		if (roleServiceImpl.isValidAuthorities(authorities)) {
			if (roleServiceImpl.isInstitutionalUser(authorities))
				return UrlConstant.URL_INSTITUTION_ADMIN_DASHBOARD + "/";
			else if (roleServiceImpl.isNormalUser(authorities))
				return UrlConstant.URL_INSTITUTION_USER_DASHBOARD;
		}
		if (!model.containsAttribute("password")) {
			User user = userServiceImpl.findById(id);
			if (!userServiceImpl.verifyPasswordResetToken(user, token)) {
				redirectAttributes.addFlashAttribute("info",
						messageSource.getMessage("token.password.valid.not", null, Locale.getDefault()));
				return "redirect:" + UrlConstant.URL_LOGIN;
			}
			Password password = new Password();
			password.setUserId(user.getId());
			password.setToken(token);
			model.addAttribute("password", password);
		}
		return PageConstant.PAGE_PASSWORD_RESET;
	}

	/**
	 * Set Password
	 * 
	 * @param password
	 * @param result
	 * @param redirectAttributes
	 * @return String
	 */
	@RequestMapping(value = UrlConstant.URL_RESET, method = RequestMethod.POST)
	public String reset(@ModelAttribute("password") Password password, BindingResult result,
			RedirectAttributes redirectAttributes) {
		LOG.info("Inside PasswordResetController#reset method.");
		Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder
				.getContext().getAuthentication().getAuthorities();
		if (roleServiceImpl.isValidAuthorities(authorities)) {
			if (roleServiceImpl.isInstitutionalUser(authorities))
				return UrlConstant.URL_INSTITUTION_ADMIN_DASHBOARD + "/";
			else if (roleServiceImpl.isNormalUser(authorities))
				return UrlConstant.URL_INSTITUTION_USER_DASHBOARD;
		}
		User user = userServiceImpl.findById(password.getUserId());
		if (!userServiceImpl.verifyPasswordResetToken(user, password.getToken())) {
			redirectAttributes.addFlashAttribute("info",
					messageSource.getMessage("token.password.valid.not", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_LOGIN;
		}
		validatorUtil.validate(password, result);
		if (result.hasErrors()) {
			LOG.debug("Password reset validation errors : {}", result);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.password", result);
			redirectAttributes.addFlashAttribute("password", password);
			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("error.password.reset", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_PASSWORD + UrlConstant.URL_RESET + "?id=" + password.getUserId()
					+ "&&token=" + password.getToken();
		}
		user.setPassword(UserUtil.passwordEncoder(password.getPassword()));
		user.setPasswordResetCode("");
		try {
			userServiceImpl.update(user);
			redirectAttributes.addFlashAttribute("success",
					messageSource.getMessage("success.password.reset", null, Locale.getDefault()));
			LOG.info("Reseting password : {} ", user);
			return "redirect:" + UrlConstant.URL_LOGIN;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("password", password);
			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("exception.password.reset", null, Locale.getDefault()));
			LOG.error("Cannot reset password due to {}", e);
			return "redirect:" + UrlConstant.URL_PASSWORD + UrlConstant.URL_RESET + "?id=" + password.getUserId()
					+ "&&token=" + password.getToken();
		}
	}
}
