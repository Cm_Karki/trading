package com.sharesansar.trading.controller.institution.user;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.constant.UrlConstant;
import com.sharesansar.trading.entity.AuditUser;
import com.sharesansar.trading.entity.auth.UserDetail;
import com.sharesansar.trading.service.UserDetailService;
import com.sharesansar.trading.util.ValidatorUtil;

@Controller
@Secured(value = { RoleConstant.NORMAL_USER })
@RequestMapping(value = UrlConstant.URL_INSTITUTION_USER_MY_DETAILS)
public class UserDetailController {

	private static final Logger LOG = LogManager.getLogger(UserDetailController.class);
	
	@Autowired
	private MessageSource messageSource;

	@Autowired
	private ValidatorUtil validatorUtil;
	
	@Autowired
	private UserDetailService userDetailServiceImpl;



	@RequestMapping(method = RequestMethod.GET)
	public String getMyDetailsList(RedirectAttributes redirectAttributes,Model model) {
		LOG.info("Inside MyDetailsController#getUserDetailsList method");
		AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String name = auth.getUsername();
		UserDetail userDetail = userDetailServiceImpl.findByName(name);
		userDetail.setId(userDetail.getId());
		userDetail.setName(userDetail.getName());
		userDetail.setAddress(userDetail.getAddress());
		userDetail.setContactNumber(userDetail.getContactNumber());
		userDetail.setBoid(userDetail.getBoid());
		userDetail.setClientCode(userDetail.getClientCode());
		userDetail.setFatherName(userDetail.getFatherName());
		userDetail.setDateOfBirth(userDetail.getDateOfBirth());
		model.addAttribute("userDetail", userDetail);
		return PageConstant.PAGE_INSTITUTION_USER_MY_DETAILS_LIST;
	}
	
	
	@RequestMapping(value = UrlConstant.URL_CREATE, method = RequestMethod.GET)
	public String create(Model model) {
		LOG.info("Inside UserDetailController#create method");
		if (!model.containsAttribute("userDetail"))
			model.addAttribute("userDetail", new UserDetail());
		return PageConstant.PAGE_FIXED_DEPOSIT_CREATE;
	}
	/**
	 * Method for getting user to edit
	 * 
	 * @param id
	 * @param redirectAttributes
	 * @param model
	 * @return {@link String} user edit page
	 */
	@RequestMapping(value = UrlConstant.URL_EDIT_ID, method = RequestMethod.GET)
	public String edit(RedirectAttributes redirectAttributes, Model model) {
		LOG.info("Inside UserDetailsController#edit method");
		AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String name = auth.getUsername();
		UserDetail userDetail = userDetailServiceImpl.findByName(name);
		userDetail.setId(userDetail.getId());
		userDetail.setName(userDetail.getName());
		userDetail.setAddress(userDetail.getAddress());
		userDetail.setContactNumber(userDetail.getContactNumber());
		userDetail.setBoid(userDetail.getBoid());
		userDetail.setClientCode(userDetail.getClientCode());
		userDetail.setFatherName(userDetail.getFatherName());
		userDetail.setDateOfBirth(userDetail.getDateOfBirth());
		model.addAttribute("userDetail", userDetail);
		return PageConstant.PAGE_INSTITUTION_USER_MY_DETAILS_UPDATE;
	}
	
	
	@RequestMapping(value = UrlConstant.URL_UPDATE, method = RequestMethod.POST)
	public String update(@ModelAttribute("userDetail") UserDetail userDetail, BindingResult result,
			RedirectAttributes redirectAttributes) {
		LOG.info("Inside UserDetailController#update method");
		AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String name = auth.getUsername();
		UserDetail userDetail1 =userDetailServiceImpl.findByName(name);
//		userDetail1.setName(userDetail.getName());
		userDetail1.setAddress(userDetail.getAddress());
		userDetail1.setContactNumber(userDetail.getContactNumber());
		userDetail1.setBoid(userDetail.getBoid());
		userDetail1.setClientCode(userDetail.getClientCode());
		userDetail1.setGrandfatherName(userDetail.getGrandfatherName());
		userDetail1.setFatherName(userDetail.getFatherName());
		userDetail1.setDateOfBirth(userDetail.getDateOfBirth());
		validatorUtil.validate(userDetail1, result);
		if (result.hasErrors()) {
			LOG.info("UserDetail validation errors : {}", result);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.user", result);
			redirectAttributes.addFlashAttribute("userDetail", userDetail1);
			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("error.user.update", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_INSTITUTION_USER_MY_DETAILS + UrlConstant.URL_EDIT + "/" + userDetail1.getId();
		}
		try {
			System.out.println("i am here successfully updated");
			userDetailServiceImpl.update(userDetail1);
			redirectAttributes.addFlashAttribute("success", messageSource.getMessage("success.fixedDeposit.update",
					null, Locale.getDefault()));
			LOG.info("User Detail updated successfully {}", userDetail1);
			return "redirect:" + UrlConstant.URL_INSTITUTION_USER_MY_DETAILS;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("userDetail", userDetail1);
			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("exception.user.update", null, Locale.getDefault()));
			LOG.error("Cannot update user due to following error : {} ", e);
		}
		return "redirect:" + UrlConstant.URL_INSTITUTION_USER_MY_DETAILS + UrlConstant.URL_EDIT + "/" + userDetail1.getId();
	}

		
	}


	
	

