package com.sharesansar.trading.controller.institution.admin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.HtmlUtils;

import com.sharesansar.trading.constant.AppConstant;
import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.constant.UrlConstant;
import com.sharesansar.trading.entity.AuditUser;
import com.sharesansar.trading.entity.auth.Role;
import com.sharesansar.trading.entity.auth.User;
import com.sharesansar.trading.expection.NameAlreadyExistException;
import com.sharesansar.trading.object.InstitutionJsonResponse;
import com.sharesansar.trading.object.UserJsonResponse;
import com.sharesansar.trading.service.InstitutionService;
import com.sharesansar.trading.service.RoleService;
import com.sharesansar.trading.service.UserService;
import com.sharesansar.trading.util.AppUtil;
import com.sharesansar.trading.util.JtableJSONResponse;
import com.sharesansar.trading.util.JtableRequestData;
import com.sharesansar.trading.util.UrlUtil;
import com.sharesansar.trading.util.ValidatorUtil;

@Controller
@Secured(value = { RoleConstant.INSTITUTION_ADMIN })
@RequestMapping(value = UrlConstant.URL_INSTITUTION_ADMIN_USER)
public class InstitutionUserController {

	private static final Logger LOG = LogManager.getLogger(InstitutionUserController.class);

	@Autowired
	private ValidatorUtil validatorUtil;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private UserService userServiceImpl;

	@Autowired
	private InstitutionService institutionServiceImpl;

	@Autowired
	private RoleService roleServiceImpl;

	@Value("${user.password}")
	private String password;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
		// date formatter
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

	@RequestMapping(value = UrlConstant.URL_LIST, method = RequestMethod.GET)
	public String getUserList(Model model) {
		LOG.info("Inside InstitutionUserController#getUserList method");
		return PageConstant.PAGE_USER_LIST;

	}

	/**
	 * Method for getting a list of institution
	 * 
	 * @param jtableRequestData
	 * @return {@link JtableJSONResponse} of {@link InstitutionJsonResponse}
	 */
	@ResponseBody
	@RequestMapping(value = UrlConstant.URL_LIST_AJAX, method = RequestMethod.GET)
	public JtableJSONResponse<UserJsonResponse> users(@ModelAttribute JtableRequestData jtableRequestData,
			@RequestParam(name = "fields[]", required = false) String[] fields,
			@RequestParam(name = "queries[]", required = false) String[] queries) {
		LOG.info("Inside InstitutionUserController#users method : Search queries {} and fields {}", queries, fields);
		JtableJSONResponse<UserJsonResponse> jtableJSONResponse = new JtableJSONResponse<UserJsonResponse>();
		try {
			AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			List<Role> role = roleServiceImpl.findAllByName(RoleConstant.NORMAL_USER);
			List<User> users = userServiceImpl.findAllUserByInstitution(fields, queries, jtableRequestData,
					auth.getUser().getInstitution(), role);
			jtableJSONResponse.setTotalRecordCount(
					userServiceImpl.countAllUserByInstitution(fields, queries, auth.getUser().getInstitution(), role));
			jtableJSONResponse.setResult(AppConstant.ALL_CAPS_OK);
			jtableJSONResponse.setRecords(getUserJsonResponses(users));
		} catch (Exception e) {
			LOG.error("Error while getting user list {}", e);
			jtableJSONResponse.setResult(AppConstant.ALL_CAPS_ERROR);
			jtableJSONResponse.setMessage(messageSource.getMessage("exception.user.fetch", null, Locale.getDefault()));
		}
		return jtableJSONResponse;
	}

	/**
	 * <p>
	 * Method for getting {@link List} of {@link UserJsonResponse}. The list is used
	 * as response data for user jtable.
	 * </p>
	 * 
	 * @param users
	 *            {@link List} of {@link User} The objects containing data required
	 *            to build {@link UserJsonResponse}.
	 * @return {@link List} of {@link UserJsonResponse} contains response data for
	 *         jtable
	 */
	private List<UserJsonResponse> getUserJsonResponses(List<User> users) {
		LOG.info("Inside InstitutionUserController#getBranchJsonResponses method.");
		List<UserJsonResponse> userJsonResponses = new ArrayList<UserJsonResponse>();
		for (User user : users) {
			UserJsonResponse userJsonResponse = new UserJsonResponse();
			userJsonResponse.setEmail(HtmlUtils.htmlEscape(user.getEmail()));
			userJsonResponse.setExpiredDate(user.getExpiredDate());
			userJsonResponse.setId(user.getId());
			userJsonResponse.setInstitution(HtmlUtils.htmlEscape(user.getInstitution().getName()));
			userJsonResponse.setRoleName(HtmlUtils.htmlEscape(user.getRoles().get(0).getName()));
			userJsonResponse.setUserName(HtmlUtils.htmlEscape(user.getUserName()));
			userJsonResponse.setEnabled(user.getEnabled() ? "Enabled" : "Disabled");
			userJsonResponses.add(userJsonResponse);
		}
		return userJsonResponses;
	}

	@RequestMapping(value = UrlConstant.URL_CREATE, method = RequestMethod.GET)
	public String create(Model model) {
		LOG.info("Inside InstitutionUserController#create method");
		if (!model.containsAttribute("user"))
			model.addAttribute("user", new User());
		return PageConstant.PAGE_USER_CREATE;
	}

	@RequestMapping(value = UrlConstant.URL_CREATE, method = RequestMethod.POST)
	public String save(@ModelAttribute("user") User user, BindingResult result, RedirectAttributes redirectAttributes,
			HttpServletRequest request, Model model) {

		LOG.info("Inside UserController#save method user {}", user);
		user.setEnabled(Boolean.TRUE);
		user.getUserDetail().setUser(user);
		user.setPassword(password);
		user.setPasswordResetCode(AppUtil.getRandomAlphanumeric(16));
		AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		user.setInstitution(auth.getUser().getInstitution());
		user.setRoles(roleServiceImpl.findAllByName(RoleConstant.NORMAL_USER));
		validatorUtil.validate(user, result);
		if (result.hasErrors()) {
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.user", result);
			redirectAttributes.addFlashAttribute("user", user);
			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("error.user.create", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_USER + UrlConstant.URL_CREATE;
		}
		try {
			userServiceImpl.isUnique(user.getUserName(), user.getId());
			userServiceImpl.save(user);
			String passwordResetUrl = UrlUtil.getBaseUrl(request) + UrlConstant.URL_PASSWORD + UrlConstant.URL_RESET;
			userServiceImpl.sendUserCreateEmail(user, passwordResetUrl);
			redirectAttributes.addFlashAttribute("success", messageSource.getMessage("success.user.create",
					new Object[] { user.getUserDetail().getName() }, Locale.getDefault()));
			LOG.info("User " + user.getUserDetail().getName() + "created Successfully.");
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_USER + UrlConstant.URL_LIST;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("user", user);
			if (e instanceof NameAlreadyExistException) {
				result.rejectValue("userName", "Unique.user.username");

				redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.user", result);
				redirectAttributes.addFlashAttribute("error",
						messageSource.getMessage("error.user.create", null, Locale.getDefault()));
				LOG.debug("User validation errors : {}", result);
			} else {
				redirectAttributes.addFlashAttribute("error",
						messageSource.getMessage("exception.user.create", null, Locale.getDefault()));
				LOG.error("Cannot create user due to following error : {} ", e);
			}
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_USER + UrlConstant.URL_CREATE;

		}
	}

	/**
	 * Method for getting a user object with specified id for view
	 * 
	 * @param id
	 * @param redirectAttributes
	 * @param model
	 * @return {@link String} user view page
	 */
	@RequestMapping(value = UrlConstant.URL_VIEW_ID, method = RequestMethod.GET)
	public String view(@PathVariable("id") Long id, RedirectAttributes redirectAttributes, Model model) {
		LOG.info("Inside InstitutionUserController#view method: {}", id);
		User user = userServiceImpl.findById(id);
		if (user == null) {
			redirectAttributes.addFlashAttribute("info",
					messageSource.getMessage("exist.not.user", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_USER + UrlConstant.URL_LIST;
		}
		model.addAttribute("user", user);
		return PageConstant.PAGE_USER_VIEW;
	}

	/**
	 * Method for getting user to edit
	 * 
	 * @param id
	 * @param redirectAttributes
	 * @param model
	 * @return {@link String} user edit page
	 */
	@RequestMapping(value = UrlConstant.URL_EDIT_ID, method = RequestMethod.GET)
	public String edit(@PathVariable("id") long id, RedirectAttributes redirectAttributes, Model model) {
		LOG.info("Inside InstitutionUserController#edit method");
		User user = userServiceImpl.findById(id);
		if (user == null) {
			redirectAttributes.addFlashAttribute("info",
					messageSource.getMessage("exist.not.user", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_USER + UrlConstant.URL_LIST;
		}
		if (!model.containsAttribute("user")) {
			model.addAttribute("user", user);
		}
		return PageConstant.PAGE_USER_UPDATE;
	}

	/**
	 * The method use to update the user
	 *
	 * @param user
	 * @param result
	 * @param redirectAttributes
	 * @return {@link String} Redirect to respective Get request
	 */
	@RequestMapping(value = UrlConstant.URL_UPDATE, method = RequestMethod.POST)
	public String update(@ModelAttribute("user") User user, BindingResult result,
			RedirectAttributes redirectAttributes) {
		LOG.info("Inside InstitutionUserController#update method");
		User user1 = userServiceImpl.findById(user.getId());
		if (user1 == null) {
			redirectAttributes.addFlashAttribute("info",
					messageSource.getMessage("exit.not.user", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_USER + UrlConstant.URL_LIST;
		}
		String prevRole = user1.getRoles().get(0).getName();
		AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		user1.getUserDetail().setName(user.getUserDetail().getName());
		user1.getUserDetail().setContactNumber(user.getUserDetail().getContactNumber());
		user1.getUserDetail().setAddress(user.getUserDetail().getAddress());
		user1.getUserDetail().setUser(user1);
		user1.setUserName(user.getUserName());
		user1.setEmail(user.getEmail());
		user1.setExpiredDate(user.getExpiredDate());
		validatorUtil.validate(user1, result);
		if (result.hasErrors()) {
			LOG.debug("User validation errors : {}", result);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.user", result);
			redirectAttributes.addFlashAttribute("user", user1);
			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("error.user.update", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_USER + UrlConstant.URL_EDIT + "/" + user1.getId();
		}
		try {
			userServiceImpl.isUnique(user1.getUserName(), user1.getId());
			userServiceImpl.update(user1);
			if (!prevRole.equals(user1.getRoles().get(0).getName()))
				userServiceImpl.sendUserRoleChangeEmail(user1, prevRole);
			redirectAttributes.addFlashAttribute("success", messageSource.getMessage("success.user.update",
					new Object[] { user1.getUserName() }, Locale.getDefault()));
			LOG.info("User updated successfully {}", user1);
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_USER + UrlConstant.URL_LIST;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("user", user1);
			if (e instanceof NameAlreadyExistException) {
				result.rejectValue("userName", "Unique.user.username");
				redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.user", result);
				redirectAttributes.addFlashAttribute("error",
						messageSource.getMessage("error.user.update", null, Locale.getDefault()));
				LOG.debug("User validation errors : {}", result);
			} else {
				redirectAttributes.addFlashAttribute("error",
						messageSource.getMessage("exception.user.update", null, Locale.getDefault()));
				LOG.error("Cannot update user due to following error : {} ", e);
			}
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_USER + UrlConstant.URL_EDIT + "/" + user1.getId();
		}
	}

	/**
	 * Change the status of user<br>
	 * As enable or disable
	 *
	 * @param id
	 * @param action
	 * @param redirectAttributes
	 * @return String
	 */
	@RequestMapping(value = UrlConstant.URL_STATUS, method = RequestMethod.POST)
	public String status(@RequestParam("id") Long id, RedirectAttributes redirectAttributes) {
		LOG.info("Inside InstitutionUserController#status method: {}", id);
		User user = userServiceImpl.findById(id);
		if (user == null) {
			redirectAttributes.addFlashAttribute("info",
					messageSource.getMessage("exist.not.user", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_USER + UrlConstant.URL_LIST;
		}
		if (user.getEnabled())
			user.setEnabled(Boolean.FALSE);
		else
			user.setEnabled(Boolean.TRUE);
		try {
			userServiceImpl.update(user);
			if (user.getEnabled()) {
				userServiceImpl.sendUserEnabledEmail(user);
				redirectAttributes.addFlashAttribute("success",
						messageSource.getMessage("success.user.enabled", null, Locale.getDefault()));
			} else {
				userServiceImpl.sendUserDisabledEmail(user);
				redirectAttributes.addFlashAttribute("success",
						messageSource.getMessage("success.user.disabled", null, Locale.getDefault()));
			}
			LOG.info("Changing status of user : {}", user);
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("exception.user.status", null, Locale.getDefault()));
			LOG.error("Cannot change status of user due to {}", e);
		}
		return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_USER + UrlConstant.URL_VIEW + "/" + user.getId();
	}

}
