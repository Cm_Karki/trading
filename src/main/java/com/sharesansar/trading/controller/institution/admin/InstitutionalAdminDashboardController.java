package com.sharesansar.trading.controller.institution.admin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.constant.UrlConstant;
import com.sharesansar.trading.entity.AuditUser;
import com.sharesansar.trading.entity.auth.User;

@Controller
@RequestMapping(value = UrlConstant.URL_INSTITUTION_ADMIN_DASHBOARD)
@Secured(value = { RoleConstant.INSTITUTION_ADMIN })
public class InstitutionalAdminDashboardController {

	private static final Logger LOG = LogManager.getLogger(InstitutionalAdminDashboardController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String getDashboard(Model model) {

		LOG.info("Inside InstitutionalAdminDashboardController#getDashboard method");
		return PageConstant.PAGE_INSTITUTION_ADMIN_DASHBOARD;

	}

}
