package com.sharesansar.trading.controller.institution.user;

import java.beans.PropertyEditorSupport;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sharesansar.trading.constant.OrderStatus;
import com.sharesansar.trading.constant.OrderType;
import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.constant.UrlConstant;
import com.sharesansar.trading.constant.Validity;
import com.sharesansar.trading.entity.AuditUser;
import com.sharesansar.trading.entity.Order;
import com.sharesansar.trading.object.MarketDepth;
import com.sharesansar.trading.object.MarketDepth.Buy;
import com.sharesansar.trading.object.MarketDepth.Sell;
import com.sharesansar.trading.object.NEPSECompany;
import com.sharesansar.trading.service.CompanyService;
import com.sharesansar.trading.service.OrderService;
import com.sharesansar.trading.util.ApiUtil;
import com.sharesansar.trading.util.MarketDepthCruler;
import com.sharesansar.trading.util.ValidatorUtil;

@Controller
@Secured(value = { RoleConstant.NORMAL_USER })
@RequestMapping(value = UrlConstant.URL_INSTITUTION_USER_MARKET_DEPTH)
public class MarketDepthController {

	private static final Logger LOG = LogManager.getLogger(InstitutionUserDashboard.class);

	@Value("${base.url}")
	private String baseUrl;

	@Value("${api.key}")
	private String key;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private ValidatorUtil validatorUtil;

	@Autowired
	private OrderService orderServiceImpl;

	@Autowired
	private CompanyService companyServiceImpl;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
		// order
		binder.registerCustomEditor(NEPSECompany.class, new PropertyEditorSupport() {

			// Converts a String ID to a Order (when submitting form)
			@Override
			public void setAsText(String name) {
				if (name == null || name.length() <= 0) {
					throw new RuntimeException();
				}
				NEPSECompany company = new NEPSECompany();
				if (company == null)
					this.setValue(null);
				else
					this.setValue(company);
			}
		});
		// Validity Enum
		binder.registerCustomEditor(Validity.class, new PropertyEditorSupport() {
			// Converts a String validity to a Validity enum(when submitting form)
			@Override
			public void setAsText(String name) {
				if (name == null || name.length() <= 0) {
					throw new RuntimeException();
				}
				Validity validity = Validity.getByValue(name);
				if (validity == null)
					throw new RuntimeException();
				else
					this.setValue(validity);
			}
		});

		// OrderType Enum
		binder.registerCustomEditor(OrderType.class, new PropertyEditorSupport() {
			// Converts a String validity to a Validity enum(when submitting form)
			@Override
			public void setAsText(String name) {
				if (name == null || name.length() <= 0) {
					throw new RuntimeException();
				}
				OrderType orderType = OrderType.getByValue(name);
				if (orderType == null)
					throw new RuntimeException();
				else
					this.setValue(orderType);
			}
		});
	}

	@RequestMapping(method = RequestMethod.GET)
	public String getMarketDepth(Model model) {
		LOG.info("Inside MarketDepthController#getMarketDepth method");
		AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		model.addAttribute("balance", auth.getUser().getInstitution().getMoneyAllocation().getAmount());
		model.addAttribute("company", new NEPSECompany());
		model.addAttribute("share", new NEPSECompany());
		model.addAttribute("companyList", companyServiceImpl.findAll());
		model.addAttribute("validityList", Arrays.asList(Validity.values()));
		model.addAttribute("order", new Order());

		List<Order> orders = orderServiceImpl.findByUserAndStatusAndOrderType(auth.getUser(), OrderStatus.LIVE,
				OrderType.BUY);
		model.addAttribute("orderList", orders);
		/**
		 * NEPSE Cruler
		 */
		// MarketDepth market = MarketDepthCruler.getMarketDepth();

		// List<Buy> buys = market.getBuys();
		// for (Buy b : buys) {
		// System.err.println("buys <<<<<<<<<<<<<<<<<< " + b.getQuantity() + " >>>>>>>>>
		// " + b.getPrice());
		// }
		// List<Sell> sells = market.getSells();
		// for (Sell b : sells) {
		// System.out.println("sells <<<<<<<<<<<<<<<<<< " + b.getQuantity() + "
		// >>>>>>>>> " + b.getPrice());
		// }
		model.addAttribute("marketdepth", MarketDepthCruler.getMarketDepth());
		return PageConstant.PAGE_INSTITUTION_USER_MARKET_DEPTH;
	}

	/**
	 * controller to save user order
	 * 
	 * @param order
	 * @param results
	 * @param model
	 * @return
	 */
	@RequestMapping(value = UrlConstant.URL_INSTITUTION_USER_ORDER_SAVE_AJAX, method = RequestMethod.POST)
	public String saveOrder(@ModelAttribute("order") Order order, @RequestParam("radioValue") String radioValue,
			BindingResult result, Model model) {
		LOG.info("Inside MarketDepthController#saveOrder method radioValue {}, orderType {}", radioValue,
				order.getOrderType());
		AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		order.setStatus(OrderStatus.LIVE);
		order.setUser(auth.getUser());

		/**
		 * NEPSE Cruler for selected company
		 */
		MarketDepth market = MarketDepthCruler.getMarketDepth();
		if (order.getOrderType().equals(OrderType.BUY)) {
			List<Buy> buys = market.getBuys();
			for (Buy b : buys) {
				if (b.getPrice().equals(order.getPrice())) {
					Order executedOrder = new Order();
					executedOrder.setCompany(order.getCompany());
					executedOrder.setOrderType(OrderType.BUY);
					executedOrder.setPrice(order.getPrice());
					executedOrder.setQuantity(
							order.getQuantity().compareTo(b.getQuantity()) < 1 ? order.getPrice() : b.getQuantity());
					executedOrder.setStatus(OrderStatus.EXECUTED);
					executedOrder.setUser(auth.getUser());
					orderServiceImpl.save(executedOrder);
					break;
				}
			}
		} else if (order.getOrderType().equals(OrderType.SELL)) {
			List<Sell> sells = market.getSells();
			for (Sell s : sells) {
				Order executedOrder = new Order();
				executedOrder.setCompany(order.getCompany());
				executedOrder.setOrderType(OrderType.SELL);
				executedOrder.setPrice(order.getPrice());
				executedOrder.setQuantity(
						order.getQuantity().compareTo(s.getQuantity()) < 1 ? order.getPrice() : s.getQuantity());
				executedOrder.setStatus(OrderStatus.EXECUTED);
				executedOrder.setUser(auth.getUser());
				orderServiceImpl.save(executedOrder);
				break;
			}
		}

		// for order type current market value
		/**
		 * validity should be instantly price is equal to the current market price
		 */
		// need to change jst for test
		DateTime today = new DateTime();
		DateTime validTill;
		if (order.getValidity().equals(Validity.valueOf("DAY")))
			validTill = today.plusDays(1);
		else if (order.getValidity().equals(Validity.valueOf("GTC")))
			validTill = today.plusDays(15);
		else if (order.getValidity().equals(Validity.valueOf("GTD")))
			validTill = today.plusDays(90);
		else
			validTill = today;
		order.setValidTill(validTill.toDate());

		model.addAttribute("companyList", ApiUtil.getCompanyList(baseUrl, key));
		model.addAttribute("validityList", Arrays.asList(Validity.values()));

		validatorUtil.validate(order, result);
		if (result.hasErrors()) {
			model.addAttribute("isSuccess", "NO");
			model.addAttribute("org.springframework.validation.BindingResult.user", result);
			model.addAttribute("order", order);
			model.addAttribute("error", messageSource.getMessage("error.order.create", null, Locale.getDefault()));
			System.err.println("at validation");
			return PageConstant.PAGE_INSTITUTION_USER_ORDER_FORM;
		}
		try {
			orderServiceImpl.save(order);
			List<Order> orders = orderServiceImpl.findByUserAndStatusAndOrderType(auth.getUser(), OrderStatus.LIVE,
					OrderType.BUY);
			model.addAttribute("orderList", orders);
			model.addAttribute("isSuccess", "YES");
			model.addAttribute("success", messageSource.getMessage("success.order.create", new Object[] { "testOrder" },
					Locale.getDefault()));

			model.addAttribute("balance", auth.getUser().getInstitution().getMoneyAllocation().getAmount());
			model.addAttribute("company", new NEPSECompany());
			model.addAttribute("share", new NEPSECompany());
			model.addAttribute("companyList", companyServiceImpl.findAll());
			model.addAttribute("validityList", Arrays.asList(Validity.values()));

			LOG.info("Order created Successfully.");
			return PageConstant.PAGE_INSTITUTION_USER_ORDER_FORM;
		} catch (Exception e) {
			model.addAttribute("isSuccess", "NO");
			model.addAttribute("error", messageSource.getMessage("exception.order.create", null, Locale.getDefault()));
			LOG.error("Cannot create order due to following error : {} ", e);
			System.err.println("at exception");
			return PageConstant.PAGE_INSTITUTION_USER_ORDER_FORM;
		}

	}
}
