package com.sharesansar.trading.controller.institution;

import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.UrlConstant;
import com.sharesansar.trading.entity.Institution;
import com.sharesansar.trading.entity.RegisterUser;
import com.sharesansar.trading.entity.auth.Role;
import com.sharesansar.trading.entity.auth.User;
import com.sharesansar.trading.entity.auth.UserDetail;
import com.sharesansar.trading.expection.NameAlreadyExistException;
import com.sharesansar.trading.service.InstitutionService;
import com.sharesansar.trading.service.RoleService;
import com.sharesansar.trading.service.UserService;
import com.sharesansar.trading.util.AppUtil;
import com.sharesansar.trading.util.UserUtil;
import com.sharesansar.trading.util.ValidatorUtil;

@Controller
@RequestMapping(value = UrlConstant.URL_USER_REGISTRATION)
public class UserRegistrationContoller {

	private static final Logger LOG = LogManager.getLogger(UserRegistrationContoller.class);

	@Autowired
	private ValidatorUtil validatorUtil;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private UserService userServiceImpl;

	@Autowired
	private InstitutionService institutionServiceImpl;

	@Autowired
	private RoleService roleServiceImpl;

	@Value("${normal.user.role}")
	private String userRole;

	@RequestMapping(method = RequestMethod.GET)
	public String getRegistrationPage(Model model) {
		LOG.info("Inside UserRegistrationController#getRegistrationPage method");
		if (!model.containsAttribute("user"))
			model.addAttribute("user", new RegisterUser());
		return PageConstant.PAGE_USER_REGISTRATION;
	}

	@RequestMapping(method = RequestMethod.POST)
	public String register(@ModelAttribute("user") RegisterUser registerUser, RedirectAttributes redirectAttributes,
			BindingResult result, Model model) {
		LOG.info("Inside UserRegistrationController#register method user {}", registerUser);
		validatorUtil.validate(registerUser, result);
		if (result.hasErrors()) {
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.user", result);
			redirectAttributes.addFlashAttribute("user", registerUser);
			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("error.user.register", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_USER_REGISTRATION;
		}
		try {
			userServiceImpl.isUnique(registerUser.getUserName(), 0);
			Institution institution = institutionServiceImpl.findByIsMain(Boolean.TRUE);
			List<Role> roles = roleServiceImpl.findAllByName(userRole);
			DateTime nextYearDate = new DateTime().plusYears(1);
			User user = new User();
			UserDetail uDetail = new UserDetail();
			uDetail.setAddress(registerUser.getAddress());
			uDetail.setContactNumber(registerUser.getContactNumber());
			uDetail.setName(registerUser.getName());
			uDetail.setUser(user);
			user.setEnabled(Boolean.TRUE);
			user.setPasswordResetCode(AppUtil.getRandomAlphanumeric(16));
			user.setEmail(registerUser.getEmail());
			user.setExpiredDate(nextYearDate.toDate());
			user.setInstitution(institution);
			user.setPassword(UserUtil.passwordEncoder(registerUser.getPassword()));
			user.setRoles(roles);
			user.setUserDetail(uDetail);
			user.setUserName(registerUser.getUserName());
			userServiceImpl.save(user);
			userServiceImpl.sendUserRegisterEmail(user);
			redirectAttributes.addFlashAttribute("success", messageSource.getMessage("success.user.register",
					new Object[] { user.getUserName() }, Locale.getDefault()));
			LOG.info("User " + user.getUserDetail().getName() + "created Successfully.");
			return "redirect:" + UrlConstant.URL_LOGIN;

		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("user", registerUser);
			if (e instanceof NameAlreadyExistException) {
				result.rejectValue("userName", "Unique.user.name");

				redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.user", result);
				redirectAttributes.addFlashAttribute("error",
						messageSource.getMessage("error.user.create", null, Locale.getDefault()));
				LOG.debug("User validation errors : {}", result);
			} else {
				redirectAttributes.addFlashAttribute("error",
						messageSource.getMessage("exception.user.create", null, Locale.getDefault()));
				LOG.error("Cannot create user due to following error : {} ", e);
			}
			return "redirect:" + UrlConstant.URL_USER_REGISTRATION;

		}

	}

}
