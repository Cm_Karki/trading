package com.sharesansar.trading.controller.institution.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.constant.UrlConstant;

@Controller
@Secured(value = { RoleConstant.NORMAL_USER })
@RequestMapping(UrlConstant.URL_INSTITUTION_USER)
public class InstitutionUserDashboard {

	private static final Logger LOG = LogManager.getLogger(InstitutionUserDashboard.class);

	@RequestMapping(method = RequestMethod.GET)
	public String getDashboard(Model model) {
		LOG.info("Inside InstitutionUserDashboard#getDashboard method");
		return PageConstant.PAGE_INSTITUTION_USER_DASHBOARD;
	}

}
