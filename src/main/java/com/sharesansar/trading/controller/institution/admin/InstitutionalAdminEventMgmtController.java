package com.sharesansar.trading.controller.institution.admin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.HtmlUtils;

import com.sharesansar.trading.constant.AppConstant;
import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.constant.UrlConstant;
import com.sharesansar.trading.entity.AuditUser;
import com.sharesansar.trading.entity.InstitutionEvent;
import com.sharesansar.trading.entity.auth.User;
import com.sharesansar.trading.object.EventJsonResponse;
import com.sharesansar.trading.object.UserJsonResponse;
import com.sharesansar.trading.service.EventManagementService;
import com.sharesansar.trading.util.JtableJSONResponse;
import com.sharesansar.trading.util.JtableRequestData;
import com.sharesansar.trading.util.ValidatorUtil;

@Controller
@Secured(value = { RoleConstant.INSTITUTION_ADMIN })
@RequestMapping(value = UrlConstant.URL_INSTITUTION_ADMIN_EVENT_MGMT)
public class InstitutionalAdminEventMgmtController {

	private static final Logger LOG = LogManager.getLogger(InstitutionalAdminEventMgmtController.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private ValidatorUtil validatorUtil;

	@Autowired
	private EventManagementService eventManagementServiceImpl;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
		// date formatter
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

	@RequestMapping(method = RequestMethod.GET)
	public String getEventList(Model model) {
		LOG.info("Inside InstitutionalAdminEventMgmtController#getEventList method");
		return PageConstant.PAGE_EVENT_MGMT_LIST;
	}

	/**
	 * Method for getting a list of institution
	 * 
	 * @param jtableRequestData
	 * @return {@link JtableJSONResponse} of {@link InstitutionEventJsonResponse}
	 */
	@ResponseBody
	@RequestMapping(value = UrlConstant.URL_LIST_AJAX, method = RequestMethod.GET)
	public JtableJSONResponse<EventJsonResponse> events(@ModelAttribute JtableRequestData jtableRequestData,
			@RequestParam(name = "fields[]", required = false) String[] fields,
			@RequestParam(name = "queries[]", required = false) String[] queries) {
		LOG.info("Inside InstitutionalAdminEventMgmtController#events method : Search queries {} and fields {}",
				queries, fields);
		JtableJSONResponse<EventJsonResponse> jtableJSONResponse = new JtableJSONResponse<EventJsonResponse>();
		try {
			AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			List<InstitutionEvent> events = eventManagementServiceImpl.findAllUserByInstitution(fields, queries,
					jtableRequestData, auth.getUser().getInstitution());
			jtableJSONResponse.setTotalRecordCount(eventManagementServiceImpl.countAllEventByInstitution(fields,
					queries, auth.getUser().getInstitution()));
			jtableJSONResponse.setResult(AppConstant.ALL_CAPS_OK);
			jtableJSONResponse.setRecords(getEventJsonResponses(events));
		} catch (Exception e) {
			LOG.error("Error while getting user list {}", e);
			jtableJSONResponse.setResult(AppConstant.ALL_CAPS_ERROR);
			jtableJSONResponse.setMessage(messageSource.getMessage("exception.event.fetch", null, Locale.getDefault()));
		}
		return jtableJSONResponse;
	}

	/**
	 * <p>
	 * Method for getting {@link List} of {@link UserJsonResponse}. The list is used
	 * as response data for user jtable.
	 * </p>
	 * 
	 * @param users
	 *            {@link List} of {@link User} The objects containing data required
	 *            to build {@link UserJsonResponse}.
	 * @return {@link List} of {@link UserJsonResponse} contains response data for
	 *         jtable
	 */
	private List<EventJsonResponse> getEventJsonResponses(List<InstitutionEvent> events) {
		LOG.info("Inside InstitutionalAdminEventMgmtController#getEventJsonResponses method.");
		List<EventJsonResponse> eventJsonResponses = new ArrayList<EventJsonResponse>();
		for (InstitutionEvent event : events) {
			EventJsonResponse eventJsonResponse = new EventJsonResponse();
			eventJsonResponse.setNameEvent(HtmlUtils.htmlEscape(event.getNameEvent()));
			eventJsonResponse.setDateFrom(event.getDateFrom());
			eventJsonResponse.setDateTo(event.getDateTo());
			eventJsonResponse.setId(event.getId());
			eventJsonResponses.add(eventJsonResponse);
		}
		return eventJsonResponses;
	}

	/**
	 * Controller to handle create request form event create page
	 * 
	 * @param model
	 * @return Page create-event
	 */
	@RequestMapping(value = UrlConstant.URL_CREATE, method = RequestMethod.GET)
	public String create(Model model) {
		LOG.info("Inside InstitutionalAdminEventMgmtController#create method");
		if (!model.containsAttribute("event"))
			model.addAttribute("event", new InstitutionEvent());
		return PageConstant.PAGE_EVENT_CREATE;
	}

	/**
	 * method to handle create post request
	 * 
	 * @param user
	 * @param result
	 * @param redirectAttributes
	 * @param request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = UrlConstant.URL_CREATE, method = RequestMethod.POST)
	public String save(@ModelAttribute("user") InstitutionEvent event, BindingResult result,
			RedirectAttributes redirectAttributes, HttpServletRequest request, Model model) {

		LOG.info("Inside InstitutionalAdminEventMgmtController#save method event {}", event);
		AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		event.setInstitution(auth.getUser().getInstitution());
		event.setEnabled(true);
		validatorUtil.validate(event, result);
		if (result.hasErrors()) {
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.event", result);
			redirectAttributes.addFlashAttribute("event", event);
			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("error.event.create", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_EVENT_MGMT + UrlConstant.URL_CREATE;
		}
		try {
			eventManagementServiceImpl.save(event);
			redirectAttributes.addFlashAttribute("success", messageSource.getMessage("success.event.create",
					new Object[] { event.getNameEvent() }, Locale.getDefault()));
			LOG.info("Event " + event.getNameEvent() + "created Successfully.");
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_EVENT_MGMT;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("event", event);

			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("exception.event.create", null, Locale.getDefault()));
			LOG.error("Cannot create event due to following error : {} ", e);
		}
		return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_EVENT_MGMT + UrlConstant.URL_CREATE;

	}

	/**
	 * Method for getting a event object with specified id for view
	 *
	 * @param id
	 * @param redirectAttributes
	 * @param model
	 * @return {@link String} event view page
	 */
	@RequestMapping(value = UrlConstant.URL_VIEW_ID, method = RequestMethod.GET)
	public String view(@PathVariable("id") Long id, RedirectAttributes redirectAttributes, Model model) {
		LOG.info("Inside InstitutionalAdminEventMgmtController#view method: {}", id);
		InstitutionEvent event = eventManagementServiceImpl.findById(id);
		if (event == null) {
			redirectAttributes.addFlashAttribute("info",
					messageSource.getMessage("exist.not.event", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_EVENT_MGMT;
		}
		model.addAttribute("event", event);
		return PageConstant.PAGE_EVENT_VIEW;
	}

	/**
	 * Method for getting event to edit
	 *
	 * @param id
	 * @param redirectAttributes
	 * @param model
	 * @return {@link String} event edit page
	 */
	@RequestMapping(value = UrlConstant.URL_EDIT_ID, method = RequestMethod.GET)
	public String edit(@PathVariable("id") long id, RedirectAttributes redirectAttributes, Model model) {
		LOG.info("Inside InstitutionalAdminEventMgmtController#edit method");
		InstitutionEvent event = eventManagementServiceImpl.findById(id);
		if (event == null) {
			redirectAttributes.addFlashAttribute("info",
					messageSource.getMessage("exist.not.event", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_EVENT_MGMT;
		}
		if (!model.containsAttribute("event")) {
			model.addAttribute("event", event);
		}
		return PageConstant.PAGE_EVENT_UPDATE;
	}

	/**
	 * The method use to update the user
	 *
	 * @param user
	 * @param result
	 * @param redirectAttributes
	 * @return {@link String} Redirect to respective Get request
	 */
	@RequestMapping(value = UrlConstant.URL_UPDATE, method = RequestMethod.POST)
	public String update(@ModelAttribute("event") InstitutionEvent event, BindingResult result,
			RedirectAttributes redirectAttributes) {
		LOG.info("Inside InstitutionalAdminEventMgmtController#update method");
		InstitutionEvent event1 = eventManagementServiceImpl.findById(event.getId());
		if (event1 == null) {
			redirectAttributes.addFlashAttribute("info",
					messageSource.getMessage("exit.not.event", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_EVENT_MGMT;
		}
		AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		event1.setInstitution(auth.getUser().getInstitution());
		event1.setNameEvent(event.getNameEvent());
		event1.setDateFrom(event.getDateFrom());
		event1.setDateTo(event.getDateTo());
		event1.setEnabled(true);
		validatorUtil.validate(event1, result);
		if (result.hasErrors()) {
			LOG.debug("User validation errors : {}", result);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.event", result);
			redirectAttributes.addFlashAttribute("event", event1);
			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("error.event.update", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_EVENT_MGMT + UrlConstant.URL_EDIT + "/"
					+ event1.getId();
		}
		try {
			eventManagementServiceImpl.update(event1);
			redirectAttributes.addFlashAttribute("success", messageSource.getMessage("success.event.update",
					new Object[] { event1.getNameEvent() }, Locale.getDefault()));
			LOG.info("Evemt updated successfully {}", event1);
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_EVENT_MGMT;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("event", event1);

			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("exception.event.update", null, Locale.getDefault()));
			LOG.error("Cannot update event due to following error : {} ", e);
		}
		return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_EVENT_MGMT + UrlConstant.URL_EDIT + "/" + event1.getId();
	}

	/**
	 * Delete event
	 *
	 * @param id
	 * @param action
	 * @param redirectAttributes
	 * @return String
	 */
	@RequestMapping(value = UrlConstant.URL_STATUS, method = RequestMethod.POST)
	public String status(@RequestParam("id") Long id, RedirectAttributes redirectAttributes) {
		LOG.info("Inside InstitutionUserController#status method: {}", id);
		InstitutionEvent event = eventManagementServiceImpl.findById(id);
		if (event == null) {
			redirectAttributes.addFlashAttribute("info",
					messageSource.getMessage("exist.not.event", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_EVENT_MGMT;
		}
		if (event.getEnabled())
			event.setEnabled(Boolean.FALSE);
		else
			event.setEnabled(Boolean.TRUE);
		try {
			eventManagementServiceImpl.update(event);
			LOG.info("Changing status of event : {}", event);
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("exception.event.status", null, Locale.getDefault()));
			LOG.error("Cannot change status of event due to {}", e);
		}
		return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_EVENT_MGMT + UrlConstant.URL_VIEW + "/" + event.getId();
	}
}
