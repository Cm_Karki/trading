package com.sharesansar.trading.controller.institution.admin;

import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.constant.UrlConstant;
import com.sharesansar.trading.entity.AuditUser;
import com.sharesansar.trading.entity.InterestRate;
import com.sharesansar.trading.service.InterestRateService;
import com.sharesansar.trading.util.ValidatorUtil;

@Controller
@Secured(value = { RoleConstant.INSTITUTION_ADMIN })
@RequestMapping(value = UrlConstant.URL_INSTITUTION_ADMIN_SETTING_INTEREST_RATE)
public class InterestRateController {

	private static final Logger LOG = LogManager.getLogger(InterestRateController.class);;

	@Autowired
	private InterestRateService interestRateServiceImpl;

	@Autowired
	private ValidatorUtil validatorUtil;

	@Autowired
	private MessageSource messageSource;

	@RequestMapping(value = UrlConstant.URL_INSTITUTION_ADMIN_SETTING_INTEREST_RATE_AJAX, method = RequestMethod.GET)
	public String getModal(Model model) {
		
		LOG.info("Inside InterestRateController#getModal method");
		AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<InterestRate> interestRates = interestRateServiceImpl
				.findAllByInstitution(auth.getUser().getInstitution());
		if (interestRates.size() != 0) {
			model.addAttribute("interestRate", interestRates.get(0));
		}
			
		else {
			model.addAttribute("interestRate", new InterestRate());
		}
		return PageConstant.PAGE_INSTITUTION_ADMIN_INTEREST_RATE;
	}

	@RequestMapping(value = UrlConstant.URL_INSTITUTION_ADMIN_SETTING_INTEREST_RATE_SAVE_AJAX, method = RequestMethod.POST)
	public String save(@ModelAttribute("interestRate") InterestRate interestRate, BindingResult result, Model model) {
		LOG.info("Inside InterestRateController#save method  interest {}", interestRate.getInterest());
		InterestRate interestRate1 = null;
		AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (interestRate.getId() != 0) {
			interestRate1 = interestRateServiceImpl.findById(interestRate.getId());
			interestRate1.setInterest(interestRate.getInterest());
		} else {
			interestRate1 = new InterestRate();
			interestRate1.setInstitution(auth.getUser().getInstitution());
			interestRate1.setInterest(interestRate.getInterest());
		}
		interestRate.setInstitution(auth.getUser().getInstitution());
		validatorUtil.validate(interestRate, result);
		if (result.hasErrors()) {
			model.addAttribute("org.springframework.validation.BindingResult.interestRate", result);
			model.addAttribute("interestRate", interestRate);
			model.addAttribute("error",
					messageSource.getMessage("error.interestRate.create", null, Locale.getDefault()));
		} else {
			try {
				interestRateServiceImpl.save(interestRate1);

				model.addAttribute("success",
						messageSource.getMessage("error.interestRate.create", null, Locale.getDefault()));
				LOG.info("Interest Rate created Successfully.");
			} catch (Exception e) {
				model.addAttribute("interestRate", interestRate);
				model.addAttribute("error",
						messageSource.getMessage("exception.interestRate.create", null, Locale.getDefault()));

				LOG.error("Cannot create user due to following error : {} ", e);
			}
		}
		return PageConstant.PAGE_INSTITUTION_ADMIN_INTEREST_RATE;
	}

}
