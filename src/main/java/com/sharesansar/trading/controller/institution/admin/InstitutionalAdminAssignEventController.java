package com.sharesansar.trading.controller.institution.admin;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.constant.UrlConstant;
import com.sharesansar.trading.entity.AuditUser;
import com.sharesansar.trading.entity.InstitutionEvent;
import com.sharesansar.trading.entity.auth.User;
import com.sharesansar.trading.object.ResponseData;
import com.sharesansar.trading.service.EventManagementService;
import com.sharesansar.trading.service.UserService;

@Controller
@Secured(value = { RoleConstant.INSTITUTION_ADMIN })
@RequestMapping(value = UrlConstant.URL_INSTITUTION_ADMIN_ASSIGN_EVENT)
public class InstitutionalAdminAssignEventController {

	private static final Logger LOG = LogManager.getLogger(InstitutionalAdminAssignEventController.class);

	@Autowired
	private EventManagementService eventManagementServiceImpl;

	@Autowired
	private UserService userServiceImpl;

	/**
	 * method to map event assign request
	 * 
	 * @param identity
	 * @param model
	 * @return
	 */
	@RequestMapping(method = RequestMethod.GET)
	public String getAssignEvent(@RequestParam(name = "identity", required = false, defaultValue = "0") long identity,
			Model model) {
		LOG.info("Inside InstitutionalAdminAssignEventController#getAssignEvent method");
		AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<InstitutionEvent> events = eventManagementServiceImpl.findByIstitution(auth.getUser().getInstitution());
		// If event is empty redirect to event create page
		if (events == null || events.isEmpty())
			return "redirect:" + UrlConstant.URL_INSTITUTION_ADMIN_EVENT_MGMT + UrlConstant.URL_CREATE;
		// If identity is 0 or event is undetermined by identity in case
		// use default event other wise proceed
		InstitutionEvent event;

		event = eventManagementServiceImpl.findByIdAndInstitution(identity, auth.getUser().getInstitution());
		event = event == null ? events.get(0) : event;
		model.addAttribute("events", events);
		model.addAttribute("event", event);
		model.addAttribute("identity", event.getId());
		return PageConstant.PAGE_INSTITUTION_ADMIN_ASSIGN_EVENT;
	}

	/**
	 * Find all user of given institution add assign them event
	 * 
	 * @param eventId
	 * @param model
	 * @return
	 */
	@RequestMapping(value = UrlConstant.URL_INSTITUTION_ADMIN_ASSIGN_EVENT_MODAL_AJAX, method = RequestMethod.GET)
	public String getUsers(@RequestParam("id") Long eventId, Model model) {
		LOG.info("Inside InstitutionAdminAssignEventController#getUsers method eventId {}", eventId);
		AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<User> user = userServiceImpl.findByInstitutionAndRoles(auth.getUser().getInstitution(),
				RoleConstant.NORMAL_USER);
		model.addAttribute("eventObj", eventManagementServiceImpl.findById(eventId));
		System.err.println("size of user==== " + user.size());
		model.addAttribute("users", user);
		return PageConstant.PAGE_INSTITUTION_ADMIN_ASSIGN_EVENT_MODAL;
	}

	@RequestMapping(value = UrlConstant.URL_INSTITUTION_ADMIN_ASSIGN_EVENT_SAVE_AJAX, method = RequestMethod.GET)
	public @ResponseBody ResponseData save(@RequestParam("userIds[]") Long[] userIds,
			@RequestParam("eventId") Long eventId, Model model) {
		LOG.info("Inside InstitutionAdminAssignEventController#save method userIds {}, eventId {}", userIds, eventId);
		InstitutionEvent event = eventManagementServiceImpl.findById(eventId);
		System.out.println("------------------ "+event);
		ResponseData data = new ResponseData();
		if (event != null) {
			List<User> users = new ArrayList<User>();

			for (Long id : userIds) {
				User user = userServiceImpl.findById(id);
				if (user != null) {
					users.add(user);
				}
			}
			event.setUsers(users);
			InstitutionEvent event1 = eventManagementServiceImpl.save(event);
			System.out.println("-------------------- "+event1.getUsers().toString());
			data.setMessage("SUCCESS");
			data.setUrl("/institution/admin/settings/event-assign?identity=" + eventId);
		} else {
			data.setMessage("ERROR");
			data.setUrl("");
		}
		return data;

	}
}