package com.sharesansar.trading.controller.institution.admin;

import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.constant.UrlConstant;
import com.sharesansar.trading.entity.AuditUser;
import com.sharesansar.trading.entity.MoneyAllocation;
import com.sharesansar.trading.service.MoneyAllocationService;
import com.sharesansar.trading.util.ValidatorUtil;

@Controller
@Secured(value = { RoleConstant.INSTITUTION_ADMIN })
@RequestMapping(value = UrlConstant.URL_INSTITUTION_ADMIN_SETTING_MONEY)
public class MoneyAllocationController {

	private static final Logger LOG = LogManager.getLogger(MoneyAllocationController.class);

	@Autowired
	private MoneyAllocationService moneyAllocationServiceImpl;

	@Autowired
	private ValidatorUtil validatorUtil;

	@Autowired
	private MessageSource messageSource;

	@RequestMapping(value = UrlConstant.URL_INSTITUTION_ADMIN_SETTING_MONEY_AJAX, method = RequestMethod.GET)
	public String getModal(Model model) {
		LOG.info("Inside MoneyAllocationController#getModal method");
		AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		List<MoneyAllocation> moneys = moneyAllocationServiceImpl.findAllByInstitution(auth.getUser().getInstitution());
		if (moneys.size() != 0)
			model.addAttribute("money", moneys.get(0));
		else {
			model.addAttribute("money", new MoneyAllocation());
		}
		return PageConstant.PAGE_INSTITUTION_ADMIN_MONEY_ALLOCATION;
	}

	@RequestMapping(value = UrlConstant.URL_INSTITUTION_ADMIN_SETTING_MONEY_SAVE_AJAX, method = RequestMethod.POST)
	public String save(@ModelAttribute("money") MoneyAllocation money, BindingResult result, Model model) {
		LOG.info("Inside MoneyAllocationController#save method  money {}", money.getId());
		MoneyAllocation money1 = null;
		AuditUser auth = (AuditUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (money.getId() != 0) {
			money1 = moneyAllocationServiceImpl.findById(money.getId());
			money1.setAmount(money.getAmount());
		} else {
			money1 = new MoneyAllocation();
			money1.setInstitution(auth.getUser().getInstitution());
			money1.setAmount(money.getAmount());
		}
		money.setInstitution(auth.getUser().getInstitution());
		validatorUtil.validate(money, result);
		if (result.hasErrors()) {
			model.addAttribute("org.springframework.validation.BindingResult.money", result);
			model.addAttribute("money", money);
			model.addAttribute("error", messageSource.getMessage("error.money.create", null, Locale.getDefault()));
		} else {
			try {
				moneyAllocationServiceImpl.save(money1);

				model.addAttribute("success",
						messageSource.getMessage("success.money.create", null, Locale.getDefault()));
				LOG.info("Money created Successfully.");
			} catch (Exception e) {
				model.addAttribute("money", money);
				model.addAttribute("error",
						messageSource.getMessage("exception.money.create", null, Locale.getDefault()));

				LOG.error("Cannot create user due to following error : {} ", e);
			}
		}
		return PageConstant.PAGE_INSTITUTION_ADMIN_MONEY_ALLOCATION;
	}

}
