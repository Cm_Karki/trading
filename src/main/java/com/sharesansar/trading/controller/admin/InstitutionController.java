package com.sharesansar.trading.controller.admin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.HtmlUtils;

import com.sharesansar.trading.constant.AppConstant;
import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.constant.UrlConstant;
import com.sharesansar.trading.entity.Institution;
import com.sharesansar.trading.expection.NameAlreadyExistException;
import com.sharesansar.trading.object.InstitutionJsonResponse;
import com.sharesansar.trading.service.InstitutionService;
import com.sharesansar.trading.util.JtableJSONResponse;
import com.sharesansar.trading.util.JtableRequestData;
import com.sharesansar.trading.util.ValidatorUtil;

@Controller
@Secured(RoleConstant.APP_ADMIN)
@RequestMapping(value = UrlConstant.URL_APP_ADMIN_INSTITUTION)
public class InstitutionController {

	private static final Logger LOG = LogManager.getLogger(InstitutionController.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private InstitutionService institutionServiceImpl;

	@Autowired
	private ValidatorUtil validatorUtil;

	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
		// date formatter
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

	@RequestMapping(value = UrlConstant.URL_LIST, method = RequestMethod.GET)
	public String getInstitutionList(Model model) {
		LOG.info("Inside InstitutionController#getUserList method");
		return PageConstant.PAGE_APP_ADMIN_INSTITUTION_LIST;

	}

	/**
	 * Method for getting a list of institution
	 * 
	 * @param jtableRequestData
	 * @return {@link JtableJSONResponse} of {@link InstitutionJsonResponse}
	 */
	@ResponseBody
	@RequestMapping(value = UrlConstant.URL_LIST_AJAX, method = RequestMethod.GET)
	public JtableJSONResponse<InstitutionJsonResponse> institutions(@ModelAttribute JtableRequestData jtableRequestData,
			@RequestParam(name = "fields[]", required = false) String[] fields,
			@RequestParam(name = "queries[]", required = false) String[] queries) {
		LOG.info("Inside InstitutionController#institutions method : Search queries {} and fields {}", queries, fields);
		JtableJSONResponse<InstitutionJsonResponse> jtableJSONResponse = new JtableJSONResponse<InstitutionJsonResponse>();
		try {
			List<Institution> institutions = institutionServiceImpl.getAllInstitution(fields, queries,
					jtableRequestData);
			jtableJSONResponse.setTotalRecordCount(institutionServiceImpl.countAllInstitution(fields, queries));
			jtableJSONResponse.setResult(AppConstant.ALL_CAPS_OK);
			jtableJSONResponse.setRecords(getInstitutionJsonResponses(institutions));
		} catch (Exception e) {
			LOG.error("Error while getting institution list {}", e);
			jtableJSONResponse.setResult(AppConstant.ALL_CAPS_ERROR);
			jtableJSONResponse
					.setMessage(messageSource.getMessage("exception.institution.fetch", null, Locale.getDefault()));
		}
		return jtableJSONResponse;
	}

	/**
	 * <p>
	 * Method for getting {@link List} of {@link InstitutionJsonResponse}. The list
	 * is used as response data for institution jtable.
	 * </p>
	 * 
	 * @param institutions
	 *            {@link List} of {@link Institution} The objects containing data
	 *            required to build {@link InstitutionJsonResponse}.
	 * @return {@link List} of {@link InstitutionJsonResponse} contains response
	 *         data for jtable
	 */
	private List<InstitutionJsonResponse> getInstitutionJsonResponses(List<Institution> institutions) {
		LOG.info("Inside InstitutionController#getInstitutionJsonResponses method.");
		List<InstitutionJsonResponse> institutionJsonResponses = new ArrayList<InstitutionJsonResponse>();
		for (Institution institution : institutions) {
			InstitutionJsonResponse institutionJsonResponse = new InstitutionJsonResponse();
			institutionJsonResponse.setAddress(HtmlUtils.htmlEscape(institution.getAddress()));
			institutionJsonResponse.setContactNumber(institution.getContactNo());
			institutionJsonResponse.setEmail(HtmlUtils.htmlEscape(institution.getEmail()));
			institutionJsonResponse.setExpiredDate(institution.getExpiredDate());
			institutionJsonResponse.setId(institution.getId());
			institutionJsonResponse.setMaxUserLimit(institution.getMaxUserLimit());
			institutionJsonResponse.setName(HtmlUtils.htmlEscape(institution.getName()));
			institutionJsonResponses.add(institutionJsonResponse);
			System.out.println("--------------- "+institutionJsonResponses.size());
		}
		return institutionJsonResponses;
	}

	@RequestMapping(value = UrlConstant.URL_CREATE, method = RequestMethod.GET)
	public String create(Model model) {
		LOG.info("Inside InstitutionController#create method");
		if (!model.containsAttribute("institution"))
			model.addAttribute("institution", new Institution());
		return PageConstant.PAGE_APP_ADMIN_INSTITUTION_CREATE;
	}

	@RequestMapping(value = UrlConstant.URL_CREATE, method = RequestMethod.POST)
	public String save(@ModelAttribute("institution") Institution institution, BindingResult result,
			RedirectAttributes redirectAttributes, Model model) {
		LOG.info("Inside InstitutionController#save method");
		validatorUtil.validate(institution, result);
		if (result.hasErrors()) {
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.institution", result);
			redirectAttributes.addFlashAttribute("institution", institution);
			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("error.institution.create", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_APP_ADMIN_INSTITUTION + UrlConstant.URL_CREATE;
		}
		try {
			institutionServiceImpl.isUnique(institution.getName(), institution.getId());
			institutionServiceImpl.save(institution);
			redirectAttributes.addFlashAttribute("success", messageSource.getMessage("success.institution.create",
					new Object[] { institution.getName() }, Locale.getDefault()));
			LOG.info("Instituiton created Successfully");
			return "redirect:" + UrlConstant.URL_APP_ADMIN_INSTITUTION + UrlConstant.URL_LIST;

		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("institution", institution);
			if (e instanceof NameAlreadyExistException) {
				result.rejectValue("name", "Unique.institution.name");
				redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.institution",
						result);
				redirectAttributes.addFlashAttribute("error",
						messageSource.getMessage("error.institution.create", null, Locale.getDefault()));
			} else
				redirectAttributes.addFlashAttribute("error",
						messageSource.getMessage("exception.institution.create", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_APP_ADMIN_INSTITUTION + UrlConstant.URL_CREATE;
		}

	}

	/**
	 * Method for getting a Institution object with specified id for view
	 * 
	 * @param id
	 * @param redirectAttributes
	 * @param model
	 * @return {@link String} Institution view page
	 */
	@RequestMapping(value = UrlConstant.URL_VIEW_ID, method = RequestMethod.GET)
	public String view(@PathVariable("id") Long id, RedirectAttributes redirectAttributes, Model model) {
		LOG.info("Inside InstitutionController#view method: {}", id);
		Institution institution = institutionServiceImpl.findById(id);
		if (institution == null) {
			redirectAttributes.addFlashAttribute("info",
					messageSource.getMessage("exist.not.institution", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_APP_ADMIN_INSTITUTION + UrlConstant.URL_LIST;
		}
		model.addAttribute("institution", institution);
		return PageConstant.PAGE_APP_ADMIN_INSTITUTION_VIEW;
	}

	/**
	 * Method for getting institution to edit
	 * 
	 * @param id
	 * @param redirectAttributes
	 * @param model
	 * @return {@link String} Institution edit page
	 */
	@RequestMapping(value = UrlConstant.URL_EDIT_ID, method = RequestMethod.GET)
	public String edit(@PathVariable("id") long id, RedirectAttributes redirectAttributes, Model model) {
		LOG.info("Inside InstitutionController#edit method");
		Institution institution = institutionServiceImpl.findById(id);
		if (institution == null) {
			redirectAttributes.addFlashAttribute("info",
					messageSource.getMessage("exist.not.institution", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_APP_ADMIN_INSTITUTION + UrlConstant.URL_LIST;
		}
		if (!model.containsAttribute("institution")) {
			model.addAttribute("institution", institution);
		}
		return PageConstant.PAGE_APP_ADMIN_INSTITUTION_EDIT;
	}

	/**
	 * The method use to update the institution
	 * 
	 * @param institution
	 * @param result
	 * @param redirectAttributes
	 * @return {@link String} Redirect to respective Get request
	 */
	@RequestMapping(value = UrlConstant.URL_UPDATE, method = RequestMethod.POST)
	public String update(@ModelAttribute("institution") Institution institution, BindingResult result,
			RedirectAttributes redirectAttributes) {
		LOG.info("Inside InstitutionController#update method");
		Institution institution1 = institutionServiceImpl.findById(institution.getId());
		if (institution1 == null) {
			redirectAttributes.addFlashAttribute("info",
					messageSource.getMessage("exist.not.institution", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_APP_ADMIN_INSTITUTION + UrlConstant.URL_LIST;
		}
		institution1.setName(institution.getName());
		institution1.setContactNo(institution.getContactNo());
		institution1.setAddress(institution.getAddress());
		institution1.setEmail(institution.getEmail());
		institution1.setExpiredDate(institution.getExpiredDate());
		institution1.setMaxUserLimit(institution.getMaxUserLimit());
		validatorUtil.validate(institution1, result);
		if (result.hasErrors()) {
			LOG.debug("Institution validation errors : {}", result);
			redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.institution", result);
			redirectAttributes.addFlashAttribute("institution", institution1);
			redirectAttributes.addFlashAttribute("error",
					messageSource.getMessage("error.institution.update", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_APP_ADMIN_INSTITUTION + UrlConstant.URL_EDIT + "/"
					+ institution1.getId();
		}
		try {
			institutionServiceImpl.isUnique(institution1.getName(), institution1.getId());
			institutionServiceImpl.update(institution1);
			redirectAttributes.addFlashAttribute("success", messageSource.getMessage("success.institution.update",
					new Object[] { institution1.getName() }, Locale.getDefault()));
			LOG.info("Institution update successfully {}", institution1);
			return "redirect:" + UrlConstant.URL_APP_ADMIN_INSTITUTION + UrlConstant.URL_LIST;
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("institution", institution1);
			if (e instanceof NameAlreadyExistException) {
				result.rejectValue("name", "Unique.institution.name");
				redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.institution",
						result);
				redirectAttributes.addFlashAttribute("error",
						messageSource.getMessage("error.institution.update", null, Locale.getDefault()));
				LOG.debug("Institution validation errors : {}", result);
			} else {
				redirectAttributes.addFlashAttribute("error",
						messageSource.getMessage("exception.institution.update", null, Locale.getDefault()));
				LOG.error("Cannot create institution due to following error : {} ", e);
			}
			return "redirect:" + UrlConstant.URL_APP_ADMIN_INSTITUTION + UrlConstant.URL_EDIT + "/"
					+ institution1.getId();
		}
	}

	/**
	 * Delete {@link Institution}
	 * 
	 * @param id
	 * @param redirectAttributes
	 * @return
	 */
	@RequestMapping(value = UrlConstant.URL_DELETE, method = RequestMethod.POST)
	public String delete(@RequestParam("id") long id, RedirectAttributes redirectAttributes) {
		LOG.info("Inside InstitutionController#delete method . Deleting id {}", id);
		Institution institution = institutionServiceImpl.findById(id);
		if (institution == null) {
			redirectAttributes.addFlashAttribute("info",
					messageSource.getMessage("exist.not.institution", null, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_APP_ADMIN_INSTITUTION + UrlConstant.URL_LIST;
		}
		try {
			institutionServiceImpl.delete(institution);
			redirectAttributes.addFlashAttribute("success", messageSource.getMessage("success.institution.delete",
					new Object[] { institution.getName() }, Locale.getDefault()));
			LOG.info("institution deleted successfully. Id {}", id);
			return "redirect:" + UrlConstant.URL_APP_ADMIN_INSTITUTION + UrlConstant.URL_LIST;
		} catch (Exception e) {
			LOG.info("Cannot delete institution due to {}", e);
			if (e instanceof DataIntegrityViolationException)
				redirectAttributes.addFlashAttribute("error",
						messageSource.getMessage("foreignKeyException.institution.delete",
								new Object[] { institution.getName() }, Locale.getDefault()));
			else
				redirectAttributes.addFlashAttribute("error", messageSource.getMessage("exception.institution.delete",
						new Object[] { institution.getName() }, Locale.getDefault()));
			return "redirect:" + UrlConstant.URL_APP_ADMIN_INSTITUTION + UrlConstant.URL_VIEW + "/"
					+ institution.getId();
		}
	}
}
