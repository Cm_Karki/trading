package com.sharesansar.trading.controller.admin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.constant.UrlConstant;

@Controller
@Secured({ RoleConstant.APP_ADMIN })
@RequestMapping(value = UrlConstant.URL_APP_ADMIN_DASHBOARD)
public class AppAdminDashboardController {

	private static final Logger LOG = LogManager.getLogger(AppAdminDashboardController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String getAdminDashdoard(Model model) {
		LOG.info("Inside AdminDashboardController#getAdminDashboard method");
		return PageConstant.PAGE_APP_ADMIN_DASHBOARD;
	}
}
