package com.sharesansar.trading.controller.admin;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.constant.UrlConstant;
import com.sharesansar.trading.entity.Company;
import com.sharesansar.trading.object.NEPSECompany;
import com.sharesansar.trading.service.CompanyService;
import com.sharesansar.trading.util.ApiUtil;
import com.sharesansar.trading.util.NEPSECompaniesCruler;

@Controller
@Secured(value = RoleConstant.APP_ADMIN)
@RequestMapping(value = UrlConstant.URL_APP_ADMIN_COMPANIES_SYNC)
public class CompaniesSyncController {

	private static final Logger LOG = LogManager.getLogger(CompaniesSyncController.class);

	@Value("${base.url}")
	private String baseUrl;

	@Value("${api.key}")
	private String key;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private CompanyService companyServiceImpl;

	@RequestMapping(value = UrlConstant.URL_APP_ADMIN_COMPANIES_SYNC_AJAX, method = RequestMethod.GET)
	public String getSyncCompaniesModal(Model model) {
		LOG.info("Inside CompaniesSyncController#syncCompanies method");
		return PageConstant.PAGE_APP_ADMIN_COMPANIES_SYNC_MODAL;
	}

	@RequestMapping(value = UrlConstant.URL_APP_ADMIN_COMPANIES_SYNC_START_AJAX, method = RequestMethod.GET)
	public String startCompaniesSync(Model model) {
		LOG.info("Inside CompaniesSyncController#startCompaniesSync method");
		List<Company> companyList = new ArrayList<Company>();
		List<NEPSECompany> sharesansarCompanies = ApiUtil.getCompanyList(baseUrl, key);
		List<NEPSECompany> nepseCompanies = NEPSECompaniesCruler.getNEPSECompanies();
		try {
			for (NEPSECompany nepseCompany : nepseCompanies) {
				NEPSECompany company = sharesansarCompanies.stream()
						.filter(sharesansarCompany -> nepseCompany.getSymbol().equals(sharesansarCompany.getSymbol()))
						.findAny().orElse(null);
				if (company != null) {
					Company com = new Company();
					com.setIdentity(company.getId());
					com.setName(company.getName());
					com.setSymbol(company.getSymbol());
					companyList.add(com);
				}
			}
			companyServiceImpl.save(companyList);
			model.addAttribute("success", messageSource.getMessage("success.company.sync", null, Locale.getDefault()));
			LOG.info("Comapany sync completed successfully");
		} catch (Exception e) {
			model.addAttribute("error", messageSource.getMessage("error.company.sync", null, Locale.getDefault()));
		}
		return PageConstant.PAGE_APP_ADMIN_COMPANIES_SYNC_MODAL;
	}
}
