package com.sharesansar.trading.controller.admin;

import java.util.Collection;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.UrlConstant;
import com.sharesansar.trading.service.AdminRoleService;
import com.sharesansar.trading.util.ValidatorUtil;

@Controller
@RequestMapping(value = UrlConstant.URL_APP_ADMIN)
public class AppAdminLoginController {

	private static final Logger LOG = LogManager.getLogger(AppAdminLoginController.class);

	@Autowired
	private ValidatorUtil validatorUtil;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private AdminRoleService adminRoleServiceImpl;

	@RequestMapping(value = UrlConstant.URL_APP_ADMIN_LOGIN, method = RequestMethod.GET)
	public String getLoginPage(Model model) {
		LOG.info("Inside AdminLoginController#getLoginPage method");
		@SuppressWarnings("unchecked")
		Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>) SecurityContextHolder
				.getContext().getAuthentication().getAuthorities();
		System.out.println(authorities);
		if (adminRoleServiceImpl.isValidAuthorities(authorities))
			return "redirect:" + UrlConstant.URL_APP_ADMIN_DASHBOARD + "/";
		return PageConstant.PAGE_APP_ADMIN_LOGIN;
	}

	@RequestMapping(value = UrlConstant.URL_APP_ADMIN_LOGIN_FAIL, method = RequestMethod.GET)
	public String getLoginFailPage(HttpServletRequest request, Model model) {
		LOG.info("Inside UserLoginController#getLoginFailPage method");
		AuthenticationException exception = (AuthenticationException) request.getSession()
				.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
		if (exception instanceof BadCredentialsException)
			model.addAttribute("authFailed",
					messageSource.getMessage("login.invalid.credentials", null, Locale.getDefault()));
		if (exception instanceof DisabledException)
			model.addAttribute("authFaild",
					messageSource.getMessage("login.account.dissable", null, Locale.getDefault()));
		else
			model.addAttribute("authFaild", messageSource.getMessage("login.invalid", null, Locale.getDefault()));

		return PageConstant.PAGE_APP_ADMIN_LOGIN;
	}
}
