package com.sharesansar.trading.service;

import java.util.List;

import com.sharesansar.trading.entity.Company;

public interface CompanyService {

	/**
	 * 
	 * @param l
	 * @return
	 */
	Company findById(long l);

	/**
	 * 
	 * @param companyList
	 */
	void save(List<Company> companyList);

	/**
	 * 
	 * @return
	 */
	List<Company> findAll();

}
