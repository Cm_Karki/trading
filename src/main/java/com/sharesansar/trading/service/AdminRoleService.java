package com.sharesansar.trading.service;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

public interface AdminRoleService {

	/**
	 * Check whether login user has valid authorities or not
	 * 
	 * @param authorities
	 * @return boolean
	 */
	public boolean isValidAuthorities(Collection<? extends GrantedAuthority> authorities) ;
}
