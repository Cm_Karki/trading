package com.sharesansar.trading.service;

import com.sharesansar.trading.entity.auth.AppAdmin;

public interface AppAdminService {

	/**
	 * 
	 * @param username
	 * @return
	 */
	AppAdmin findByUserName(String username);

}
