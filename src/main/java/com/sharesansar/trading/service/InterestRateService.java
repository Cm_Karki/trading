package com.sharesansar.trading.service;

import java.util.List;

import com.sharesansar.trading.entity.Institution;
import com.sharesansar.trading.entity.InterestRate;

public interface InterestRateService {

	public List<InterestRate> findAllByInstitution(Institution institution);

	public InterestRate findById(long id);

	public InterestRate save(InterestRate interestRate1);

	
}
