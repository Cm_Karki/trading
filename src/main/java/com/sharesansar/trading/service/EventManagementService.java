package com.sharesansar.trading.service;

import java.util.List;

import com.sharesansar.trading.entity.Institution;
import com.sharesansar.trading.entity.InstitutionEvent;
import com.sharesansar.trading.entity.auth.User;
import com.sharesansar.trading.util.JtableRequestData;

public interface EventManagementService {

	/**
	 * get all the events of institution
	 * 
	 * @param fields
	 * @param queries
	 * @param jtableRequestData
	 * @param institution
	 * @return
	 */
	List<InstitutionEvent> findAllUserByInstitution(String[] fields, String[] queries,
			JtableRequestData jtableRequestData, Institution institution);

	/**
	 * count total live no of available event of given institution
	 * 
	 * @param fields
	 * @param queries
	 * @param institution
	 * @return
	 */
	long countAllEventByInstitution(String[] fields, String[] queries, Institution institution);

	/**
	 * 
	 * @param event
	 */
	InstitutionEvent save(InstitutionEvent event);

	/**
	 * 
	 * @param id
	 * @return
	 */
	InstitutionEvent findById(Long id);

	/**
	 * 
	 * @param event1
	 */
	void update(InstitutionEvent event1);

	/**
	 * method to get all created institution belongs to given institution
	 * 
	 * @param institution
	 * @retrun {@List of InstitutionEvent}
	 */
	List<InstitutionEvent> findByIstitution(Institution institution);

	/**
	 * 
	 * @param identity
	 * @param institution
	 * @return
	 */
	InstitutionEvent findByIdAndInstitution(long identity, Institution institution);

}
