package com.sharesansar.trading.service;

import com.sharesansar.trading.entity.auth.UserDetail;

public interface UserDetailService {

	UserDetail findByName(String name);

	void update(UserDetail userDetail1);

	

}
