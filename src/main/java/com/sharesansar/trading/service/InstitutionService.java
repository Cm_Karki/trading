package com.sharesansar.trading.service;

import java.util.List;

import com.sharesansar.trading.entity.Institution;
import com.sharesansar.trading.expection.NameAlreadyExistException;
import com.sharesansar.trading.util.JtableRequestData;

public interface InstitutionService {

	/**
	 * method to find all institute
	 * 
	 * @param fields
	 * @param queries
	 * @param jtableRequestData
	 * @return
	 */
	List<Institution> getAllInstitution(String[] fields, String[] queries, JtableRequestData jtableRequestData);

	/**
	 * method to count total number of institution
	 * 
	 * @param fields
	 * @param queries
	 * @return
	 */
	long countAllInstitution(String[] fields, String[] queries);

	/**
	 * 
	 * @param name
	 * @param id
	 * @throws NameAlreadyExistException
	 */
	void isUnique(String name, long id) throws NameAlreadyExistException;

	/**
	 * 
	 * @param institution
	 * @return
	 */
	Institution save(Institution institution);

	/**
	 * 
	 * @param id
	 * @return
	 */
	Institution findById(Long id);

	/**
	 * 
	 * @param institution1
	 */
	void update(Institution institution1);

	/**
	 * 
	 * @param institution
	 */
	void delete(Institution institution);

	/**
	 * 
	 * @return List Institution.
	 */
	List<Institution> findAll();

	/**
	 * 
	 * @param true1
	 * @param institutionName
	 */
	Institution findByIsMain(Boolean true1);

}
