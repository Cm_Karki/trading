package com.sharesansar.trading.service;

import java.util.List;

import com.sharesansar.trading.constant.OrderStatus;
import com.sharesansar.trading.constant.OrderType;
import com.sharesansar.trading.entity.Order;
import com.sharesansar.trading.entity.auth.User;

public interface OrderService {

	/**
	 * 
	 * @param order
	 * @return
	 */
	Order save(Order order);

	/**
	 * 
	 * @param user
	 * @param live
	 * @param buy
	 * @return
	 */
	List<Order> findByUserAndStatusAndOrderType(User user, OrderStatus live, OrderType buy);

}
