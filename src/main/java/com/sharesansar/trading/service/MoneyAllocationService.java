package com.sharesansar.trading.service;

import java.util.List;

import com.sharesansar.trading.entity.Institution;
import com.sharesansar.trading.entity.MoneyAllocation;

public interface MoneyAllocationService {

	/**
	 * 
	 * @param l
	 * @return
	 */
	MoneyAllocation findById(long l);

	/**
	 * 
	 * @param money1
	 * @return
	 */
	MoneyAllocation save(MoneyAllocation money1);

	/**
	 * 
	 * @return
	 */
	List<MoneyAllocation> findAllByInstitution(Institution institution);

}
