package com.sharesansar.trading.service;

import java.util.List;

import com.sharesansar.trading.entity.FixedDeposit;
import com.sharesansar.trading.util.JtableRequestData;

/**
 * Service for {@link FixedDeposit}
 * 
 * @author aleeza
 *
 */
public interface FixedDepositService {
	
	public FixedDeposit save(FixedDeposit fixeddeposit);

	public List<FixedDeposit> getAllFixedDeposit(String[] fields, String[] queries, JtableRequestData jtableRequestData);
		
	public long countAllUser(String[] fields, String[] queries);

	public FixedDeposit findById(Long id);

	public Object findAll();

	public void update(FixedDeposit fixedDeposit1);

	public void delete(FixedDeposit fixedDeposit);



	
	
}
