package com.sharesansar.trading.service;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;

import com.sharesansar.trading.entity.auth.Role;

public interface RoleService {

	/**
	 * 
	 * @return List Role
	 */
	List<Role> findAll();

	/**
	 * 
	 * @param authorities
	 * @return
	 */
	boolean isValidAuthorities(Collection<? extends GrantedAuthority> authorities);

	/**
	 * 
	 * @param authorities
	 * @return
	 */
	boolean isInstitutionalUser(Collection<? extends GrantedAuthority> authorities);

	/**
	 * 
	 * @param authorities
	 * @return
	 */
	boolean isNormalUser(Collection<? extends GrantedAuthority> authorities);

	/**
	 * 
	 * @param userRole
	 * @return
	 */
	List<Role> findAllByName(String userRole);

	Role findByName(String normalUser);

}
