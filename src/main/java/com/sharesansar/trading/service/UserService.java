package com.sharesansar.trading.service;

import java.util.List;

import com.sharesansar.trading.entity.Institution;
import com.sharesansar.trading.entity.auth.Role;
import com.sharesansar.trading.entity.auth.User;
import com.sharesansar.trading.expection.NameAlreadyExistException;
import com.sharesansar.trading.util.JtableRequestData;

public interface UserService {

	/**
	 * 
	 * @param fields
	 * @param queries
	 * @param jtableRequestData
	 * @return
	 */
	List<User> getAllUser(String[] fields, String[] queries, JtableRequestData jtableRequestData);

	/**
	 * 
	 * @param fields
	 * @param queries
	 * @return
	 */
	long countAllUser(String[] fields, String[] queries);

	/**
	 * 
	 * @param userName
	 * @param id
	 * @throws NameAlreadyExistException
	 */
	void isUnique(String userName, long id) throws NameAlreadyExistException;

	/**
	 * 
	 * @param user
	 */
	User save(User user);

	/**
	 * 
	 * @param user
	 * @param passwordResetUrl
	 */
	void sendUserCreateEmail(User user, String passwordResetUrl);

	/**
	 * 
	 * @param id
	 * @return
	 */
	User findById(Long id);

	/**
	 * 
	 * @param user
	 * @param token
	 * @return
	 */
	boolean verifyPasswordResetToken(User user, String token);

	/**
	 * 
	 * @param user1
	 */
	void update(User user1);

	/**
	 * 
	 * @param user1
	 * @param prevRole
	 */
	void sendUserRoleChangeEmail(User user1, String prevRole);

	/**
	 * 
	 * @param user
	 */
	void sendUserEnabledEmail(User user);

	/**
	 * 
	 * @param user
	 */
	void sendUserDisabledEmail(User user);

	/**
	 * 
	 * @param username
	 * @return
	 */
	User findByUserName(String username);

	/**
	 * notify user with user register successful message
	 * 
	 * @param user
	 */
	void sendUserRegisterEmail(User user);

	/**
	 * to get all the user of given institution
	 * 
	 * @param fields
	 * @param queries
	 * @param jtableRequestData
	 * @param institution
	 * @param role
	 * @return
	 */
	List<User> findAllUserByInstitution(String[] fields, String[] queries, JtableRequestData jtableRequestData,
			Institution institution, List<Role> role);

	/**
	 * 
	 * @param fields
	 * @param queries
	 * @param institution
	 * @return
	 */
	long countAllUserByInstitution(String[] fields, String[] queries, Institution institution, List<Role> role);

	/**
	 * method to find all user created by given institution
	 * 
	 * @param institution
	 * @return @List of User
	 */
	List<User> findByInstitutionAndRoles(Institution institution, String normalUser);

}
