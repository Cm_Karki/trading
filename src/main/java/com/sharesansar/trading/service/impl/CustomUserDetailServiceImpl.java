package com.sharesansar.trading.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.entity.AuditUser;
import com.sharesansar.trading.entity.auth.Role;
import com.sharesansar.trading.entity.auth.User;
import com.sharesansar.trading.service.UserService;

@Service
public class CustomUserDetailServiceImpl implements UserDetailsService {

	private static final Logger LOG = LogManager.getLogger(CustomUserDetailServiceImpl.class);

	@Autowired
	UserService userServiceImpl;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		LOG.info("Inside CustomUserDetailServiceImpl#loadUserByUsername username {}", username);
		User user = userServiceImpl.findByUserName(username);
		if (user == null)
			throw new UsernameNotFoundException("Username not found exception");
		List<GrantedAuthority> authorities = getGrantedAuthorities(user);
		return new AuditUser(username, user.getPassword(), true, true, true, true, authorities, user.getId(), user,
				null);
	}

	private List<GrantedAuthority> getGrantedAuthorities(User user) {
		LOG.info("Inside CustomUserDetailServiceImpl#getGrantedAuthorities user {}", user);
		List<String> roleList = new ArrayList<String>();
		for (Role role : user.getRoles()) {
			if (role.getName().equals(RoleConstant.INSTITUTION_ADMIN))
				roleList.add(RoleConstant.INSTITUTION_ADMIN);
			if (role.getName().equals(RoleConstant.NORMAL_USER))
				roleList.add(RoleConstant.NORMAL_USER);
		}
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for (String role : roleList) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;

	}

}
