package com.sharesansar.trading.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sharesansar.trading.entity.Institution;
import com.sharesansar.trading.entity.InstitutionEvent;
import com.sharesansar.trading.entity.auth.User;
import com.sharesansar.trading.repository.EventRepository;
import com.sharesansar.trading.service.EventManagementService;
import com.sharesansar.trading.util.JtableRequestData;

@Service
@Transactional(readOnly = true)
@SuppressWarnings({ "rawtypes", "unchecked" })
public class EventManagementServiceImpl implements EventManagementService {

	private static final Logger LOG = LogManager.getLogger(EventManagementServiceImpl.class);

	@Autowired
	private EntityManager entityManager;

	@Autowired
	private EventRepository eventRepository;

	public List<InstitutionEvent> findAllUserByInstitution(String[] fields, String[] queries,
			JtableRequestData jtableRequestData, Institution institution) {
		LOG.info(
				"Inside EventManagementServiceImpl#findAllUserByInstitution method fields {}, queries {}, jtableRequestData {}, institution {}",
				fields, queries, jtableRequestData, institution);
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery(InstitutionEvent.class);
		Root root = cq.from(InstitutionEvent.class);
		cq.select(root);
		List<Predicate> predicates = new ArrayList<Predicate>();
		predicates.add(cb.equal(root.get("institution"), institution));
		if (fields != null && fields.length > 0 && queries != null && queries.length > 0) {
			int i = 0;
			for (String field : fields) {
				predicates.add(cb.like(root.get(field), "%" + queries[i] + "%"));
				i++;
			}
		}
		cq.where(cb.and((Predicate[]) predicates.toArray(new Predicate[0])));
		if (jtableRequestData != null && jtableRequestData.getJtSorting() != null
				&& jtableRequestData.getJtSorting().length() > 0) {
			String sortColumn = jtableRequestData.getJtSorting().split(" ")[0];
			if (jtableRequestData.getJtSorting().equals("ASC"))
				cq.orderBy(cb.asc(root.get(sortColumn)));
			else
				cq.orderBy(cb.desc(root.get(sortColumn)));
		}
		Query query = entityManager.createQuery(cq);
		if (jtableRequestData != null)
			query.setFirstResult(jtableRequestData.getJtStartIndex()).setMaxResults(jtableRequestData.getJtPageSize());
		return query.getResultList();
	}

	@Override
	public long countAllEventByInstitution(String[] fields, String[] queries, Institution institution) {
		LOG.info("Inside EventManagementServiceImpl#countAllEventByInstitution method. Fields {}, Queries {}", fields,
				queries);
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(User.class);
		Root root = criteriaQuery.from(User.class);
		criteriaQuery.select(criteriaBuilder.count(root));
		List<Predicate> predicates = new ArrayList<Predicate>();
		predicates.add(criteriaBuilder.equal(root.get("institution"), institution));
		if (fields != null && fields.length > 0 && queries != null && queries.length > 0) {
			int i = 0;
			for (String field : fields) {
				predicates.add(criteriaBuilder.like(root.get(field), "%" + queries[i] + "%"));
				i++;
			}
		}
		criteriaQuery.where(criteriaBuilder.and((Predicate[]) predicates.toArray(new Predicate[0])));
		Query query = entityManager.createQuery(criteriaQuery);
		return (Long) query.getSingleResult();
	}

	@Transactional
	public InstitutionEvent save(InstitutionEvent event) {
		LOG.info("Inside EventManagementServiceImpl#save method event {}", event);
		return eventRepository.save(event);

	}

	@Override
	public InstitutionEvent findById(Long id) {
		LOG.info("Inside EventManagementServiceImpl#findById method id {}", id);
		return eventRepository.findById(id);
	}

	@Transactional
	public void update(InstitutionEvent event) {
		LOG.info("Inside EventManagementServiceImpl#update method event {}", event);
		eventRepository.save(event);

	}

	@Override
	public List<InstitutionEvent> findByIstitution(Institution institution) {
		LOG.info("Inside EventManagementServiceImpl#findByIstitution method institution {}", institution);
		return eventRepository.findByInstitution(institution);

	}

	@Override
	public InstitutionEvent findByIdAndInstitution(long identity, Institution institution) {
		LOG.info("Inside EventManagementServiceImpl#findByIdAndInstitution method institution {}, id {}", institution,
				identity);
		return eventRepository.findByIdAndInstitution(identity, institution);
	}

}
