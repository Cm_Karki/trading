package com.sharesansar.trading.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sharesansar.trading.entity.Institution;
import com.sharesansar.trading.entity.MoneyAllocation;
import com.sharesansar.trading.repository.MoneyAllocationRepository;
import com.sharesansar.trading.service.MoneyAllocationService;

@Service
@Transactional(readOnly = true)
public class MoneyAllocationServiceImpl implements MoneyAllocationService {

	private static final Logger LOG = LogManager.getLogger(MoneyAllocationServiceImpl.class);

	@Autowired
	private MoneyAllocationRepository repo;

	@Override
	public MoneyAllocation findById(long id) {
		LOG.info("Inside MoneyAllocationServiceImpl#findById method id {}", id);

		return repo.findById(id);
	}

	@Transactional
	public MoneyAllocation save(MoneyAllocation money) {
		LOG.info("Inside MoneyAllocationServiceImpl#save method money {}", money);
		return repo.save(money);
	}

	@Override
	public List<MoneyAllocation> findAllByInstitution(Institution institution) {
		LOG.info("Inside MoneyAllocationServiceImpl#findAll method");
		return (List<MoneyAllocation>) repo.findAllByInstitution(institution);
	}

}
