package com.sharesansar.trading.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sharesansar.trading.constant.OrderStatus;
import com.sharesansar.trading.constant.OrderType;
import com.sharesansar.trading.entity.Order;
import com.sharesansar.trading.entity.auth.User;
import com.sharesansar.trading.repository.OrderRepository;
import com.sharesansar.trading.service.OrderService;

@Service
@Transactional(readOnly = true)
public class OrderServiceImpl implements OrderService {

	private static final Logger LOG = LogManager.getLogger(OrderServiceImpl.class);

	@Autowired
	private OrderRepository orderRepository;

	@Transactional
	public Order save(Order order) {
		LOG.info("Inside OrderServiceImpl#save method order {}", order);
		return orderRepository.save(order);
	}

	@Override
	public List<Order> findByUserAndStatusAndOrderType(User user, OrderStatus status, OrderType type) {
		LOG.info("Inside OrderServiceImpl#findByUserAndStatusAndOrderType method user {}, status {}, orderType {}",
				user, status, type);
		return orderRepository.findAllByUserAndStatusAndOrderType(user, status, type);
	}
}
