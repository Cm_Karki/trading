package com.sharesansar.trading.service.impl;

import java.util.Collection;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.entity.auth.Role;
import com.sharesansar.trading.repository.RoleRepository;
import com.sharesansar.trading.service.RoleService;

@Service
@Transactional(readOnly = true)
public class RoleServiceImpl implements RoleService {

	private static final Logger LOG = LogManager.getLogger(RoleServiceImpl.class);

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public List<Role> findAll() {
		LOG.info("Inside RoleServiceImpl#findAll method");
		return (List<Role>) roleRepository.findAll();

	}

	@Override
	public boolean isValidAuthorities(Collection<? extends GrantedAuthority> authorities) {
		LOG.info("Inside RoleServiceImpl#isValidAuthorities method authorities {}", authorities);
		boolean isValidAuthorities = false;
		for (GrantedAuthority authority : authorities) {
			if (authority.getAuthority().equals(RoleConstant.INSTITUTION_ADMIN))
				isValidAuthorities = true;
			if (authority.getAuthority().equals(RoleConstant.NORMAL_USER))
				isValidAuthorities = true;
		}
		return isValidAuthorities;
	}

	@Override
	public boolean isInstitutionalUser(Collection<? extends GrantedAuthority> authorities) {
		LOG.info("Inside RoleServiceImpl#isInstitutionalUser method authorities {}", authorities);
		boolean isValidAuthorities = false;
		for (GrantedAuthority authority : authorities) {
			if (authority.getAuthority().equals(RoleConstant.INSTITUTION_ADMIN))
				isValidAuthorities = true;
		}
		return isValidAuthorities;
	}

	@Override
	public boolean isNormalUser(Collection<? extends GrantedAuthority> authorities) {
		LOG.info("Inside RoleServiceImpl#isNormalUser method authorities {}", authorities);
		boolean isValidAuthorities = false;
		for (GrantedAuthority authority : authorities) {
			if (authority.getAuthority().equals(RoleConstant.NORMAL_USER))
				isValidAuthorities = true;
		}
		return isValidAuthorities;
	}

	@Override
	public List<Role> findAllByName(String role) {
		LOG.info("Inside RoleServiceImpl#findAllByName method role {}", role);
		return roleRepository.findAllByName(role);
	}

	@Override
	public Role findByName(String role) {
		LOG.info("Inside RoleServiceImpl#findByName method role {}", role);
		return roleRepository.findByName(role);

	}
}
