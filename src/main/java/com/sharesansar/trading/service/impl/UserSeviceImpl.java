package com.sharesansar.trading.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;

import com.sharesansar.trading.constant.PageConstant;
import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.entity.Institution;
import com.sharesansar.trading.entity.auth.Role;
import com.sharesansar.trading.entity.auth.User;
import com.sharesansar.trading.expection.NameAlreadyExistException;
import com.sharesansar.trading.repository.UserRepository;
import com.sharesansar.trading.service.UserService;
import com.sharesansar.trading.util.JtableRequestData;

@Service
@Transactional(readOnly = true)
@SuppressWarnings({ "unchecked", "rawtypes" })
public class UserSeviceImpl implements UserService {

	private static final Logger LOG = LogManager.getLogger(UserService.class);

	@Autowired
	private MessageSource messageSource;

	@Autowired
	private EntityManager entityManager;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private EmailServiceImpl emailServiceImpl;

	@Override
	public List<User> getAllUser(String[] fields, String[] queries, JtableRequestData jtableRequestData) {
		LOG.info("Inside UserServiceImpl#getAllInstitution method fields {}, queries {}, jtableRequestData {}", fields,
				queries, jtableRequestData);

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(User.class);
		Root root = criteriaQuery.from(User.class);
		criteriaQuery.select(root);
		List<Predicate> predicates = new ArrayList<Predicate>();
		if (fields != null && fields.length > 0 && queries != null && queries.length > 0) {
			int i = 0;
			for (String field : fields) {
				predicates.add(criteriaBuilder.like(root.get(field), "%" + queries[i] + "%"));
				i++;
			}
		}
		criteriaQuery.where(criteriaBuilder.and((Predicate[]) predicates.toArray(new Predicate[0])));
		if (jtableRequestData != null && jtableRequestData.getJtSorting() != null
				&& jtableRequestData.getJtSorting().length() > 0) {
			String sortColumn = jtableRequestData.getJtSorting().split(" ")[0];
			if (jtableRequestData.getJtSorting().equals("ASC"))
				criteriaQuery.orderBy(criteriaBuilder.asc(root.get(sortColumn)));
			else
				criteriaQuery.orderBy(criteriaBuilder.desc(root.get(sortColumn)));
		}
		Query query = entityManager.createQuery(criteriaQuery);
		if (jtableRequestData != null)
			query.setFirstResult(jtableRequestData.getJtStartIndex()).setMaxResults(jtableRequestData.getJtPageSize());
		return query.getResultList();
	}

	@Override
	public long countAllUser(String[] fields, String[] queries) {
		LOG.info("Inside UserServiceImpl#countAllUser method. Fields {}, Queries {}", fields, queries);
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(User.class);
		Root root = criteriaQuery.from(User.class);
		criteriaQuery.select(criteriaBuilder.count(root));
		List<Predicate> predicates = new ArrayList<Predicate>();
		if (fields != null && fields.length > 0 && queries != null && queries.length > 0) {
			int i = 0;
			for (String field : fields) {
				predicates.add(criteriaBuilder.like(root.get(field), "%" + queries[i] + "%"));
				i++;
			}
		}
		criteriaQuery.where(criteriaBuilder.and((Predicate[]) predicates.toArray(new Predicate[0])));
		Query query = entityManager.createQuery(criteriaQuery);
		return (Long) query.getSingleResult();
	}

	@Override
	public void isUnique(String userName, long id) throws NameAlreadyExistException {
		LOG.info("Inside UserServiceImpl#isUnique method. userName {}, id {}", userName, id);
		List<User> users = userRepository.findByUserNameAndIdNot(userName, id);
		if (users.size() > 0)
			throw new NameAlreadyExistException("User Name Already Taken");

	}

	@Transactional
	public User save(User user) {
		LOG.info("Inside UserServiceImpl#save method. user {}", user);
		return userRepository.save(user);
	}

	@Override
	public void sendUserCreateEmail(User user, String passwordResetUrl) {
		LOG.info("Inside UserServiceImpl#sendUserCreateEmail. User {}", user);
		String subject = messageSource.getMessage("subject.user.create", null, Locale.getDefault());
		ArrayList<String> emails = new ArrayList<String>();
		emails.add(user.getEmail());
		String[] recipients = emails.toArray(new String[emails.size()]);
		passwordResetUrl += "?id=" + user.getId() + "&&token=" + user.getPasswordResetCode();
		Context context = new Context();
		context.setVariable("userName", user.getUserName());
		context.setVariable("passwordResetUrl", passwordResetUrl);
		context.setVariable("roleName", user.getRoles().get(0).getName());
		context.setVariable("email", user.getEmail());
		String path = PageConstant.PAGE_EMAIL_USER_CREATE;
		emailServiceImpl.sendEmail(recipients, subject, context, path);

	}

	@Override
	public User findById(Long id) {
		LOG.info("Inside UserServiceImpl#findById method. id {}", id);
		return userRepository.findById(id);
	}

	@Override
	public boolean verifyPasswordResetToken(User user, String token) {
		LOG.info("Inside UserServiceImpl#verifyPasswordResetToken.");
		boolean isValid = Boolean.FALSE;
		if (user != null && user.getPasswordResetCode().equals(token))
			isValid = Boolean.TRUE;
		return isValid;
	}

	@Transactional
	public void update(User user1) {
		LOG.info("Inside UserServiceImpl#update method.user {}", user1);
		userRepository.save(user1);
	}

	@Override
	public void sendUserRoleChangeEmail(User user, String prevRole) {
		LOG.info("Inside UserServiceImpl#sendUserRoleChangeEmail. User {}, previous role {}", user, prevRole);
		String subject = messageSource.getMessage("subject.role.change", null, Locale.getDefault());
		ArrayList<String> emails = new ArrayList<String>();
		emails.add(user.getEmail());
		String[] recipients = emails.toArray(new String[emails.size()]);
		Context context = new Context();
		context.setVariable("userName", user.getUserName());
		context.setVariable("roleName", user.getRoles().get(0).getName());
		context.setVariable("prevRole", prevRole);
		context.setVariable("email", user.getEmail());
		String path = PageConstant.PAGE_EMAIL_USER_ROLE_CHANGED;
		emailServiceImpl.sendEmail(recipients, subject, context, path);

	}

	@Override
	public void sendUserEnabledEmail(User user) {
		LOG.info("Inside UserServiceImpl#sendUserEnabledEmail. User {}", user);
		String subject = messageSource.getMessage("subject.role.change", null, Locale.getDefault());
		ArrayList<String> emails = new ArrayList<String>();
		emails.add(user.getEmail());
		String[] recipients = emails.toArray(new String[emails.size()]);
		Context context = new Context();
		context.setVariable("userName", user.getUserName());
		context.setVariable("roleName", user.getRoles().get(0).getName());
		context.setVariable("email", user.getEmail());
		String path = PageConstant.PAGE_EMAIL_USER_ACCOUNT_ENABLED;
		emailServiceImpl.sendEmail(recipients, subject, context, path);

	}

	@Override
	public void sendUserDisabledEmail(User user) {
		LOG.info("Inside UserServiceImpl#sendUserDisabledEmail. User {}", user);
		String subject = messageSource.getMessage("subject.role.change", null, Locale.getDefault());
		ArrayList<String> emails = new ArrayList<String>();
		emails.add(user.getEmail());
		String[] recipients = emails.toArray(new String[emails.size()]);
		Context context = new Context();
		context.setVariable("userName", user.getUserName());
		context.setVariable("roleName", user.getRoles().get(0).getName());
		context.setVariable("email", user.getEmail());
		String path = PageConstant.PAGE_EMAIL_USER_ACCOUNT_DISABLE;
		emailServiceImpl.sendEmail(recipients, subject, context, path);

	}

	@Override
	public User findByUserName(String username) {
		LOG.info("Inside UserServiceImpl#findByUserName username {}", username);
		return userRepository.findByUserName(username);
	}

	@Override
	public void sendUserRegisterEmail(User user) {
		LOG.info("Inside UserServiceImpl#sendUserRegisterEmail. User {}", user);
		String subject = messageSource.getMessage("subject.user.create", null, Locale.getDefault());
		ArrayList<String> emails = new ArrayList<String>();
		emails.add(user.getEmail());
		String[] recipients = emails.toArray(new String[emails.size()]);
		Context context = new Context();
		context.setVariable("userName", user.getUserName());
		context.setVariable("email", user.getEmail());
		String path = PageConstant.PAGE_EMAIL_USER_ACCOUNT_REGISTERD;
		emailServiceImpl.sendEmail(recipients, subject, context, path);

	}

	@Override
	public List<User> findAllUserByInstitution(String[] fields, String[] queries, JtableRequestData jtableRequestData,
			Institution institution, List<Role> role) {
		LOG.info(
				"Inside UserServiceImpl#findAllUserByInstitution method institution name {}, fields {}, queries {}, jtableRequestData {}",
				fields, queries, jtableRequestData, institution.getName());

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(User.class);
		Root root = criteriaQuery.from(User.class);
		Join<User, Role> role1 = root.join("roles");
		criteriaQuery.select(root);
		List<Predicate> predicates = new ArrayList<Predicate>();
		predicates.add(criteriaBuilder.equal(root.get("institution"), institution));
		predicates.add(criteriaBuilder.equal(role1.get("name"), RoleConstant.NORMAL_USER));
		if (fields != null && fields.length > 0 && queries != null && queries.length > 0) {
			int i = 0;
			for (String field : fields) {
				predicates.add(criteriaBuilder.like(root.get(field), "%" + queries[i] + "%"));
				i++;
			}
		}
		criteriaQuery.where(criteriaBuilder.and((Predicate[]) predicates.toArray(new Predicate[0])));
		if (jtableRequestData != null && jtableRequestData.getJtSorting() != null
				&& jtableRequestData.getJtSorting().length() > 0) {
			String sortColumn = jtableRequestData.getJtSorting().split(" ")[0];
			if (jtableRequestData.getJtSorting().equals("ASC"))
				criteriaQuery.orderBy(criteriaBuilder.asc(root.get(sortColumn)));
			else
				criteriaQuery.orderBy(criteriaBuilder.desc(root.get(sortColumn)));
		}
		Query query = entityManager.createQuery(criteriaQuery);
		if (jtableRequestData != null)
			query.setFirstResult(jtableRequestData.getJtStartIndex()).setMaxResults(jtableRequestData.getJtPageSize());
		return query.getResultList();
	}

	@Override
	public long countAllUserByInstitution(String[] fields, String[] queries, Institution institution, List<Role> role) {
		LOG.info("Inside UserServiceImpl#countAllUserByInstitution method. institution {}, Fields {}, Queries {}",
				fields, queries, institution.getName());
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(User.class);
		Root root = criteriaQuery.from(User.class);
		Join<User, Role> role1 = root.join("roles");
		criteriaQuery.select(criteriaBuilder.count(root));
		List<Predicate> predicates = new ArrayList<Predicate>();
		predicates.add(criteriaBuilder.equal(root.get("institution"), institution));
		predicates.add(criteriaBuilder.equal(role1.get("name"), RoleConstant.NORMAL_USER));
		if (fields != null && fields.length > 0 && queries != null && queries.length > 0) {
			int i = 0;
			for (String field : fields) {
				predicates.add(criteriaBuilder.like(root.get(field), "%" + queries[i] + "%"));
				i++;
			}
		}
		criteriaQuery.where(criteriaBuilder.and((Predicate[]) predicates.toArray(new Predicate[0])));
		Query query = entityManager.createQuery(criteriaQuery);
		return (Long) query.getSingleResult();
	}

	@Override
	public List<User> findByInstitutionAndRoles(Institution institution, String role) {
		LOG.info("Inside UserServiceImpl#findByInstitution method. institution {}, role {}", institution, role);

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery cq = cb.createQuery(User.class);
		Root root = cq.from(User.class);
		Join<User, Role> role1 = root.join("roles");
		List<Predicate> predicates = new ArrayList<Predicate>();
		predicates.add(cb.equal(root.get("institution"), institution));
		predicates.add(cb.equal(role1.get("name"), role));
		cq.where(cb.and((Predicate[]) predicates.toArray(new Predicate[0])));
		Query q = entityManager.createQuery(cq);
		return q.getResultList();
	}
}
