package com.sharesansar.trading.service.impl;

import java.util.Collection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.service.AdminRoleService;

@Service
public class AdminRoleServiceImpl implements AdminRoleService {
	private static Logger LOG = LogManager.getLogger(AdminRoleServiceImpl.class);

	public boolean isValidAuthorities(Collection<? extends GrantedAuthority> authorities) {
		LOG.info("Inside AdminRoleServiceImpl#isValidAuthorities method authorities {}", authorities);
		boolean isValidAuthorities = false;
		for (GrantedAuthority authority : authorities) {
			if (authority.getAuthority().equals(RoleConstant.APP_ADMIN))
				isValidAuthorities = true;
		}
		return isValidAuthorities;
	}

}
