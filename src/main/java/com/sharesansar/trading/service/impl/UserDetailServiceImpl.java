package com.sharesansar.trading.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sharesansar.trading.entity.auth.UserDetail;
import com.sharesansar.trading.repository.UserDetailRepository;
import com.sharesansar.trading.service.UserDetailService;


@Service
@Transactional(readOnly = true)

public class UserDetailServiceImpl  implements UserDetailService{

	private static final Logger LOG =  LogManager.getLogger(UserDetailServiceImpl.class);
	
	@Autowired
	private UserDetailRepository userDetailRepository;

	@Override
	public UserDetail findByName(String name) {
		LOG.info("Inside UserDetailServiceImpl#findByName method. name {}", name);
		return userDetailRepository.findByName(name);
	}

	@Transactional
	public void update(UserDetail userDetail1) {
		LOG.info("Inside UserDetailServiceImpl#update method.user {}", userDetail1);
		userDetailRepository.save(userDetail1);
		}
	}




