package com.sharesansar.trading.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sharesansar.trading.entity.Institution;
import com.sharesansar.trading.entity.InterestRate;
import com.sharesansar.trading.repository.InterestRateRepository;
import com.sharesansar.trading.service.InterestRateService;

@Service
@Transactional(readOnly = true)
public class InterestRateServiceImpl implements InterestRateService{
	
	private static final Logger LOG = LogManager.getLogger(InterestRateServiceImpl.class);
	@Autowired
	private InterestRateRepository interestRateRepository;


	@Override
	public List<InterestRate> findAllByInstitution(Institution institution) {
		LOG.info("Inside InterestRateServiceImpl#findAll method");
		return (List<InterestRate>) interestRateRepository.findAllByInstitution(institution);
	}


	@Override
	public InterestRate findById(long id) {
		LOG.info("Inside InterestRateServiceImpl#findById method id {}", id);

		return interestRateRepository.findById(id);
	}


	@Transactional
	public InterestRate save(InterestRate interestRate) {
			LOG.info("Inside InterestRateServiceImpl#save method money {}", interestRate);
			return interestRateRepository.save(interestRate);
		}

		
	}


