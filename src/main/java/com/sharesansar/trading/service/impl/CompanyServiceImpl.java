package com.sharesansar.trading.service.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sharesansar.trading.entity.Company;
import com.sharesansar.trading.repository.CompanyRepository;
import com.sharesansar.trading.service.CompanyService;

@Service
@Transactional(readOnly = true)
public class CompanyServiceImpl implements CompanyService {

	private static final Logger LOG = LogManager.getLogger(CompanyServiceImpl.class);
	@Autowired
	private CompanyRepository companyRepository;

	@Override
	public Company findById(long id) {
		LOG.info("Inside CompanyServiceImpl#findById method Id {}", id);
		return companyRepository.findById(id);
	}

	@Transactional
	public void save(List<Company> companyList) {
		LOG.info("Inside CompanyServiceImpl#saveList method size of companyLsit {}", companyList.size());
		companyRepository.save(companyList);

	}

	@Override
	public List<Company> findAll() {
		LOG.info("Inside CompanyServiceImpl#findAll method");
		return (List<Company>) companyRepository.findAll();
	}

}
