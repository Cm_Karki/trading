package com.sharesansar.trading.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sharesansar.trading.entity.FixedDeposit;
import com.sharesansar.trading.repository.FixedDepositRepository;
import com.sharesansar.trading.service.FixedDepositService;
import com.sharesansar.trading.util.JtableRequestData;

/**
 * Class that implement {@link FixedDepositService}
 * 
 * @author aleeza
 *
 */
@Service
@Transactional(readOnly = true)
@SuppressWarnings({ "unchecked", "rawtypes" })
public class FixedDepositServiceImpl implements FixedDepositService {

	private static final Logger LOG = LogManager.getLogger(FixedDepositServiceImpl.class);
	
	@Autowired
	private FixedDepositRepository fixedDepositRepository;
	
	@Autowired
	private EntityManager entityManager;


	@Transactional
	public FixedDeposit save(FixedDeposit fixedDeposit) {
		LOG.info("Inside UserServiceImpl#save method. user {}", fixedDeposit);
		return fixedDepositRepository.save(fixedDeposit);
	}


	@Override
	public List<FixedDeposit> getAllFixedDeposit(String[] fields, String[] queries,JtableRequestData jtableRequestData) {
		LOG.info("Inside FixedDepositServiceImpl#getAllFixedDeposit method fields {}, queries {}, jtableRequestData {}", fields,
				queries, jtableRequestData);

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(FixedDeposit.class);
		Root root = criteriaQuery.from(FixedDeposit.class);
		criteriaQuery.select(root);
		List<Predicate> predicates = new ArrayList<Predicate>();
		if (fields != null && fields.length > 0 && queries != null && queries.length > 0) {
			int i = 0;
			for (String field : fields) {
				predicates.add(criteriaBuilder.like(root.get(field), "%" + queries[i] + "%"));
				i++;
			}
		}
		criteriaQuery.where(criteriaBuilder.and((Predicate[]) predicates.toArray(new Predicate[0])));
		if (jtableRequestData != null && jtableRequestData.getJtSorting() != null
				&& jtableRequestData.getJtSorting().length() > 0) {
			String sortColumn = jtableRequestData.getJtSorting().split(" ")[0];
			if (jtableRequestData.getJtSorting().equals("ASC"))
				criteriaQuery.orderBy(criteriaBuilder.asc(root.get(sortColumn)));
			else
				criteriaQuery.orderBy(criteriaBuilder.desc(root.get(sortColumn)));
		}
		Query query = entityManager.createQuery(criteriaQuery);
		if (jtableRequestData != null)
			query.setFirstResult(jtableRequestData.getJtStartIndex()).setMaxResults(jtableRequestData.getJtPageSize());
		return query.getResultList();
	}


	@Override
	public long countAllUser(String[] fields, String[] queries) {
		LOG.info("Inside FixedDepositServiceImpl#countAllUser method. Fields {}, Queries {}", fields, queries);
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(FixedDeposit.class);
		Root root = criteriaQuery.from(FixedDeposit.class);
		criteriaQuery.select(criteriaBuilder.count(root));
		List<Predicate> predicates = new ArrayList<Predicate>();
		if (fields != null && fields.length > 0 && queries != null && queries.length > 0) {
			int i = 0;
			for (String field : fields) {
				predicates.add(criteriaBuilder.like(root.get(field), "%" + queries[i] + "%"));
				i++;
			}
		}
		criteriaQuery.where(criteriaBuilder.and((Predicate[]) predicates.toArray(new Predicate[0])));
		Query query = entityManager.createQuery(criteriaQuery);
		return (Long) query.getSingleResult();
	}


	@Override
	public FixedDeposit findById(Long id) {
		LOG.info("Inside FixedDepositServiceImpl#findById method. id {}", id);
		return fixedDepositRepository.findById(id);
	}

	@Override
	public List<FixedDeposit> findAll() {
		LOG.info("Inside FixedDepositServiceImpl#findAll method.");
		return (List<FixedDeposit>) fixedDepositRepository.findAll();
	}


	@Transactional
	public void update(FixedDeposit fixedDeposit1) {
		LOG.info("Inside FixedDepositServiceImpl#update method.user {}", fixedDeposit1);
		fixedDepositRepository.save(fixedDeposit1);
	}


	@Transactional
	public void delete(FixedDeposit fixedDeposit) {
		LOG.info("Inside FixedDepositServiceImpl#delete method institution {}", fixedDeposit);
			fixedDepositRepository.delete(fixedDeposit);

		}
		
	}

