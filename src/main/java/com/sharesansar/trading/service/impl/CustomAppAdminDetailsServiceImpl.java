package com.sharesansar.trading.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.entity.AuditUser;
import com.sharesansar.trading.entity.auth.AdminRole;
import com.sharesansar.trading.entity.auth.AppAdmin;
import com.sharesansar.trading.service.AppAdminService;

@Service
public class CustomAppAdminDetailsServiceImpl implements UserDetailsService {

	private final static Logger LOG = LogManager.getLogger(CustomAppAdminDetailsServiceImpl.class);

	@Autowired
	AppAdminService appAdminServiceImpl;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		LOG.info("Inside CustomAppAdminDetailsServiceImpl#loadUserByUsername method username {}", username);
		AppAdmin admin = appAdminServiceImpl.findByUserName(username);
		if (admin == null)
			throw new UsernameNotFoundException("Username not found exception");
		List<GrantedAuthority> authorityList = getGrantedAuthorities(admin);
		return new AuditUser(username, admin.getPassword(), true, true, true, true, authorityList, admin.getId(), null,
				admin);
	}

	private List<GrantedAuthority> getGrantedAuthorities(AppAdmin admin) {
		LOG.info("Inside CustomAppAdminDetailsServiceImpl#getGrantedAuthorities method username {}", admin);
		List<String> roleList = new ArrayList<String>();
		for (AdminRole role : admin.getAdminRoles()) {
			if (role.getName().equals(RoleConstant.APP_ADMIN)) {
				roleList.add(RoleConstant.APP_ADMIN);

			}
		}
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		for (String role : roleList) {
			authorities.add(new SimpleGrantedAuthority(role));
		}
		return authorities;
	}

}
