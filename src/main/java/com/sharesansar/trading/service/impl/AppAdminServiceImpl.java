package com.sharesansar.trading.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sharesansar.trading.entity.auth.AppAdmin;
import com.sharesansar.trading.repository.AppAdminRepository;
import com.sharesansar.trading.service.AppAdminService;

@Service
@Transactional(readOnly = true)
public class AppAdminServiceImpl implements AppAdminService {

	private static final Logger LOG = LogManager.getLogger(AppAdminServiceImpl.class);

	@Autowired
	AppAdminRepository appAdminRepository;

	public AppAdmin findByUserName(String username) {
		LOG.info("Inside AppAdminServiceImpl#findByUserName method username {}", username);
		return appAdminRepository.findByUserName(username);
	}

}
