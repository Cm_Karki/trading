package com.sharesansar.trading.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sharesansar.trading.entity.Institution;
import com.sharesansar.trading.expection.NameAlreadyExistException;
import com.sharesansar.trading.repository.InstitutionRepository;
import com.sharesansar.trading.service.InstitutionService;
import com.sharesansar.trading.util.JtableRequestData;

@SuppressWarnings({ "rawtypes", "unchecked" })
@Service
@Transactional(readOnly = true)
public class InstitutionServiceImpl implements InstitutionService {

	private static final Logger LOG = LogManager.getLogger(Institution.class);

	@Autowired
	private InstitutionRepository institutionRepository;

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Institution> getAllInstitution(String[] fields, String[] queries, JtableRequestData jtableRequestData) {
		LOG.info("Inside InstitutionServiceImpl#getAllInstitution method fields {}, queries {}, jtableRequestData {}",
				fields, queries, jtableRequestData);

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(Institution.class);
		Root root = criteriaQuery.from(Institution.class);
		criteriaQuery.select(root);
		List<Predicate> predicates = new ArrayList<Predicate>();
		if (fields != null && fields.length > 0 && queries != null && queries.length > 0) {
			int i = 0;
			for (String field : fields) {
				predicates.add(criteriaBuilder.like(root.get(field), "%" + queries[i] + "%"));
				i++;
			}
		}
		criteriaQuery.where(criteriaBuilder.and((Predicate[]) predicates.toArray(new Predicate[0])));
		if (jtableRequestData != null && jtableRequestData.getJtSorting() != null
				&& jtableRequestData.getJtSorting().length() > 0) {
			String sortColumn = jtableRequestData.getJtSorting().split(" ")[0];
			if (jtableRequestData.getJtSorting().equals("ASC"))
				criteriaQuery.orderBy(criteriaBuilder.asc(root.get(sortColumn)));
			else
				criteriaQuery.orderBy(criteriaBuilder.desc(root.get(sortColumn)));
		}
		Query query = entityManager.createQuery(criteriaQuery);
		if (jtableRequestData != null)
			query.setFirstResult(jtableRequestData.getJtStartIndex()).setMaxResults(jtableRequestData.getJtPageSize());
		return query.getResultList();
	}

	@Override
	public long countAllInstitution(String[] fields, String[] queries) {
		LOG.info("Inside InstitutionServiceImpl#countAllBranch method. Fields {}, Queries {}", fields, queries);
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(Institution.class);
		Root root = criteriaQuery.from(Institution.class);
		criteriaQuery.select(criteriaBuilder.count(root));
		List<Predicate> predicates = new ArrayList<Predicate>();
		if (fields != null && fields.length > 0 && queries != null && queries.length > 0) {
			int i = 0;
			for (String field : fields) {
				predicates.add(criteriaBuilder.like(root.get(field), "%" + queries[i] + "%"));
				i++;
			}
		}
		criteriaQuery.where(criteriaBuilder.and((Predicate[]) predicates.toArray(new Predicate[0])));
		Query query = entityManager.createQuery(criteriaQuery);
		return (Long) query.getSingleResult();
	}

	@Override
	public void isUnique(String name, long id) throws NameAlreadyExistException {
		LOG.info("Inside InstitutionServiceImpl#isUnique method name {}, id {}", name, id);
		if (institutionRepository.countByNameAndIdNot(name, id) > 0)
			throw new NameAlreadyExistException("Institution Name Already Exist");

	}

	@Transactional
	public Institution save(Institution institution) {
		LOG.info("Inside InstitutionServiceImpl#save method institution {}", institution);
		return institutionRepository.save(institution);

	}

	@Override
	public Institution findById(Long id) {
		LOG.info("Inside InstitutionServiceImpl#findById method id {}", id);
		return institutionRepository.findById(id);
	}

	@Transactional
	public void update(Institution institution1) {
		LOG.info("Inside InstitutionServiceImpl#findById method institution {}", institution1);
		institutionRepository.save(institution1);

	}

	@Transactional
	public void delete(Institution institution) {
		LOG.info("Inside InstitutionServiceImpl#delete method institution {}", institution);
		institutionRepository.delete(institution);

	}

	@Override
	public List<Institution> findAll() {
		LOG.info("Inside InstitutionServiceImpl#findAll method.");
		return (List<Institution>) institutionRepository.findAll();
	}

	@Override
	public Institution findByIsMain(Boolean true1) {
		LOG.info("Inside InstitutionServiceImpl#findByName method.");
		return institutionRepository.findByIsMain(true1);
	}

}
