package com.sharesansar.trading.service.impl;

import java.io.File;
import java.nio.charset.StandardCharsets;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

/**
 * This is a service class for sending emails. This class contains method for
 * sending Email. The Email can be a text email or html email. If any method
 * pertaining to sending/receiving email is required, add it to this class.
 * 
 * @author rajendra
 *
 */
@Transactional(readOnly = true)
@Service
public class EmailServiceImpl {

	private static final Logger LOG = LogManager.getLogger(EmailServiceImpl.class);

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private SpringTemplateEngine templateEngine;

//	@Value("${spring.mail.fromEmail}")
//	private String fromEmail;

	public void sendHTMLEmail(String[] recipients, String subject, String body, File attachment)
			throws MessagingException {
		LOG.info("Inside EmailServiceImpl#sendHTMLEmail method.");
		MimeMessage message = javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
				StandardCharsets.UTF_8.name());
		helper.setTo(recipients);
		helper.setText(body, true);
		helper.setSubject(subject);
//		helper.setFrom(fromEmail);
		javaMailSender.send(message);
	}

	private String getEmailBody(final Context context, final String path) throws Exception {
		LOG.info("Inside EmailServiceImpl#getEmailBody method.");
		return templateEngine.process(path, context);
	}

	@Transactional
	public void sendEmail(String[] recipients, String subject, final Context context, final String path) {
		LOG.info("Inside EmailServiceImpl#sendEmail method.");
		try {
			sendHTMLEmail(recipients, subject, getEmailBody(context, path), null);
		} catch (Exception e) {
			LOG.error("Error while sending email - error detail {}", e);
		}
	}

}
