package com.sharesansar.trading.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.sharesansar.trading.config.auth.SpringSecurityAuditorAware;

/**
 * Configuration class for Spring Data Jpa Auditing
 * 
 * @author rajendra
 *
 */
@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
public class JpaConfig {

	@Bean
	public SpringSecurityAuditorAware auditorAware() {
		return new SpringSecurityAuditorAware();
	}

}
