package com.sharesansar.trading.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import com.sharesansar.trading.config.auth.CustomAuthenticationSuccessHandler;
import com.sharesansar.trading.config.auth.CustomLogoutHandler;
import com.sharesansar.trading.service.impl.CustomAppAdminDetailsServiceImpl;

@Configuration
@EnableWebSecurity
@Order(2)
public class AdminSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	CustomAppAdminDetailsServiceImpl customAppAdminDetailsServiceImpl;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(11);
		auth.userDetailsService(customAppAdminDetailsServiceImpl).passwordEncoder(passwordEncoder);
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/static/**");
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.antMatcher("/app-admin/**").authorizeRequests().anyRequest().authenticated();
		http.formLogin().loginPage("/app-admin/login").loginProcessingUrl("/app-admin/login/process")
				.defaultSuccessUrl("/app-admin/dashboard").failureUrl("/app-admin/login/fail")
				.usernameParameter("admin_username").passwordParameter("admin_password")
				.successHandler(customAuthenticationSuccessHandler()).permitAll();
		http.logout().deleteCookies("JSESSIONID").invalidateHttpSession(true).logoutUrl("/app-admin/logout")
				.logoutSuccessUrl("/app-admin/login").addLogoutHandler(logoutHandler()).permitAll();
		http.csrf();
	}

	@Bean
	public CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler() {
		return new CustomAuthenticationSuccessHandler();
	}

	@Bean
	public LogoutHandler logoutHandler() {
		return new CustomLogoutHandler();
	}
}
