package com.sharesansar.trading.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import com.sharesansar.trading.config.auth.CustomUserAuthenticationSuccessHandler;
import com.sharesansar.trading.config.auth.CustomUserLogoutHandler;
import com.sharesansar.trading.service.impl.CustomUserDetailServiceImpl;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class UserSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	CustomUserDetailServiceImpl customUserDetailServiceImpl;

	@Autowired
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(11);
		auth.userDetailsService(customUserDetailServiceImpl).passwordEncoder(bCryptPasswordEncoder);
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/static/**");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/static/**").permitAll();
		http.formLogin().loginPage("/user/login").loginProcessingUrl("/user/login/process")
				.defaultSuccessUrl("/user/admin/dashboard").failureUrl("/user/login/fail")
				.usernameParameter("user_username").passwordParameter("user_password")
				.successHandler(customUserAuthenticationSuccessHandler()).permitAll();
		http.logout().deleteCookies("JSESSIONID").invalidateHttpSession(true).logoutUrl("/user/logout")
				.logoutSuccessUrl("/user/login").addLogoutHandler(logoutHandler()).permitAll();
		http.csrf();
	}

	@Bean
	public CustomUserAuthenticationSuccessHandler customUserAuthenticationSuccessHandler() {
		return new CustomUserAuthenticationSuccessHandler();
	}

	@Bean
	public LogoutHandler logoutHandler() {
		return new CustomUserLogoutHandler();
	}
}
