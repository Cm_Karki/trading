package com.sharesansar.trading.config.auth;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import com.sharesansar.trading.constant.RoleConstant;
import com.sharesansar.trading.constant.UrlConstant;
import com.sharesansar.trading.service.RoleService;

public class CustomUserAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Autowired
	RoleService roleServiceImpl;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		String targetUrl = determineTargetUrl(authentication);
		redirectStrategy.sendRedirect(request, response, targetUrl);

	}

	private String determineTargetUrl(Authentication authentication) {
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		if (roleServiceImpl.isValidAuthorities(authorities)) {
			if (roleServiceImpl.isInstitutionalUser(authorities))
				return UrlConstant.URL_INSTITUTION_ADMIN_DASHBOARD + "/";
			else if (roleServiceImpl.isNormalUser(authorities))
				return UrlConstant.URL_INSTITUTION_USER_DASHBOARD;
			else
				return UrlConstant.URL_LOGIN + UrlConstant.URL_USER_LOGIN_FAIL;
		} else
			return UrlConstant.URL_LOGIN + UrlConstant.URL_USER_LOGIN_FAIL;

	}

	public RedirectStrategy getRedirectStrategy() {
		return redirectStrategy;
	}

	public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
		this.redirectStrategy = redirectStrategy;
	}
}