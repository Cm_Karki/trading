package com.sharesansar.trading.config.auth;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.sharesansar.trading.entity.AuditUser;

/**
 * This class implements AuditAware interface, which is used to implement logic
 * for @createdBy and @lastModifiedBy annotations. The class overrides the
 * method "getCurrentAuditor" which returns the value for the annotations. The
 * class has used {@link AuthUser} as type for the createdBy(@createdBy) and
 * updatedBy(@lastModifiedBy) fields.
 * 
 * @author rajendra
 *
 */
@Component
public class SpringSecurityAuditorAware implements AuditorAware<String> {
	private static final Logger LOG = LogManager.getLogger(SpringSecurityAuditorAware.class);

	@SuppressWarnings("unchecked")
	public String getCurrentAuditor() {
		LOG.info("Inside SpringSecurityAuditorAware#getCurrentAuditor class.");
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (authentication != null) {
			List<GrantedAuthority> authorityList = (List<GrantedAuthority>) authentication.getAuthorities();
			if ("anonymousUser".equals(authentication.getPrincipal())
					|| "ROLE_ANONYMOUS".equals(authorityList.get(0).getAuthority())) {
				return null;
			} else {
				return ((AuditUser) authentication.getPrincipal()).getUserName();
			}
		} else {
			return null;
		}
	}
}