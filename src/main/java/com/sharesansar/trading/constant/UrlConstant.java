package com.sharesansar.trading.constant;

public class UrlConstant {
	public static final String URL_ROOT = "/";
	public static final String URL_LOGIN = URL_ROOT + "user/login";
	public static final String URL_USER_LOGIN = URL_ROOT + "login";
	public static final String URL_LOGOUT = URL_ROOT + "logout";
	public static final String URL_USER_LOGIN_FAIL = URL_ROOT + "fail";
	public static final String URL_INSTITUTION_ADMIN_DASHBOARD = URL_ROOT + "institution/admin/dashboard";
	public static final String URL_INSTITUTION_USER_DASHBOARD = URL_ROOT + "institution/user/dashboard";

	/** for user registration **/
	public static final String URL_USER_REGISTRATION = URL_ROOT + "institution/user/registration";

	/** for password reset **/
	public static final String URL_RESET = URL_ROOT + "reset";
	public static final String URL_PASSWORD = URL_ROOT + "password";

	/** For App Admin Login **/
	public static final String URL_APP_ADMIN = URL_ROOT + "app-admin";
	public static final String URL_APP_ADMIN_DASHBOARD = URL_ROOT + "app-admin/dashboard";
	public static final String URL_APP_ADMIN_LOGOUT = URL_ROOT + "app-admin/logout";
	public static final String URL_APP_ADMIN_LOGIN_FAIL = URL_ROOT + "login/fail";
	public static final String URL_APP_ADMIN_LOGIN = URL_ROOT + "login";

	// company sync by app admin
	public static final String URL_APP_ADMIN_COMPANIES_SYNC = URL_ROOT + "app-admin/companies-sync";
	public static final String URL_APP_ADMIN_COMPANIES_SYNC_AJAX = URL_ROOT + "companies-sync.do";
	public static final String URL_APP_ADMIN_COMPANIES_SYNC_START_AJAX = URL_ROOT + "companies-sync-start.do";

	/** for institution **/
	public static final String URL_APP_ADMIN_INSTITUTION = URL_ROOT + "app-admin/institution";
	public static final String URL_LIST = URL_ROOT + "list";
	public static final String URL_LIST_AJAX = URL_ROOT + "list.do";
	public static final String URL_CREATE = URL_ROOT + "create";
	public static final String URL_VIEW_ID = URL_ROOT + "view/{id}";
	public static final String URL_VIEW = URL_ROOT + "view";
	public static final String URL_EDIT = URL_ROOT + "edit";
	public static final String URL_EDIT_ID = URL_ROOT + "edit/{id}";
	public static final String URL_UPDATE = URL_ROOT + "update";
	public static final String URL_DELETE = URL_ROOT + "delete";
	public static final String URL_STATUS = URL_ROOT + "status";

	/** for user created by app-admin **/
	public static final String URL_APP_ADMIN_USER = URL_ROOT + "app-admin/user";

	/** for user created by institution admin **/
	public static final String URL_INSTITUTION_ADMIN_USER = URL_ROOT + "institution/admin/user";

	// Fixed deposit
	public static final String URL_FIXED_DEPOSIT = URL_ROOT + "institution/user/fixed-deposit";
	public static final String URL_FIXED_DEPOSIT_LIST = URL_FIXED_DEPOSIT + "list";

	// My Details
	public static final String URL_INSTITUTION_USER_MY_DETAILS = URL_ROOT + "institution/user/my-details";
	// public static final String URL_INSTITUTION_USER_MY_DETAILS_LIST =
	// URL_INSTITUTION_USER_MY_DETAILS + "list";

	/** for Instituional Admin Settings **/
	public static final String URL_INSTITUTION_ADMIN_EVENT_MGMT = URL_ROOT + "institution/admin/settings/event-mgmt";
	public static final String URL_INSTITUTION_ADMIN_SETTING_MONEY = URL_ROOT
			+ "institution/admin/settings/money-allocation";
	public static final String URL_INSTITUTION_ADMIN_SETTING_MONEY_AJAX = URL_ROOT + "money-allocation.do";
	public static final String URL_INSTITUTION_ADMIN_SETTING_MONEY_SAVE_AJAX = URL_ROOT + "save.do";

	public static final String URL_INSTITUTION_ADMIN_ASSIGN_EVENT = URL_ROOT
			+ "institution/admin/settings/event-assign";
	public static final String URL_INSTITUTION_ADMIN_ASSIGN_EVENT_MODAL_AJAX = URL_ROOT + "event-assign.do";
	public static final String URL_INSTITUTION_ADMIN_ASSIGN_EVENT_SAVE_AJAX = URL_ROOT + "save.do";

	public static final String URL_INSTITUTION_USER = URL_ROOT + "institution/user/dashboard";
	public static final String URL_INSTITUTION_USER_MARKET_DEPTH = URL_ROOT + "institution/user/market-depth";
	public static final String URL_INSTITUTION_USER_ORDER_SAVE_AJAX = URL_ROOT + "order.do";

	/** for Interest Rate Settings **/
	public static final String URL_INSTITUTION_ADMIN_SETTING_INTEREST_RATE = URL_ROOT
			+ "institution/admin/settings/interest-rate";
	public static final String URL_INSTITUTION_ADMIN_SETTING_INTEREST_RATE_AJAX = URL_ROOT + "interest-rate.do";
	public static final String URL_INSTITUTION_ADMIN_SETTING_INTEREST_RATE_SAVE_AJAX = URL_ROOT + "save.do";

}
