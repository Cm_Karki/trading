package com.sharesansar.trading.constant;

/**
 * 
 * @author Cm
 *
 */
public enum Validity {
	DAY("Day"), GTC("GTC"), GTD("GTD"), AON("AON"), IOC("IOC"), FOK("FOK");

	private String value;

	private Validity(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public static Validity getByValue(String value) {
		for (Validity validity : Validity.values())
			if (validity.getValue().equals(value))
				return validity;
		return null;
	}

}
