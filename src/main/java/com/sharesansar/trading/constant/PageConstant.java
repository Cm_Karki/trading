package com.sharesansar.trading.constant;

public class PageConstant {

	// Email template
	public static final String PAGE_EMAIL_USER_CREATE = "email/user-create";
	public static final String PAGE_EMAIL_USER_ROLE_CHANGED = "email/user-role-changed";
	public static final String PAGE_EMAIL_USER_ACCOUNT_REGISTERD = "email/user-registerd";

	public static final String PAGE_EMAIL_USER_ACCOUNT_DISABLE = "email/user-account-disabled";
	public static final String PAGE_EMAIL_USER_ACCOUNT_ENABLED = "email/user-account-anabled";

	public static final String PAGE_APP_ADMIN_LOGIN = "users/app-admin/admin_login";
	public static final String PAGE_APP_ADMIN_DASHBOARD = "users/app-admin/dashboard";
	public static final String PAGE_APP_ADMIN_COMPANIES_SYNC_MODAL = "users/app-admin/_sync-company :: modalWrapper";
	/** Institution **/
	public static final String PAGE_APP_ADMIN_INSTITUTION_LIST = "users/app-admin/institution/list";
	public static final String PAGE_APP_ADMIN_INSTITUTION_CREATE = "users/app-admin/institution/create";
	public static final String PAGE_APP_ADMIN_INSTITUTION_VIEW = "users/app-admin/institution/view";
	public static final String PAGE_APP_ADMIN_INSTITUTION_EDIT = "users/app-admin/institution/update";
	/** User **/
	public static final String PAGE_APP_ADMIN_USER_LIST = "users/app-admin/user/list";
	public static final String PAGE_APP_ADMIN_USER_CREATE = "users/app-admin/user/create";
	public static final String PAGE_APP_ADMIN_USER_VIEW = "users/app-admin/user/view";
	public static final String PAGE_APP_ADMIN_USER_UPDATE = "users/app-admin/user/update";

	public static final String PAGE_USER_LOGIN = "users/institution/login";

	public static final String PAGE_PASSWORD_RESET = "users/institution/password-reset";
	/** Institutional Admin **/
	public static final String PAGE_INSTITUTION_ADMIN_DASHBOARD = "users/institution/admin/dashboard";
	public static final String PAGE_USER_REGISTRATION = "users/institution/registration";

	/** user by institution admin **/
	public static final String PAGE_USER_LIST = "users/institution/admin/user/list";
	public static final String PAGE_USER_CREATE = "users/institution/admin/user/create";
	public static final String PAGE_USER_VIEW = "users/institution/admin/user/view";
	public static final String PAGE_USER_UPDATE = "users/institution/admin/user/update";

	/** institutional admin settings **/
	public static final String PAGE_EVENT_MGMT_LIST = "users/institution/admin/setting/event-mgmt";
	public static final String PAGE_EVENT_CREATE = "users/institution/admin/setting/event-create";
	public static final String PAGE_EVENT_VIEW = "users/institution/admin/setting/event-view";
	public static final String PAGE_EVENT_UPDATE = "users/institution/admin/setting/event-update";

	public static final String PAGE_INSTITUTION_ADMIN_ASSIGN_EVENT = "users/institution/admin/setting/event-assign/assign-event";
	public static final String PAGE_INSTITUTION_ADMIN_ASSIGN_EVENT_MODAL = "users/institution/admin/setting/event-assign/_modal :: modalWrapper";
	public static final String PAGE_INSTITUTION_ADMIN_MONEY_ALLOCATION = "users/institution/admin/setting/_money-allocation :: modalWrapper";

	public static final String PAGE_INSTITUTION_USER_DASHBOARD = "users/institution/user/dashboard";
	public static final String PAGE_INSTITUTION_USER_MARKET_DEPTH = "users/institution/user/market-depth";
	public static final String PAGE_INSTITUTION_USER_ORDER_FORM = "users/institution/user/_order-form :: formWrapper";

	public static final String PAGE_INSTITUTION_ADMIN_INTEREST_RATE = "users/institution/admin/setting/interest-rate :: modalWrapper";

	// Fixed deposit
	public static final String PAGE_FIXED_DEPOSIT_LIST = "users/institution/user/fixed-deposit/list";
	public static final String PAGE_FIXED_DEPOSIT_CREATE = "users/institution/user/fixed-deposit/create";
	public static final String PAGE_FIXED_DEPOSIT_VIEW = "users/institution/user/fixed-deposit/view";
	public static final String PAGE_FIXED_DEPOSIT_UPDATE = "users/institution/user/fixed-deposit/update";

	// My Details
	public static final String PAGE_INSTITUTION_USER_MY_DETAILS_LIST = "users/institution/user/my-details/view";
	public static final String PAGE_INSTITUTION_USER_MY_DETAILS_EDIT = "users/institution/user/my-details/edit";
	public static final String PAGE_INSTITUTION_USER_MY_DETAILS_UPDATE = "users/institution/user/my-details/update";
	public static final String PAGE_INSTITUTION_USER_MY_DETAILS_CREATE = "users/institution/user/my-details/create";

}
