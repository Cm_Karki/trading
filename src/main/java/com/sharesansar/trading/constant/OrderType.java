package com.sharesansar.trading.constant;

/**
 * 
 * @author Cm
 *
 */
public enum OrderType {
	BUY("Buy"), SELL("Sell");

	private String value;

	private OrderType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public static OrderType getByValue(String value) {
		for (OrderType orderType : OrderType.values())
			if (orderType.getValue().equals(value))
				return orderType;
		return null;
	}

}
