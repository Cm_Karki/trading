package com.sharesansar.trading.constant;

/**
 * 
 * @author Cm
 *
 */
public enum OrderStatus {
	LIVE("Live"), TERMINATED("Terminated"), EXECUTED("Executed");

	private String value;

	private OrderStatus(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	private static OrderStatus getByValue(String value) {
		for (OrderStatus status : OrderStatus.values())
			if (status.getValue().equals(value))
				return status;
		return null;
	}

}
