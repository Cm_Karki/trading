package com.sharesansar.trading.constant;

public class AppConstant {

	public static final int FIELD_LENGTH_255 = 255;
	public static final int FIELD_LENGTH_20 = 20;
	public static final int PRECISION_36 = 36;
	public static final int SCALE_2 = 2;
	public static final String ALL_CAPS_OK = "OK";
	public static final String ALL_CAPS_ERROR = "ERROR";
	public static final String PASSWORD_VALIDATION_REGEX = "(?=.*[A-Z])(?=.*[a-z])(?=.*[\\d])(?=.*[@$#%])(?!.*\\s).{6,17}";
}
