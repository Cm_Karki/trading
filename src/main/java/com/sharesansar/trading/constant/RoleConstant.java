package com.sharesansar.trading.constant;

public class RoleConstant {

	public static final String IS_AUTHENTICATED_ANONYMOUSLY = "IS_AUTHENTICATED_ANONYMOUSLY";
	public static final String APP_ADMIN = "ROLE_APP_ADMIN";
	public static final String INSTITUTION_ADMIN = "ROLE_INSTITUTION_ADMIN";
	public static final String NORMAL_USER = "ROLE_NORMAL_USER";
}
