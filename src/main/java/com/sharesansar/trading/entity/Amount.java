package com.sharesansar.trading.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.ManyToAny;

import com.sharesansar.trading.constant.AppConstant;
import com.sharesansar.trading.entity.auth.User;

@Entity
@Table(name = "amount", indexes = { @Index(name = "index_amount", columnList = "amount") })
public class Amount extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "user_id")
	@NotNull
	@LazyCollection(LazyCollectionOption.FALSE)
	private User user;

	@Column(name = "amount")
	@NotNull
	@Digits(integer = AppConstant.PRECISION_36, fraction = AppConstant.SCALE_2)
	private BigDecimal amount;

	@Column(name = "expired_date")
	@NotNull
	private Date expiredDate;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

}
