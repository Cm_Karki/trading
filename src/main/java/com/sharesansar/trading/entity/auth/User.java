package com.sharesansar.trading.entity.auth;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.sharesansar.trading.constant.AppConstant;
import com.sharesansar.trading.entity.AbstractEntity;
import com.sharesansar.trading.entity.Institution;
import com.sharesansar.trading.entity.InstitutionEvent;
import com.sharesansar.trading.util.DateUtil;

@Entity
@Table(name = "user", indexes = { @Index(columnList = "user_name", name = "index_user_name"),
		@Index(columnList = "email", name = "index_email") })
public class User extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "institution_id")
	@LazyCollection(LazyCollectionOption.FALSE)
	@NotNull
	private Institution institution;

	@Column(name = "user_name")
	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String userName;

	@Column(name = "password")
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String password;

	@Column(name = "email")
	@NotBlank
	@Email
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String email;

	@Column(name = "expired_date")
	@NotNull
	private Date expiredDate;

	@Column(name = "password_reset_code")
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String passwordResetCode;

	@Column(name = "enabled")
	private Boolean enabled;

	@OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
	@Valid
	@LazyCollection(LazyCollectionOption.FALSE)
	private UserDetail userDetail;

	@ManyToMany
	@JoinTable(name = "user_role", joinColumns = {
			@JoinColumn(name = "user_id", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "role_id", nullable = false, updatable = false) })
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Role> roles;

	@ManyToMany(mappedBy = "users")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<InstitutionEvent> events;

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String getPasswordResetCode() {
		return passwordResetCode;
	}

	public void setPasswordResetCode(String passwordResetCode) {
		this.passwordResetCode = passwordResetCode;
	}

	public UserDetail getUserDetail() {
		return userDetail;
	}

	public void setUserDetail(UserDetail userDetail) {
		this.userDetail = userDetail;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String formatDate() {
		if (getCreatedDate() != null)
			return DateUtil.getFormattedDate(getCreatedDate());
		return "";
	}

	public List<InstitutionEvent> getEvents() {
		return events;
	}

	public void setEvents(List<InstitutionEvent> events) {
		this.events = events;
	}

	

	@Override
	public String toString() {
		return "User [institution=" + institution + ", userName=" + userName + ", password=" + password + ", email="
				+ email + ", expiredDate=" + expiredDate + ", passwordResetCode=" + passwordResetCode + ", enabled="
				+ enabled + ", userDetail=" + userDetail + "]";
	}

	// defined function
	public boolean isAssignToThisEvent(Long id) {
		boolean isAssign = false;
		List<InstitutionEvent> events = this.events;
		for (InstitutionEvent event : events) {
			if (event.getId() == id) {
				isAssign = true;
				break;
			}
		}
		return isAssign;
	}

}
