package com.sharesansar.trading.entity.auth;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.sharesansar.trading.constant.AppConstant;
import com.sharesansar.trading.entity.AbstractEntity;

@Entity
@Table(name = "app_admin", indexes = { @Index(columnList = "user_name", name = "index_user_name"),
		@Index(columnList = "email", name = "index_email") })
public class AppAdmin extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "user_name")
	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String userName;

	@Column(name = "email")
	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String email;

	@Column(name = "password")
	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String password;

	@ManyToMany
	@JoinTable(name = "app_admin_admin_role", joinColumns = {
			@JoinColumn(name = "user_id", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "role_id", nullable = false, updatable = false) })
	@LazyCollection(LazyCollectionOption.FALSE)
	@NotEmpty
	private List<AdminRole> adminRoles;
	
	
	public List<AdminRole> getAdminRoles() {
		return adminRoles;
	}

	public void setAdminRoles(List<AdminRole> adminRoles) {
		this.adminRoles = adminRoles;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
