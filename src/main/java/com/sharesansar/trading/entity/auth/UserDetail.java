package com.sharesansar.trading.entity.auth;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.sharesansar.trading.constant.AppConstant;
import com.sharesansar.trading.entity.AbstractEntity;

@Entity
@Table(name = "user_detail", indexes = { @Index(columnList = "name", name = "index_name"),
		@Index(columnList = "address", name = "index_address") })
public class UserDetail extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@OneToOne
	@JoinColumn(name = "user_id")
	@LazyCollection(LazyCollectionOption.FALSE)
	private User user;

	@Column(name = "name")
	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String name;

	@Column(name = "address")
	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String address;

	@Column(name = "contact_number")
	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String contactNumber;
	
	@Column(name = "father_name")
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String fatherName;
	
	@Column(name = "grandfather_name")
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String grandfatherName;
	
	@Column(name = "date_of_birth")
	private Date dateOfBirth;
	
	@Column(name = "boid")
	@Digits(integer = AppConstant.PRECISION_36, fraction = 0)
	private BigDecimal boid;
	
	@Column(name = "client_code")
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String clientCode;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getGrandfatherName() {
		return grandfatherName;
	}

	public void setGrandfatherName(String grandfatherName) {
		this.grandfatherName = grandfatherName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public BigDecimal getBoid() {
		return boid;
	}

	public void setBoid(BigDecimal boid) {
		this.boid = boid;
	}

	public String getClientCode() {
		return clientCode;
	}

	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	
	
}
	

	
	