package com.sharesansar.trading.entity.auth;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.sharesansar.trading.constant.AppConstant;
import com.sharesansar.trading.entity.AbstractEntity;

@Entity
@Table(name = "role", indexes = { @Index(columnList = "name", name = "index_name") })
public class Role extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "name")
	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String name;

	@ManyToMany(mappedBy = "roles")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<User> user;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<User> getUser() {
		return user;
	}

	public void setUser(List<User> user) {
		this.user = user;
	}

}
