package com.sharesansar.trading.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.sharesansar.trading.constant.AppConstant;

@Entity
@Table(name = "money_allocation", indexes = { @Index(name = "index_amount", columnList = "amount") })
public class MoneyAllocation extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@OneToOne
	@JoinColumn(name = "institution_id")
	@NotNull
	@LazyCollection(LazyCollectionOption.FALSE)
	private Institution institution;

	@Column(name = "amount")
	@NotNull
	@Digits(integer = AppConstant.PRECISION_36, fraction = AppConstant.SCALE_2)
	public BigDecimal amount;

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "MoneyAllocation [institution=" + institution + ", amount=" + amount + "]";
	}

}
