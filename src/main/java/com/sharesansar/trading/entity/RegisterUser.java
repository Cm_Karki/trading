package com.sharesansar.trading.entity;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.sharesansar.trading.constant.AppConstant;
import com.sharesansar.trading.constrian.PasswordMatcher;

@PasswordMatcher(first = "password", second = "conformPassword")
public class RegisterUser {

	private Long id;

	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String userName;

	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String password;

	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String conformPassword;

	@NotBlank
	@Email
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String email;

	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String name;

	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String address;

	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String contactNumber;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConformPassword() {
		return conformPassword;
	}

	public void setConformPassword(String conformPassword) {
		this.conformPassword = conformPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

}
