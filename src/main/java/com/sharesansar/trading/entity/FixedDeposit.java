package com.sharesansar.trading.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.sharesansar.trading.constant.AppConstant;
import com.sharesansar.trading.entity.auth.User;

/**
 * Fixed deposit entity
 * 
 * @author aleeza
 *
 */
@Entity
@Table(name = "fixed_deposit")
public class FixedDeposit extends AbstractEntity {

	private static final long serialVersionUID = 7410370283692093380L;

	@ManyToOne
	@JoinColumn(name = "user_id")
	@LazyCollection(LazyCollectionOption.FALSE)
	@NotNull
	private User user;

	@Column(name = "amount", precision = AppConstant.PRECISION_36, scale = AppConstant.SCALE_2)
	@NotNull
	@Digits(integer = AppConstant.PRECISION_36, fraction = AppConstant.SCALE_2)
	private BigDecimal amount;

	@Column(name = "month")
	@NotNull
	private Integer month;


	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	@Override
	public String toString() {
		return "FixedDeposit [user=" + user + ", amount=" + amount + ", month=" + month + "]";
	}

	public Object getName() {
		// TODO Auto-generated method stub
		return null;
	}

}
