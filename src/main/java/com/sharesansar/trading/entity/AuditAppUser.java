//package com.sharesansar.trading.entity;
//
//import java.util.Collection;
//
//import org.springframework.security.core.GrantedAuthority;
//
//import com.sharesansar.trading.entity.auth.AppAdmin;
//
///**
// * Entity for audit user.
// * 
// * @author cm
// *
// */
//public class AuditAppUser extends org.springframework.security.core.userdetails.User {
//
//	private static final long serialVersionUID = 1L;
//	private Long id;
//	private AppAdmin appAdmin;
//
//	public AuditAppUser(String userName, String password, Collection<? extends GrantedAuthority> authorities) {
//		super(userName, password, authorities);
//	}
//
//	public AuditAppUser(String userName, String password, Collection<? extends GrantedAuthority> authorities, long id,
//			AppAdmin appAdmin) {
//		super(userName, password, authorities);
//		this.appAdmin = appAdmin;
//		this.id = id;
//	}
//
//	public AuditAppUser(String userName, String password, boolean enabled, boolean accountNonExpired,
//			boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities,
//			AppAdmin appAdmin, long id) {
//		super(userName, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
//		this.appAdmin = appAdmin;
//		this.id = id;
//	}
//
//	public Long getId() {
//		return id;
//	}
//
//	public void setId(Long id) {
//		this.id = id;
//	}
//
//	public AppAdmin getAppAdmin() {
//		return appAdmin;
//	}
//
//	public void setAppAdmin(AppAdmin appAdmin) {
//		this.appAdmin = appAdmin;
//	}
//
//}
