package com.sharesansar.trading.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.sharesansar.trading.constant.AppConstant;

@Entity
@Table(name = "interest_rate", indexes = { @Index(name = "index_interest", columnList = "interest") })
public class InterestRate extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@OneToOne
	@JoinColumn(name = "institution_id")
	@NotNull
	@LazyCollection(LazyCollectionOption.FALSE)
	private Institution institution;
	

	@Column(name = "interest")
	@NotNull
	@Digits(integer = AppConstant.PRECISION_36, fraction = AppConstant.SCALE_2)
	public BigDecimal interest;
	

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public BigDecimal getInterest() {
		return interest;
	}

	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}
	
	@Override
	public String toString() {
		return "InterestRate [institution=" + institution + ", interest=" + interest + "]";
	}
}
