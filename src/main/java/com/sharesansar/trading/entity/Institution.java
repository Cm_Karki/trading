package com.sharesansar.trading.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.sharesansar.trading.constant.AppConstant;

@Entity
@Table(name = "institution", indexes = { @Index(columnList = "name", name = "index_name"),
		@Index(columnList = "email", name = "index_email"), @Index(columnList = "address", name = "index_address") })
public class Institution extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@OneToMany(mappedBy = "institution", cascade = CascadeType.ALL)
	@Valid
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<InstitutionEvent> setting;

	@Column(name = "name")
	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String name;

	@Column(name = "email")
	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String email;

	@Column(name = "address")
	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String address;

	@Column(name = "contact_no")
	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String contactNo;

	@Column(name = "max_user_limit")
	@NotNull
	@Digits(integer = AppConstant.PRECISION_36, fraction = AppConstant.SCALE_2)
	private BigDecimal maxUserLimit;

	@Column(name = "expired_date")
	@NotNull
	private Date expiredDate;

	@Column(name = "is_main")
	@NotNull
	private Boolean isMain;

	@OneToOne(mappedBy = "institution")
	@Valid
	@LazyCollection(LazyCollectionOption.FALSE)
	private MoneyAllocation moneyAllocation;

	public List<InstitutionEvent> getSetting() {
		return setting;
	}

	public void setSetting(List<InstitutionEvent> setting) {
		this.setting = setting;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public BigDecimal getMaxUserLimit() {
		return maxUserLimit;
	}

	public void setMaxUserLimit(BigDecimal maxUserLimit) {
		this.maxUserLimit = maxUserLimit;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public Boolean getIsMain() {
		return isMain;
	}

	public void setIsMain(Boolean isMain) {
		this.isMain = isMain;
	}

	public MoneyAllocation getMoneyAllocation() {
		return moneyAllocation;
	}

	public void setMoneyAllocation(MoneyAllocation moneyAllocation) {
		this.moneyAllocation = moneyAllocation;
	}

	@Override
	public String toString() {
		return "Institution [name=" + name + ", email=" + email + ", address=" + address + ", contactNo=" + contactNo
				+ ", maxUserLimit=" + maxUserLimit + ", expiredDate=" + expiredDate + ", isMain=" + isMain + "]";
	}

	

	

	

}
