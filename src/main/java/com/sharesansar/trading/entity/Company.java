package com.sharesansar.trading.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.sharesansar.trading.constant.AppConstant;

@Entity
@Table(name = "company", indexes = { @Index(columnList = "identity", name = "index_identity"),
		@Index(columnList = "symbol", name = "index_symbol"), @Index(columnList = "name", name = "index_name"), })
public class Company extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "identity")
	@NotNull
	private Long identity;

	@Column(name = "symbol")
	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String symbol;

	@Column(name = "name")
	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String name;

	public Long getIdentity() {
		return identity;
	}

	public void setIdentity(Long identity) {
		this.identity = identity;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
