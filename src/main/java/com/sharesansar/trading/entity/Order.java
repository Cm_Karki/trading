package com.sharesansar.trading.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.NotBlank;

import com.sharesansar.trading.constant.AppConstant;
import com.sharesansar.trading.constant.OrderStatus;
import com.sharesansar.trading.constant.OrderType;
import com.sharesansar.trading.constant.Validity;
import com.sharesansar.trading.entity.auth.User;

@Entity
@Table(name = "order_history", indexes = { @Index(columnList = "company_id", name = "index_company_id"),
		@Index(columnList = "order_status", name = "index_stauts"),
		@Index(columnList = "order_type", name = "index_order_type") })

//@ValidateAmountAndValidDate(field)
public class Order extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "user_id")
	@LazyCollection(LazyCollectionOption.FALSE)
	private User user;

	@OneToOne
	@JoinColumn(name = "company_id")
	@NotNull
	@LazyCollection(LazyCollectionOption.FALSE)
	private Company company;

	@Enumerated(EnumType.STRING)
	@Column(name = "order_type")
	@NotNull
	private OrderType orderType;

	@Column(name = "quantity")
	@NotNull
	@Digits(integer = AppConstant.PRECISION_36, fraction = AppConstant.SCALE_2)
	private BigDecimal quantity;

	@Column(name = "price")
	@NotNull
	@Digits(integer = AppConstant.PRECISION_36, fraction = AppConstant.SCALE_2)
	private BigDecimal price;

	@Enumerated(EnumType.STRING)
	@Column(name = "validity")
	@NotNull
	private Validity validity;

	@Column(name = "valid_till")
	@NotNull
	private Date validTill;

	@Enumerated(EnumType.STRING)
	@Column(name = "order_status")
	@NotNull
	private OrderStatus status;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Validity getValidity() {
		return validity;
	}

	public void setValidity(Validity validity) {
		this.validity = validity;
	}

	public Date getValidTill() {
		return validTill;
	}

	public void setValidTill(Date validTill) {
		this.validTill = validTill;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Order [user=" + user + ", company=" + company + ", orderType=" + orderType + ", quantity="
				+ quantity + ", price=" + price + ", validity=" + validity + ", validTill=" + validTill + ", status="
				+ status + "]";
	}

}
