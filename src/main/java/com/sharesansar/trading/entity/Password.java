
package com.sharesansar.trading.entity;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import com.sharesansar.trading.constant.AppConstant;
import com.sharesansar.trading.constrian.PasswordMatcher;

/**
 * Password reset form
 * 
 * @author rajendra
 *
 */
@PasswordMatcher(first = "password", second = "confirmPassword")
public class Password {

	@NotBlank
	@Pattern(regexp = AppConstant.PASSWORD_VALIDATION_REGEX)
	private String password;

	private String confirmPassword;

	private Long userId;

	private String token;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
