package com.sharesansar.trading.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import com.sharesansar.trading.constant.AppConstant;
import com.sharesansar.trading.entity.auth.User;

@Entity
@Table(name = "institution_event", indexes = { @Index(columnList = "name_event", name = "index_name_event") })
public class InstitutionEvent extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManyToMany
	@JoinTable(name = "event_assign_user", joinColumns = {
			@JoinColumn(name = "event_id", nullable = false, updatable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "user_id", nullable = false, updatable = false) })
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<User> users;

	@ManyToOne
	@JoinColumn(name = "institution_id")
	@LazyCollection(LazyCollectionOption.FALSE)
	@NotNull
	private Institution institution;

	@Column(name = "name_event")
	@NotBlank
	@Length(max = AppConstant.FIELD_LENGTH_255)
	private String nameEvent;

	@Column(name = "date_from")
	@NotNull
	private Date dateFrom;

	@Column(name = "date_to")
	@NotNull
	private Date dateTo;

	@Column(name = "enabled")
	private Boolean enabled;

	public Institution getInstitution() {
		return institution;
	}

	public void setInstitution(Institution institution) {
		this.institution = institution;
	}

	public String getNameEvent() {
		return nameEvent;
	}

	public void setNameEvent(String nameEvent) {
		this.nameEvent = nameEvent;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "InstitutionEvent [institution=" + institution + ", nameEvent=" + nameEvent + ", dateFrom=" + dateFrom
				+ ", dateTo=" + dateTo + ", enabled=" + enabled + "]";
	}

}
