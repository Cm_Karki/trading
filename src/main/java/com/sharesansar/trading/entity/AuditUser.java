package com.sharesansar.trading.entity;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;

import com.sharesansar.trading.entity.auth.AppAdmin;
import com.sharesansar.trading.entity.auth.User;

/**
 * Entity for audit user.
 * 
 * @author cm
 *
 */
public class AuditUser extends org.springframework.security.core.userdetails.User {

	private static final long serialVersionUID = 1L;
	private Long id;
	private AppAdmin appAdmin;
	private User user;

	public AuditUser(String userName, String password, Collection<? extends GrantedAuthority> authorities) {
		super(userName, password, authorities);
	}

	public AuditUser(String userName, String password, Collection<? extends GrantedAuthority> authorities, long id,
			User user, AppAdmin appAdmin) {
		super(userName, password, authorities);
		this.id = id;
		this.user = user;
		this.appAdmin = appAdmin;
	}

	public AuditUser(String userName, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities,
			long id, User user, AppAdmin appAdmin) {
		super(userName, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.id = id;
		this.user = user;
		this.appAdmin = appAdmin;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		if (appAdmin != null)
			return appAdmin.getUserName();
		else if (user != null)
			return user.getUserName();
		else
			return "";
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
