package com.sharesansar.trading.constrian.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.BeanUtils;

import com.sharesansar.trading.constrian.PasswordMatcher;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatcher, Object> {

	private String firstFieldName;
	private String secondFieldName;
	private String errorMessage;

	@Override
	public void initialize(PasswordMatcher constraintAnnotation) {
		firstFieldName = constraintAnnotation.first();
		secondFieldName = constraintAnnotation.second();
		errorMessage = constraintAnnotation.message();

	}

	@Override
	public boolean isValid(Object value, ConstraintValidatorContext context) {
		boolean toReturn = false;
		try {
			final Object firstObj = BeanUtils.getProperty(value, firstFieldName);
			final Object secondObj = BeanUtils.getProperty(value, secondFieldName);
			toReturn = firstObj == null && secondObj == null || firstObj != null && firstObj.equals(secondObj);
		} catch (final Exception e) {

		}
		if (!toReturn) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(errorMessage).addNode(secondFieldName)
					.addConstraintViolation();
		}
		return toReturn;
	}

}
