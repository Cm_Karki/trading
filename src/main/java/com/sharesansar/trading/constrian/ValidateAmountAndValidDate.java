package com.sharesansar.trading.constrian;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.sharesansar.trading.constrian.impl.ValidateAmountValidator;

@Target({ TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = ValidateAmountValidator.class)
@Documented
public @interface ValidateAmountAndValidDate {
	String message() default "{constraints.ValidateQuantity}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	String[] fieldNames();

}
