package com.sharesansar.trading.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sharesansar.trading.entity.Institution;
import com.sharesansar.trading.entity.MoneyAllocation;

@Repository
public interface MoneyAllocationRepository extends CrudRepository<MoneyAllocation, Long> {

	/**
	 * 
	 * @param id
	 * @return
	 */
	MoneyAllocation findById(long id);

	/**
	 * 
	 * @return
	 */
	List<MoneyAllocation> findAllByInstitution(Institution institution);

}
