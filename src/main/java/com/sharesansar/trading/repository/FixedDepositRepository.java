package com.sharesansar.trading.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sharesansar.trading.entity.FixedDeposit;

/**
 * Repository for {@link FixedDeposit}
 * 
 * @author aleeza
 *
 */
@Repository
public interface FixedDepositRepository extends CrudRepository<FixedDeposit, Long> {

	FixedDeposit findById(Long id);

	/**
	 * Find {@link List} of {@link FixedDeposit} by {@link User}
	 * 
	 * @param user
	 * @return {@link List} of {@link FixedDeposit}
	 */
//	public List<FixedDeposit> findAllByFixedDeposit(FixedDeposit fixedDeposit);

}
