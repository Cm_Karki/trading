package com.sharesansar.trading.repository;

import org.dom4j.Branch;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sharesansar.trading.entity.auth.AppAdmin;

@Repository
public interface AppAdminRepository extends CrudRepository<AppAdmin, Long> {

	public AppAdmin findByUserName(String userName);
}
