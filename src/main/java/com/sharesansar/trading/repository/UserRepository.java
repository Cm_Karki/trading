package com.sharesansar.trading.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sharesansar.trading.entity.auth.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

	/**
	 * 
	 * @param userName
	 * @param id
	 * @return List User
	 */
	List<User> findByUserNameAndIdNot(String userName, long id);

	/**
	 * 
	 * @param id
	 * @return
	 */
	User findById(long id);

	/**
	 * 
	 * @param username
	 * @return
	 */
	User findByUserName(String username);


}
