package com.sharesansar.trading.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sharesansar.trading.entity.auth.Role;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {

	List<Role> findAllByName(String role);

	Role findByName(String role);

}
