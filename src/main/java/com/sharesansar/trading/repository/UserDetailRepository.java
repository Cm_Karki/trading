package com.sharesansar.trading.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sharesansar.trading.entity.auth.UserDetail;

@Repository
public interface UserDetailRepository extends CrudRepository<UserDetail , Long>{

	UserDetail findByName(String name);

}
