package com.sharesansar.trading.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sharesansar.trading.entity.Institution;
import com.sharesansar.trading.entity.InstitutionEvent;

@Repository
public interface EventRepository extends CrudRepository<InstitutionEvent, Long> {

	/**
	 * method to find event by id
	 * 
	 * @param id
	 * @return InstitutionEvent
	 */
	InstitutionEvent findById(Long id);

	/**
	 * 
	 * @param institution
	 * @return
	 */
	List<InstitutionEvent> findByInstitution(Institution institution);

	/**
	 * 
	 * @param identity
	 * @param institution
	 * @return
	 */
	InstitutionEvent findByIdAndInstitution(long identity, Institution institution);

}
