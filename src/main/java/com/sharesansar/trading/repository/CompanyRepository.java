package com.sharesansar.trading.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sharesansar.trading.entity.Company;

@Repository
public interface CompanyRepository extends CrudRepository<Company, Long> {

	/**
	 * 
	 * @param id
	 * @return
	 */
	Company findById(long id);

}
