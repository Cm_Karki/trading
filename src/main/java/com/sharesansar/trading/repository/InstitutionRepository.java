package com.sharesansar.trading.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sharesansar.trading.entity.Institution;

@Repository
public interface InstitutionRepository extends CrudRepository<Institution, Long> {

	/**
	 * for save to check by name since id is 0, for update find other than this
	 * 
	 * @param name
	 * @param id
	 * @return
	 */
	int countByNameAndIdNot(String name, long id);

	/**
	 * 
	 * @param id
	 * @return
	 */
	Institution findById(Long id);

	/**
	 * 
	 * @param true1
	 * @return
	 */
	Institution findByIsMain(Boolean true1);

}
