package com.sharesansar.trading.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sharesansar.trading.entity.Institution;
import com.sharesansar.trading.entity.InterestRate;

@Repository
public interface InterestRateRepository extends CrudRepository<InterestRate, Long>{

	public List<InterestRate> findAllByInstitution(Institution institution);

	public InterestRate findById(long id);

}
