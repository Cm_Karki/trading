package com.sharesansar.trading.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sharesansar.trading.constant.OrderStatus;
import com.sharesansar.trading.constant.OrderType;
import com.sharesansar.trading.entity.Order;
import com.sharesansar.trading.entity.auth.User;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {

	List<Order> findAllByUserAndStatusAndOrderType(User user, OrderStatus status, OrderType type);

	List<Order> findAllByStatus(OrderStatus status);

	List<Order> findAllByQuantity(BigDecimal bigDecimal);

}
