package com.sharesansar.trading.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.sharesansar.trading.object.NEPSECompany;

public class ApiUtil {

	private static final Logger LOG = LogManager.getLogger(ApiUtil.class);

	public static List<NEPSECompany> getCompanyList(String baseUrl, String key) {
		LOG.info("Inside ApiUtil#getCompanyList method baseUrl {}, key {}", baseUrl, key);
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/api/virtual-trading/company-list");
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Api-Key", key);
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		List<NEPSECompany> companies = new ArrayList<NEPSECompany>();
		try {
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<String> response = restTemplate.exchange(builder.build().encode().toUri(), HttpMethod.GET,
					entity, String.class);
			JSONObject jsonObject = new JSONObject(response.getBody());
			if (("Success").equals(jsonObject.get("type"))) {
				JSONArray jsonArray = jsonObject.getJSONArray("companies");
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject object = jsonArray.getJSONObject(i);
					if (object.has("id") && object.has("symbol") && object.has("name")) {
						NEPSECompany company = new NEPSECompany();
						company.setId(object.getLong("id"));
						company.setSymbol(object.getString("symbol"));
						company.setName(object.getString("name"));
						company.setInfo(object.getString("symbol") + " " + object.getString("name"));
						companies.add(company);
					}
				}
			}
		} catch (Exception e) {
			LOG.info("Exception while fetching company list");
			System.out.println(e.getMessage());
		}
		System.out.println("size "+companies.size());
		return companies;
	}

}
