package com.sharesansar.trading.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.sharesansar.trading.object.MarketDepth;
import com.sharesansar.trading.object.MarketDepth.Buy;
import com.sharesansar.trading.object.MarketDepth.Sell;

public class MarketDepthCruler {

	private static final Logger LOG = LogManager.getLogger(MarketDepthCruler.class);

	public static MarketDepth getMarketDepth() {
		LOG.info("Inside MarektDepthCurler#getMarketDepth method");
		MarketDepth marketDepth = new MarketDepth();
		String url = "http://nepalstock.com./marketdepthofcompany/187";
		Document document = null;
		try {
			document = Jsoup.connect(url).get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

//		Element tableBuy = document.select(".orderTable tbody").first();
//		if (tableBuy != null) {
//			Elements trBuy = tableBuy.select("tr");
//			List<Buy> buys = new ArrayList<Buy>();
//			for (Element tr : trBuy) {
//				if (!tr.select("td").get(1).text().equals("---")) {
//					Buy buy = new Buy();
//					buy.setBuyOrder(new BigDecimal(tr.select("td").get(0).text()));
//					buy.setQuantity(new BigDecimal(tr.select("td").get(1).text()));
//					buy.setPrice(new BigDecimal(tr.select("td").get(2).text()));
//					buys.add(buy);
//				}
//			}
//			marketDepth.setBuys(buys);
//
//			Element tableSell = document.select(".orderTable tbody").get(1);
//			Elements trSell = tableSell.select("tr");
//			List<Sell> sells = new ArrayList<Sell>();
//			for (Element tr : trSell) {
//				if (!tr.select("td").get(1).text().equals("---")) {
//					Sell sell = new Sell();
//					sell.setPrice(new BigDecimal(tr.select("td").get(0).text()));
//					sell.setQuantity(new BigDecimal(tr.select("td").get(1).text()));
//					sell.setBuyOrder(new BigDecimal(tr.select("td").get(2).text()));
//					sells.add(sell);
//
//				}
//			}
//			marketDepth.setSells(sells);
//
//			Element tableTotal = document.select(".orderTable tbody").get(2);
//			Element tr = tableTotal.select("tr").first();
//			marketDepth.setTotalBuy(new BigDecimal(tr.select("td").get(1).text()));
//			marketDepth.setTotalSell(new BigDecimal(tr.select("td").get(3).text()));
//			marketDepth.setSells(sells);
//		}

		return marketDepth;
	}

}
