package com.sharesansar.trading.util;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Date Utili
 * 
 * @author cm
 *
 */
public class DateUtil {

	private static final DateTimeFormatter DEFAULT_DATE_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd");
	private static final DateTimeFormatter DEFAULT_DATE_FORMATTER_SLASH = DateTimeFormat.forPattern("MM/dd/yyyy");
	private static final DateTimeFormatter DISPLAY_FORMATTER = DateTimeFormat.forPattern("E MMM dd, yyyy");

	/**
	 * Method that returns date into 'E MMM dd, yyyy' format (Eg: Jul 10,1990)
	 * 
	 * @author cm
	 * @param date
	 * @return {@link String}
	 */
	public static String getFormattedDate(Date date) {
		DateTime dateTime = new DateTime(date);
		return DISPLAY_FORMATTER.print(dateTime.toLocalDate());
	}

}
