package com.sharesansar.trading.util;

import org.apache.commons.lang.RandomStringUtils;

/**
 * Util class for application
 * 
 * @author rajendra
 *
 */
public class AppUtil {

	/**
	 * Generates random alpha numeric random number
	 * 
	 * @param length
	 *            length of random number
	 * @see RandomStringUtils
	 * @return
	 */

	public static String getRandomAlphanumeric(int length) {
		return RandomStringUtils.randomAlphanumeric(length).toString();
	}

	
}
