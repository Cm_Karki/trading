package com.sharesansar.trading.util;

import javax.servlet.http.HttpServletRequest;

/**
 * Url Util
 * 
 * @author rajendra
 *
 */
public class UrlUtil {

	/**
	 * Get base url from property file
	 * 
	 * @return String
	 */

	public static String getBaseUrl(HttpServletRequest request) {
		return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
				+ request.getContextPath();
	}

}
