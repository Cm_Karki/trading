package com.sharesansar.trading.util;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ValidatorUtil implements Validator {

	private javax.validation.Validator validator;

	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.usingContext().getValidator();
		Set<ConstraintViolation<Object>> constraintViolations = validator.validate(target);
		for (ConstraintViolation constraintViolation : constraintViolations) {
			String propertyPath = constraintViolation.getPropertyPath().toString();
			String message = constraintViolation.getMessage();
			String errorCode = constraintViolation.getConstraintDescriptor().getAnnotation().annotationType()
					.getSimpleName();
			errors.rejectValue(propertyPath, errorCode, message);
		}

	}

}
