
package com.sharesansar.trading.util;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * <p>
 * Bean class to hold response for Jtable. The parameters required for jtable
 * are:
 * </p>
 * <ul>
 * <li>Result ("OK", "ERROR")</li>
 * <li>Records (Arrays of records to show)</li>
 * <li>TotalRecordCount (The total number of records)</li>
 * </ul>
 * 
 * @author cm
 *
 * @param <T>
 *            The type of records
 */
public class JtableJSONResponse<T> {

	@JsonProperty("Result")
	private String result;

	@JsonProperty("Records")
	private List<T> records;

	@JsonProperty("Message")
	private String message;

	@JsonProperty("TotalRecordCount")
	private long totalRecordCount;

	public JtableJSONResponse() {
		super();
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public List<T> getRecords() {
		return records;
	}

	public void setRecords(List<T> records) {
		this.records = records;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getTotalRecordCount() {
		return totalRecordCount;
	}

	public void setTotalRecordCount(long totalRecordCount) {
		this.totalRecordCount = totalRecordCount;
	}

}
