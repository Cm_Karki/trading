package com.sharesansar.trading.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * User util
 * 
 * @author rajendra
 *
 */
public class UserUtil {

	/**
	 * Encode given String
	 * 
	 * @param password
	 * @return String
	 */
	public static String passwordEncoder(String password) {
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder(11);
		return passwordEncoder.encode(password);
	}
	
	public static void main(String[] args) {
		System.out.println(UserUtil.passwordEncoder("appadmin"));
	}

}
