
package com.sharesansar.trading.util;

/**
 * Class to hold jtable request data.
 * 
 * @author cm
 *
 */
public class JtableRequestData {

	private String searchTerm;
	private int jtStartIndex;
	private int jtPageSize;
	private String jtSorting;

	public String getSearchTerm() {
		return searchTerm;
	}

	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}

	public int getJtStartIndex() {
		return jtStartIndex;
	}

	public void setJtStartIndex(int jtStartIndex) {
		this.jtStartIndex = jtStartIndex;
	}

	public int getJtPageSize() {
		return jtPageSize;
	}

	public void setJtPageSize(int jtPageSize) {
		this.jtPageSize = jtPageSize;
	}

	public String getJtSorting() {
		return jtSorting;
	}

	public void setJtSorting(String jtSorting) {
		this.jtSorting = jtSorting;
	}

	@Override
	public String toString() {
		return "JtableRequestData [searchTerm=" + searchTerm + ", jtStartIndex=" + jtStartIndex + ", jtPageSize="
				+ jtPageSize + ", jtSorting=" + jtSorting + "]";
	}

}
