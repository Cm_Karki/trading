package com.sharesansar.trading.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.sharesansar.trading.object.NEPSECompany;

public class NEPSECompaniesCruler {
	private static final Logger LOG = LogManager.getLogger(MarketDepthCruler.class);

	public static List<NEPSECompany> getNEPSECompanies() {
		LOG.info("Inside NEPSECompaniesCruler#getNEPSECompanies method");
		List<NEPSECompany> companies = new ArrayList<NEPSECompany>();
		String url = "http://nepalstock.com./marketdepth";
		Document document = null;
		try {
			document = Jsoup.connect(url).get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Elements nepseCompanies = document.select("#StockSymbol_Select1 option");
		if (!nepseCompanies.isEmpty()) {
			for (Element c : nepseCompanies) {
				if (c.text().equals("Choose Symbol")) {
					continue;
				}
				NEPSECompany company = new NEPSECompany();
				company.setId(Long.parseLong(c.val()));
				company.setSymbol(c.text());
				companies.add(company);
				System.out.println("company id = "+c.val()+" symbol = "+c.text());
			}
		}

		return companies;
	}
}
