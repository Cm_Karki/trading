package com.sharesansar.trading.expection;

public class NameAlreadyExistException extends Exception {
	
	private static final long serialVersionUID = -658805939062029284L;

	public NameAlreadyExistException(String msg) {
		super(msg);
	}
}
