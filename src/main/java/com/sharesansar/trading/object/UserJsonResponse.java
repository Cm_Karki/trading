
package com.sharesansar.trading.object;

import java.io.Serializable;
import java.util.Date;

/**
 * Class for holding data for branch table. This class is used in response for
 * the request from branch list page.
 * 
 * @author rajendra
 *
 */
public class UserJsonResponse implements Serializable {

	private static final long serialVersionUID = 5174053373977846414L;

	private long id;
	private String userName;
	private String email;
	private String institution;
	private String roleName;
	private Date expiredDate;
	private String enabled;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public String isEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "UserJsonResponse [id=" + id + ", userName=" + userName + ", email=" + email + ", institution="
				+ institution + ", roleName=" + roleName + ", expiredDate=" + expiredDate + "]";
	}
}