package com.sharesansar.trading.object;

public class NEPSECompany {

	private Long id;

	private String symbol;

	private String name;

	private String info;

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "NEPSECompany [id=" + id + ", symbol=" + symbol + ", name=" + name + "]";
	}
	

}
