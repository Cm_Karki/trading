
package com.sharesansar.trading.object;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Class for holding data for branch table. This class is used in response for
 * the request from branch list page.
 * 
 * @author rajendra
 *
 */
public class EventJsonResponse implements Serializable {

	private static final long serialVersionUID = 5174053373977846414L;

	private long id;
	private String nameEvent;
	private Date dateFrom;
	private Date dateTo;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNameEvent() {
		return nameEvent;
	}

	public void setNameEvent(String nameEvent) {
		this.nameEvent = nameEvent;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	@Override
	public String toString() {
		return "EventJsonResponse [id=" + id + ", nameEvent=" + nameEvent + ", dateFrom=" + dateFrom + ", dateTo="
				+ dateTo + "]";
	}

	

}