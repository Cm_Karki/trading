package com.sharesansar.trading.object;

import java.io.Serializable;
import java.math.BigDecimal;

import com.sharesansar.trading.entity.auth.User;

public class FixedDepositJsonResponse implements Serializable {

	private static final long serialVersionUID = 5174053373977846414L; 
	private long id;
	private User user;
	private BigDecimal amount;
	private Integer month;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(Integer month) {
		this.month = month;
	}

	public void add(FixedDepositJsonResponse fixedDepositJsonResponse) {
		// TODO Auto-generated method stub

	}

}
