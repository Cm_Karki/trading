
package com.sharesansar.trading.object;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Class for holding data for branch table. This class is used in response for
 * the request from branch list page.
 * 
 * @author rajendra
 *
 */
public class InstitutionJsonResponse implements Serializable {

	private static final long serialVersionUID = -7078779240398484349L;

	private long id;
	private String name;
	private String address;
	private String contactNumber;
	private String email;
	private Date expiredDate;
	private BigDecimal maxUserLimit;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public BigDecimal getMaxUserLimit() {
		return maxUserLimit;
	}

	public void setMaxUserLimit(BigDecimal maxUserLimit) {
		this.maxUserLimit = maxUserLimit;
	}

}