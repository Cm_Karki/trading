package com.sharesansar.trading.object;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.sharesansar.trading.entity.auth.User;

public class UserDetailJsonResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5174053373977846414L;
	
	private User user;
	private String name;
	private String address;
	private String contactNumber;
	private String fatherName;
	private String grandfatherName;
	private Date dateOfBirth;
	private BigDecimal boid;
	private String clientCode;
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getGrandfatherName() {
		return grandfatherName;
	}
	public void setGrandfatherName(String grandfatherName) {
		this.grandfatherName = grandfatherName;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public BigDecimal getBoid() {
		return boid;
	}
	public void setBoid(BigDecimal boid) {
		this.boid = boid;
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	

}
