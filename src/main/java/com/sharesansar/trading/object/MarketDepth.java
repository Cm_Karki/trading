package com.sharesansar.trading.object;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MarketDepth {

	private BigDecimal totalBuy;

	private BigDecimal totalSell;

	private List<Sell> sells = new ArrayList<Sell>();

	private List<Buy> buys = new ArrayList<Buy>();

	public static class Sell {
		private BigDecimal quantity;

		private BigDecimal price;

		private BigDecimal buyOrder;

		public BigDecimal getQuantity() {
			return quantity;
		}

		public void setQuantity(BigDecimal quantity) {
			this.quantity = quantity;
		}

		public BigDecimal getPrice() {
			return price;
		}

		public void setPrice(BigDecimal price) {
			this.price = price;
		}

		public BigDecimal getBuyOrder() {
			return buyOrder;
		}

		public void setBuyOrder(BigDecimal buyOrder) {
			this.buyOrder = buyOrder;
		}

	}

	public static class Buy {

		private BigDecimal quantity;

		private BigDecimal price;

		private BigDecimal buyOrder;

		public BigDecimal getQuantity() {
			return quantity;
		}

		public void setQuantity(BigDecimal quantity) {
			this.quantity = quantity;
		}

		public BigDecimal getPrice() {
			return price;
		}

		public void setPrice(BigDecimal price) {
			this.price = price;
		}

		public BigDecimal getBuyOrder() {
			return buyOrder;
		}

		public void setBuyOrder(BigDecimal buyOrder) {
			this.buyOrder = buyOrder;
		}

	}

	public static class getDetail {

		private BigDecimal livePrice;

		private BigDecimal poinChange;

		private BigDecimal perChange;

		private BigDecimal prevClose;

		private BigDecimal open;

		private BigDecimal high;

		private BigDecimal low;

		private BigDecimal close;

		public BigDecimal getLivePrice() {
			return livePrice;
		}

		public void setLivePrice(BigDecimal livePrice) {
			this.livePrice = livePrice;
		}

		public BigDecimal getPoinChange() {
			return poinChange;
		}

		public void setPoinChange(BigDecimal poinChange) {
			this.poinChange = poinChange;
		}

		public BigDecimal getPerChange() {
			return perChange;
		}

		public void setPerChange(BigDecimal perChange) {
			this.perChange = perChange;
		}

		public BigDecimal getPrevClose() {
			return prevClose;
		}

		public void setPrevClose(BigDecimal prevClose) {
			this.prevClose = prevClose;
		}

		public BigDecimal getOpen() {
			return open;
		}

		public void setOpen(BigDecimal open) {
			this.open = open;
		}

		public BigDecimal getHigh() {
			return high;
		}

		public void setHigh(BigDecimal high) {
			this.high = high;
		}

		public BigDecimal getLow() {
			return low;
		}

		public void setLow(BigDecimal low) {
			this.low = low;
		}

		public BigDecimal getClose() {
			return close;
		}

		public void setClose(BigDecimal close) {
			this.close = close;
		}

	}

	public BigDecimal getTotalBuy() {
		return totalBuy;
	}

	public void setTotalBuy(BigDecimal totalBuy) {
		this.totalBuy = totalBuy;
	}

	public BigDecimal getTotalSell() {
		return totalSell;
	}

	public void setTotalSell(BigDecimal totalSell) {
		this.totalSell = totalSell;
	}

	public List<Sell> getSells() {
		return sells;
	}

	public void setSells(List<Sell> sells) {
		this.sells = sells;
	}

	public List<Buy> getBuys() {
		return buys;
	}

	public void setBuys(List<Buy> buys) {
		this.buys = buys;
	}

}
