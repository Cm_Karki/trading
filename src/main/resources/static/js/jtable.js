var sort = 'name ASC';
var fields = {};
var rowNumber = 0;

function rowInserted(event, data){
	
}

$(document).ready(function(){
	var queries1 = [];
	var fields1 = [];
	var dataUrl = $("#listUrl").text();
	$('#JtableContainer').jtable({
		title : '',
		paging : true,
		pageSize : 10,
		sorting : true,
		defaultSorting : sort,
		columnResizable : false,
		columnSelectable : false,
		gotoPageArea : 'none',
		selecting : false,
		create : false,
		edit : false,
		toolbarsearch:true,
		toolbarreset:false,
		actions : {
			listAction : function(postData, jtParams) {
				return $.Deferred(function($dfd) {
					return $.ajax({
						url : dataUrl,
						method : "GET",
						data: {
							searchTerm : "", 
							jtStartIndex : jtParams.jtStartIndex,
							jtPageSize : jtParams.jtPageSize,
							jtSorting : jtParams.jtSorting,
							queries : (queries1.length == 0) ? new Array() : queries1,
							fields :  (fields1.length == 0) ? new Array() : fields1
						},
						success : function(data) {
							rowNumber = 0;
							$dfd.resolve(data);
						},
						error : function() {
							$dfd.reject();
						}
					});
				});
			}
		},
		fields : fields,
		rowInserted: rowInserted
	});
	$("input").bind('keyup',function(ev){
		if($(this).hasClass("jtable-toolbarsearch")){
			if (ev.which === 13) {
				var i=0;
				$('.jtable-toolbarsearch').each(function(){
					var $id = $(this).attr('id');
					fields1[i] = $id.replace('jtable-toolbarsearch-','');
					queries1[i] = $(this).val();
					i++;
				});
				$('#JtableContainer').jtable('load');
	    	}
		}
	});			
	$('#JtableContainer').jtable('load');
	$("table").addClass("table table-hover");
});