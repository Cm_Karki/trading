﻿/** JTABLE Multiple toolbar search extension 
**/
(function ($) {
	var base={
		_addRowToTableHead:$.hik.jtable.prototype._addRowToTableHead
	}
    $.extend(true, $.hik.jtable.prototype, {
		options: {
			toolbarsearch:false,
			toolbarreset:true
		},
		/** Overrides Method
		/* Adds tr element to given thead element
        *************************************************************************/
        _addRowToTableHead: function ($thead) {
            var $tr = $('<tr></tr>')
                .appendTo($thead);

            this._addColumnsToHeaderRow($tr);
			if(this.options.toolbarsearch){			
	            var $tr = $('<tr></tr>')
                .appendTo($thead);
    	        this._toolbarsearch_addColumnsToHeaderRow($tr);
			}

		},
		/* Adds column header cells to given tr element.
        *************************************************************************/
        _toolbarsearch_addColumnsToHeaderRow: function ($tr) {
			var self = this;
			if(this.options.selecting && this.options.selectingCheckboxes){
				$tr.append('<td/>');	
			}
	    	for (var i = 0; i < this._columnList.length; i++) {
    	    	var fieldName = this._columnList[i];
        	    var $headerCell = this._toolbarsearch_createHeaderCellForField(fieldName, this.options.fields[fieldName]);
            	$headerCell.appendTo($tr);
            }
			if(this.options.toolbarreset){
			$reset = $('<th></th>')
                .addClass('jtable-toolbarsearch-reset')
                .attr('colspan',$(".jtable-command-column-header").length);
			$resetbutton = $('<input type="button" class="jtable-toolbarsearch-reset-button" value="Reset"/>').appendTo($reset);
			$resetbutton.click(function(){
				$('.jtable-toolbarsearch').val('');
				self.load({});				
			});
			$tr.append($reset);
			}
        },		

        /* Creates a header cell for given field.
        *  Returns th jQuery object.
        *************************************************************************/		
        _toolbarsearch_createHeaderCellForField: function (fieldName, field) {
			var self = this;
			if(typeof field.searchable === 'undefined'){
				field.searchable = false;
			};
            field.width = field.width || '10%'; //default column width: 10%.
            
            var placeholderField = "";
            
            if(fieldName == "organizationName")
            	placeholderField = "organization name";
            else if(fieldName == "schemaName")
            	placeholderField = "schema name";
            else if(fieldName == "contactNumber")
            	placeholderField = "contact number";
            else if(fieldName == "fullName")
            	placeholderField = "name";
            else if(fieldName == "userName")
            	placeholderField = "email";
            else if(fieldName == "phoneNumber")
            	placeholderField = "phone number";
            else
            	placeholderField = fieldName;

			var $input = $('<input id="jtable-toolbarsearch-' + fieldName + '" type="text" placeholder="Search by '+ placeholderField +'" />')
				.addClass('jtable-toolbarsearch form-control')
				
				.css('width','90%');
			if(field.type=="date"){
				var displayFormat = field.displayFormat || this.options.defaultDateFormat;
            	$input.datepicker({ dateFormat: displayFormat,changeMonth: true,
			changeYear: true,yearRange: "-100:+1",numberOfMonths: 3,
			showButtonPanel: true});
			}
																	
            var $headerContainerDiv = $('<div />')
                .addClass('jtable-column-header-container');
                
			if(field.searchable){	
				$headerContainerDiv.append($input);
			}
		
            var $th = $('<th></th>')
                .addClass('jtable-column-header')
                .css('width', field.width)
                .data('fieldName', fieldName)
                .append($headerContainerDiv);

            return $th;
        }
	});
	
})(jQuery);