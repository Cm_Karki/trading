var radioState = false;
$(document).ready(function() {
	$("#companyId").select2({
		width : "100%"
	});
})
$(document).on("click", "#cnc", function() {
	if (radioState == false) {
		check();
		radioState = true;
	} else {
		uncheck();
		radioState = false;
	}

	function check() {
		document.getElementById("cnc").checked = true;
	}
	function uncheck() {
		document.getElementById("cnc").checked = false;
	}
})

$(document).on("click", "#cusom-toggle", function() {
	// document.getElementById('nav').addClass("bg-primary");
	$("#nav").toggleClass("bg-danger");
	if ($("#order-button").html() === "Buy") {
		$("#order-button").html('sell');
	} else {
		$("#order-button").html('Buy');
	}

	$(".heading").toggleClass("bg-danger");
	if ($(".heading").html() === "Pending Buy Order") {
		$(".heading").html("Pending Sell Order");
	} else {
		$(".heading").html("Pending Buy Order");
	}
	console.log($("#order-type").val());
	if ($("#order-type").val() === "Buy") {
		$("#order-type").val("Sell");
	} else {
		$("#order-type").val("Buy");
	}

})

$(document).on("click", ".radio-type", function() {
	var v = $(this).val();
	radiovalue(v);
})
function radiovalue(v) {
	console.log(v);
	if (v === "lmt") {
		$(".disabled").find("input").removeAttr("disabled");
		$(".disabled").find("input").css("cursor", "text");
		$(".disabled").find("select").removeAttr("disabled");
		$(".disabled").find("select").css("cursor", "default");
		$(".disabled").css("opacity", "1");
		$('#orderForm').trigger("reset");
	} else {
		$(".disabled").find("input").attr("disabled", "disabled");
		$(".disabled").find("input").css("cursor", "no-drop");
		$(".disabled").find("select").attr("disabled", "disabled");
		$(".disabled").find("select").css("cursor", "no-drop");
		$(".disabled").css("opacity", "0.5");
		$('#orderForm').trigger("reset");
	}

}

$(document).on(
		"change",
		"#validity",
		function() {
			var value = $(this).val();
			var now = new Date();
			var dateAfter15 = new Date(new Date().getTime()
					+ (15 * 24 * 60 * 60 * 1000));
			var dateAfter15Formated = dateAfter15.getDate() + '/'
					+ (dateAfter15.getMonth()) + '/'
					+ dateAfter15.getFullYear();
			var dateAfter90 = new Date(new Date().getTime()
					+ (90 * 24 * 60 * 60 * 1000));
			var dateAfter90Formated = dateAfter90.getDate() + '/'
					+ (dateAfter90.getMonth()) + '/'
					+ dateAfter90.getFullYear();
			console.log(dateAfter15);
			if (value === "GTC") {
				$("#validTill").datepicker({
					todayHighlight : true,
					format : "yyyy-mm-dd",
					autoclose : true,
					startDate : dateAfter15
				});
				$(".date-disabled").find("input").val(
						$.datepicker.formatDate('yyyy-MM-dd', dateAfter15));
				$(".date-disabled").find("input").attr("disabled", "disabled");
				$(".date-disabled").find("input").css("cursor", "no-drop");
				$(".date-disabled").css("opacity", "0.5");
			} else if (value === "GTD") {
				$(".date-disabled").find("input").removeAttr("disabled");
				$(".date-disabled").find("input").css("cursor", "default");
				$(".date-disabled").find("input").val(dateAfter90);
				$(".date-disabled").css("opacity", "1");
			} else {
				$(".date-disabled").find("input").attr("disabled", "disabled");
				$(".date-disabled").find("input").css("cursor", "no-drop");
				$(".date-disabled").find("input").attr("disabled", "disabled");
				$(".date-disabled").find("input").val("");
				$(".date-disabled").css("opacity", "0.5");
			}
		})

$(document).on("click", "#order-button", function() {
	var url = $("#placeOrderUrl").text();
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	var radioValue = $("input[name='type']:checked").val();
	var orderType = $("#order-button").html();
	console.log(radioValue);
	$.ajax({
		url : url,
		type : "POST",
		data : $("#orderForm").serialize()+"&radioValue="+radioValue,
		beforeSend : function(xhr) {
			xhr.setRequestHeader(header, token);
		},

		success : function(result) {
			$(".order-form-container").html(result);
			$(".tableContainer").html($(".tableWrapper").html());
			$(".tableWrapper").remove();
			radiovalue(radioValue);
			var isSuccess = $("#testId").attr("issuccess");
			if (isSuccess === "YES") {
				$(".table-order").html($(".wrapper").html());
				$('#orderForm').trigger("reset");
			}

		},
		error : function() {

		}
	})
})
