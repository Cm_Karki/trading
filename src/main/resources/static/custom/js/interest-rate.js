$(document).on("click", "#my-modal-interest", function() {
	var url = $("#interest-rate-url").text();
	console.log(url)
	$.ajax({

		url : url,
		type : "GET",
		data : {

		},
		success : function(result) {

			$(".modal-container").html(result);
		},
		error : function() {

		}
	})
})

$(document).on("click", "#saveChangeInterest", function() {
	var url = $("#interest-allocation-save-url").text();
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	console.log($("#interestRateForm").serialize());
	$.ajax({
		url : url,
		type : "POST",
		data : $("#interestRateForm").serialize(),
		beforeSend : function(xhr) {
			xhr.setRequestHeader(header, token);
		},
		success : function(result) {
			$(".modal-container").html(result);
			setTimeout(function() {
				$('#myModal').modal('hide')
			}, 4000);
		},
		error : function() {

		}
	})
})
