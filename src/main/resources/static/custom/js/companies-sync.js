$(document).on("click", "#my-modal", function() {
	var url = $("#companies-sync-url").text();
	console.log(url);
	$.ajax({

		url : url,
		type : "GET",
		data : {

		},
		success : function(result) {

			$(".modal-container").html(result);
		},
		error : function() {

		}
	})
})

$(document).on("click", "#syncCompanyId", function() {
	var url = $("#companies-sync-start-url").text();
	console.log(url);
	$.ajax({

		url : url,
		type : "GET",
		data : {

		},
		success : function(result) {

			$(".modal-container").html(result);
		},
		error : function() {

		}
	})
})


$(document).on("click", "#syncCompanyIds", function() {
	var url = $("#companies-sync-start-url").text();
	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$.ajax({
		url : url,
		type : "POST",
		data : $("#moneyAllocationForm").serialize(),
		beforeSend : function(xhr) {
			xhr.setRequestHeader(header, token);
		},
		success : function(result) {
			$(".modal-container").html(result);
			setTimeout(function() {
				$('#myModal').modal('hide')
			}, 4000);
		},
		error : function() {

		}
	})
})