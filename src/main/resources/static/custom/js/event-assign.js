$(document).on("click", "#btnAdd", function() {
	var url = $("#event-assign-url").text();
	var id = $("#identity").val();
	$.ajax({
		url : url,
		type : "GET",
		data : {
			id : id
		},
		success : function(result) {
			$(".modal-holder").html(result);
		},
		error : function() {

		}
	});

})

$(document).on("click", "#btnSave", function() {
	var userIds = new Array();
	var eventId = $(".hiddenEventId").val();
	var url = $("#event-assign-save-url").text();
	var index = 0;
	$(":checkbox").each(function() {
		if ($(this).prop("checked") == true) {
			index++;
			userIds.push($(this).val());
		}
	});

	$.ajax({
		url : url,
		type : "GET",
		data : {
			eventId : eventId,
			userIds : userIds

		},
		success : function(result) {
			if (result.message === "SUCCESS") {
				window.location.replace(result.url);
			}
			elseif(result.message === "ERROR")
			{

			}
		},
		error : function() {

		}
	});
})